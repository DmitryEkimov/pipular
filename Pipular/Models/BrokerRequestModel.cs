﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pipular.Models
{
    public class BrokerRequestModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string BrokerName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("[0-9-+]+",ErrorMessage="Only numbers and '-+'")]
        public string Phone { get; set; }
    }
}