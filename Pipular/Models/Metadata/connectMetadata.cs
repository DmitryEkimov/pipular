﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pipular.Models.Metadata
{
    public class connectMetadata
    {
        [Display(Name = "Max orders number")]
        [RegularExpression("[0-9]+(?:[0-9]*)?|unlimited", ErrorMessage="You can use only positive numbers.")]
        public int cfg_morders { get; set; }

        [Display(Name = "Max summury volume")]
        [RegularExpression("[0-9]+(?:\\.[0-9]*)?|unlimited", ErrorMessage="You can use only positive decimals.")]
        public double cfg_mvolume { get; set; }
    }
}