﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using Pipular.Models.Charts;

namespace Pipular.Models
{
    public class SystemModel
    {
        public int nmbr { get; set; }

        public IEnumerable<tarif> tariffs { get; set; }

        public IEnumerable<ProfitChart> profit { get; set; }

        public int atype { get; set; }

        [Display(Name = "Enabled")]
        public int enabled { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "System name")]
        public string name { get; set; }

        [Display(Name = "Profit/loss")]
        public double pl { get; set; }

        [Display(Name = "Profit/loss in pips")]
        public double pts { get; set; }

        [Display(Name = "Drawdown")]
        public double drawdown { get; set; }

        [Display(Name = "Drawdown in pips")]
        public double pdrawdown { get; set; }

        [Display(Name = "Clients")]
        public int clients { get; set; }

        [Display(Name = "Opened")]
        public int opened { get; set; }

        [Display(Name = "Closed")]
        public int closed { get; set; }

        [Display(Name = "Signals")]
        public int signals { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Last signal time")]
        public DateTime? last_signal_time { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Create time")]
        public DateTime dt_create { get; set; }

        [Display(Name = "Serv3Online")]
        public int serv3online { get; set; }
    }

    public class AddSystemModel
    {
        public int nmbr { get; set; }

        [Display(Name = "Enabled")]
        public int enabled { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Name of system")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Description")]
        public string Desc { get; set; }

        [DataType(DataType.Html)]
        [Display(Name = "Html description")]
        public string Desc_html { get; set; }

        [Display(Name = "Trade type")]
        public List<int> Trade_type { get; set; }
        
        [Display(Name = "Trade style")]
        public List<int> Trade_style { get; set; }
        
        [Display(Name = "Markets")]
        public List<int> Trade_markets { get; set; }
        
        [Display(Name = "Sessions")]
        public List<int> Trade_sessions { get; set; }
    }

    public partial class tariff
    {
        [Display(Name="System")]
        public int system { get; set; }

        [Display(Name = "Enabled")]
        public int enabled { get; set; }

        [Display(Name = "Type")]
        public int type { get; set; }

        [Display(Name = "Price")]
        public double summa { get; set; }

        [Display(Name = "MT4")]
        public int mt4 { get; set; }

        [Display(Name = "E-mail")]
        public int email { get; set; }

        [Display(Name = "ICQ")]
        public int icq { get; set; }

        [Display(Name = "SMS")]
        public int phone { get; set; }

        [Display(Name = "Skype")]
        public int skype { get; set; }
    }

    public class AddTariffModel
    {
        [Display(Name = "Type")]
        public int type { get; set; }

        [Display(Name = "Size")]
        public int tsize { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Price")]
        public double summa { get; set; }

        [Display(Name = "Send to MT4")]
        public int mt4 { get; set; }

        [Display(Name = "Send to e-mail")]
        public int email { get; set; }

        [Display(Name = "Send to ICQ")]
        public int icq { get; set; }

        [Display(Name = "Send by SMS")]
        public int phone { get; set; }

        [Display(Name = "Send to Skype")]
        public int skype { get; set; }
    }

    public class ConnectModel
    {
        public string system_name { get; set; }

        public tarif tariff { get; set; }

        public connect connect { get; set; }
    }

    public class PortfolioModel
    {
        public int id { get; set; }

        public string name { get; set; }

        public int @public { get; set; }

        public DateTime? create { get; set; }
    }

    public class Portfolio : account
    {
        public IEnumerable<ConnectModel> connects { get; set; }
    }

    public class FillUpBalanceModel
    {
        [DataType(DataType.Currency)]
        [Display(Name = "Sum $")]
        public double summa { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Comment")]
        public string comment { get; set; }
    }

    public class CopyModel : copy
    {
        //public int enabled { get; set; }

        public int account_number { get; set; }

        public string account_company { get; set; }

        public int system_nmbr { get; set; }

        public string system_name { get; set; }

        public string cmd { get; set; }

        public string symbol { get; set; }

        public int digits { get; set; }

        public double pl { get; set; }

        public double pts { get; set; }
    }
}