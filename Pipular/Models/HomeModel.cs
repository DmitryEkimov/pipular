﻿using Pipular.Attributes.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pipular.Models
{
    public enum MessageType : byte { Bad, Middle, Good, Special }

    public class TraderModel
    {
        public int nmbr { get; set; }

        public string name { get; set; }

        public int systems { get; set; }

        public int connects { get; set; }

        public int experience { get; set; }
    }

    public class Message
    {
        public MessageType type { get; set; }

        public string text { get; set; }
    }

    public class FollowModel
    {
        public IQueryable<tarif> tariffs { get; set; }

        public string system_name { get; set; }

        [Display(Name="Tariff")]
        public int tarif_id { get; set; }

        //[Mandatory(ErrorMessage = "You must agree to the Terms to follow system.")]
        [BooleanRequired(ErrorMessage = "You must agree to the Terms to follow system.")]
        [DisplayName("I have read the agreement and authorize the system to trade on my account.")]
        public bool agree { get; set; }
    }
}