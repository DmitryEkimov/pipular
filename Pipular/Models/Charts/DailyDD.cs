﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pipular.Models.Charts
{
    public class DailyDD
    {
        public double dd { get; set; }
        public double dd_pip { get; set; }
        public DateTime date { get; set; }
    }
}