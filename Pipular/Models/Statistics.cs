﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pipular.Models
{
    public class TopSystemsModel : system
    {
        public int place { get; set; }
        public double pips { get; set; }
        public double profit { get; set; }
    }

    public class PopSystemsModel : system
    {
        public int place { get; set; }
        public int followers { get; set; }
    }
    
    public class TopTradesModel : history
    {
        public int place { get; set; }
        public string system_name { get; set; }
        public int copies { get; set; }
    }

    public class TopInstrumentsModel
    {
        public int place { get; set; }
        public string symbol { get; set; }
        public int signals { get; set; }
        public int buys { get; set; }
        public int total_signals { get; set; }
    }
}