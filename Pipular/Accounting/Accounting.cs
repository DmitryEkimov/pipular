﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pipular.Accounting
{
    public class Accounting
    {
        private readonly AccountingManager _manager;
        
        public Accounting() : this(AccountingManager.Instance) { }

        public Accounting(AccountingManager manager)
        {
            _manager = manager;
        }

        public void Start()
        {
            try { _manager.Start(); }
            catch (Exception ex) { throw ex; }
        }

        public void Stop()
        {
            try {_manager.Stop();}
            catch (Exception ex) { throw ex; }
        }

        public void Recount()
        {
            try { _manager.Recount(); }
            catch (Exception ex) { throw ex; }
        }

        public void RecountThis(RecountResult recountResult)
        {
            try { _manager.RecountThis(recountResult); }
            catch (Exception ex) { throw ex; }
        }

        public void RecountFromLast()
        {
            try { _manager.RecountFromLast(); }
            catch (Exception ex) { throw ex; }
        }
    }
}