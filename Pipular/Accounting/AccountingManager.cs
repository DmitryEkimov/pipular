﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.IO;
using System.Configuration;
using NLog;
using Pipular.Models;

namespace Pipular.Accounting
{
    public class AccountingManager
    {
        private readonly static Lazy<AccountingManager> _instance = new Lazy<AccountingManager>(() => new AccountingManager());

        public static AccountingManager Instance
        {
            get
            {
                return _instance.Value;
            }
        }
        
        protected Timer timer;
        
        protected DateTime _next_recount;
        protected RecountResult _last_recount;
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public AccountingManager()
        {
            try
            {
                _last_recount = GetLastRecount();
                _next_recount = GetNextRecount();
                if (AccountingConfiguration.AutoStart) Start();
            }
            catch (Exception ex) { logger.Error(String.Format("Message: {0}\r\nInner exception: {1}", ex.Message, ex.InnerException)); } 
        }


        // public fields
        public DateTime NextRecount
        {
            get
            {
                return _next_recount;
            }
            set
            {
                _next_recount = value;
                logger.Info("Field was changed: Next recount " + _next_recount);
            }
        }

        public RecountResult LastRecount
        {
            get { return _last_recount; }
        }

        public TimeSpan LeftToStart
        {
            get
            {
                return DateTime.UtcNow - _next_recount;
            }
            set
            {
                _next_recount = DateTime.UtcNow + value;
                logger.Info("Field was changed: Left to start " + value + ". Next recount " + _next_recount);
                Restart();
            }
        }



        public void Recount()
        {
            try
            {
                _last_recount = GetLastRecount();
                Recount(_last_recount.stop, DateTime.UtcNow);
            }
            catch (Exception ex) { logger.Error(String.Format("Message: {0}\r\nInner exception: {1}", ex.Message, ex.InnerException)); }
        }

        public void RecountThis(RecountResult thisRecount)
        {
            try
            {
                logger.Info("Removing this recounting... (from " + thisRecount.start + " to " + thisRecount.stop + ")");
                RemoveThisAccounting(thisRecount);
                Recount(thisRecount.start, thisRecount.stop);
            }
            catch (Exception ex) { logger.Error(String.Format("Message: {0}\r\nInner exception: {1}", ex.Message, ex.InnerException)); }
        }
        
        public void RecountFromLast()
        {
            try
            {
                logger.Info("Removing last recounting...");
                _last_recount = GetLastRecount();
                RemoveThisAccounting(_last_recount);
                Recount(_last_recount.start, DateTime.UtcNow);
            }
            catch (Exception ex) { logger.Error(String.Format("Message: {0}\r\nInner exception: {1}", ex.Message, ex.InnerException)); }
        }  


        public void Start()
        {
            try
            {
                timer = new Timer();
                timer.AutoReset = true;
                timer.Interval = AccountingConfiguration.RecountNow ? 100 : (GetNextRecount() - DateTime.UtcNow).TotalMilliseconds;
                timer.Elapsed += new ElapsedEventHandler(Recount);
                timer.Start();
                logger.Info("Timer starts.");
            }
            catch (Exception ex) { logger.Error(String.Format("Message: {0}\r\nInner exception: {1}", ex.Message, ex.InnerException)); }
        }

        public void Stop()
        {
            timer.Stop();
            timer.Close();
            timer.Dispose();
            logger.Info("Timer stops.");
        }

        public void Restart()
        {
            if (timer == null) Start();
            else
            { 
                Stop();
                Start();
            }
        }

        // one recount
        private void Recount(DateTime start, DateTime stop)
        {
            try
            {
                Recounting recounting = new Recounting();
                logger.Info("One recounting starts. Period from " + start + " to " + stop + " by UTC.");
                recounting.Recount(start, stop);
                logger.Info("One recounting successfully complete.");
                SetRecountTime(start, stop);
            }
            catch (Exception ex) { throw ex; }
        }

        private void Recount(object sender, ElapsedEventArgs e)
        {
            try
            {
                logger.Info("Recounting starts.");

                timer.Interval = (GetNextRecount() - DateTime.UtcNow).TotalMilliseconds;
                
                Recounting recounting = new Recounting();

                DateTime start = _last_recount.stop;
                DateTime stop = DateTime.UtcNow;

                recounting.Recount(start, stop);

                SetRecountTime(start, stop);

                _next_recount = GetNextRecount();

                logger.Info("Recounting successfully complete. Next recount will be " + _next_recount);
            }
            catch (Exception ex) { logger.Error(String.Format("Message: {0}.\r\nInner exception: {1}", ex.Message, ex.InnerException)); Stop(); }
        }

        private DateTime GetNextRecount()
        {
            DateTime result = new DateTime();
            DateTime now = DateTime.UtcNow;

            int size = AccountingConfiguration.Size;
            
            switch (AccountingConfiguration.Period)
            {
                case AccountingConfiguration.PeriodType.minutes:
                    TimeSpan ts = new TimeSpan(0, size, 0);
                    int add_periods = (int)Math.Ceiling((now - _last_recount.stop).TotalMinutes / size);
                    TimeSpan addts = new TimeSpan(0, size * add_periods, 0);
                    result = _last_recount.stop + addts;
                    return result;
                case AccountingConfiguration.PeriodType.hours:
                    return result;
                case AccountingConfiguration.PeriodType.days:
                    return result;
                case AccountingConfiguration.PeriodType.weeks:
                    return result;
                case AccountingConfiguration.PeriodType.months:
                    return result;
                default: 
                    return now.AddMinutes(10);
            }
        }

        private RecountResult GetLastRecount()
        {
            RecountResultCollection rrc = RecountResultCollection.Load();

            return rrc.Count == 0 ? new RecountResult() { start = new DateTime(1970, 1, 1), stop = new DateTime(1970, 1, 1) } : rrc.Last();
        }

        private TimeSpan GetTimeLeftToRecount()
        {
            throw new NotImplementedException();
        }

        private void SetRecountTime(DateTime start, DateTime stop)
        {
            try
            {
                RecountResultCollection rrc = RecountResultCollection.Load();

                if (rrc.Count > 0 && AccountingConfiguration.ReplaceLast) rrc.RemoveAt(rrc.Count - 1);

                rrc.Add(new RecountResult()
                {
                    start = start.Round(),
                    stop = stop.Round()
                });

                rrc.Save();

                _last_recount = rrc.Last();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RemoveThisAccounting(RecountResult this_recount)
        {
            pipularEntities DB = new pipularEntities();

            IEnumerable<accounting> acc = from a in DB.accountings
                                          where a.dt_start == this_recount.start && a.dt_stop == this_recount.stop
                                          select a;
            foreach (accounting a in acc)
                DB.accountings.DeleteObject(a);

            DB.SaveChanges();

            RecountResultCollection.Delete(new RecountResult() { start = this_recount.start, stop = this_recount.stop });
        }
    }
}
