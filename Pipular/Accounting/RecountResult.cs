﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pipular.Accounting
{
    public class RecountResult
    {
        public DateTime start { get; set; }

        public DateTime stop { get; set; }
    }
}