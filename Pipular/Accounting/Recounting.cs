﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pipular.Models;

namespace Pipular.Accounting
{
    public class Recounting
    {
        pipularEntities DB = new pipularEntities();
        RecountResultCollection RRC = new RecountResultCollection();

        #region Recounting
        /// <summary>
        /// Recount all fees from last recount time to stop time
        /// </summary>
        /// <param name="stop"></param>
        /// <returns></returns>
        public int Recount(DateTime start, DateTime stop)
        {
            try
            {
                DateTime lastRecountTime = start;
                List<history> History = GetHistory(lastRecountTime, stop).ToList();
                List<connect> Connects = GetConnects().ToList();
                List<tarif> Tariffs = GetTariffs().ToList();
                List<system> Systems = GetSystems().ToList();
                List<balance> Balances = GetBalances().ToList();

                foreach (connect c in Connects)
                {
                    List<history> HistoryOfConnect = GetConnectHistory(History, c).ToList();
                    account acc = DB.accounts.FirstOrDefault(a => a.nmbr == c.account2);
                    tarif tariff = Tariffs.First(t => t.nmbr == c.tarif);

                    if ((HistoryOfConnect.Count == 0 && tariff.type > 0) || acc == null) continue; // реальный или демо счёт

                    int trader_nmbr = Systems.First(s => s.nmbr == c.system).user;

                    double trader_balance = Balances.Where(b => b.user == trader_nmbr).ToList().Sum(b => b.summa);
                    double client_balance = Balances.Where(b => b.user == c.user).ToList().Sum(b => b.summa);
                    double reward = GetReward(HistoryOfConnect, tariff, c.dt_create, lastRecountTime, stop);

                    int paid = reward <= client_balance ? 1 : 0;

                    double profit_trader = History.Where(h => h.connect == 0 && h.system == c.system).Sum(h => h.pl);
                    double profit_trader_pip = History.Where(h => h.connect == 0 && h.system == c.system).Sum(h => h.pts);
                    double profit_client = History.Where(h => h.connect == c.nmbr && h.system == c.system).Sum(h => h.pl);
                    double profit_client_pip = History.Where(h => h.connect == c.nmbr && h.system == c.system).Sum(h => h.pts);

                    // pay from user
                    balance client_add_balance = new balance()
                        {
                            user = c.user,
                            system = c.system,
                            type = 2,
                            active = paid,
                            summa = -reward,  // important!
                            comment = "withdraw for system",
                            dt_create = DateTime.UtcNow
                        };
                    
                    DB.balances.AddObject(client_add_balance);
                    Balances.Add(client_add_balance);

                    // pay to trader
                    balance trader_add_balance = new balance()
                    {
                        user = trader_nmbr,
                        system = c.system,
                        type = 2,
                        active = paid,
                        summa = reward,      // important!
                        comment = "pay for system",
                        dt_create = DateTime.UtcNow
                    };

                    DB.balances.AddObject(trader_add_balance);
                    Balances.Add(trader_add_balance);

                    DB.SaveChanges();

                    DB.accountings.AddObject(new accounting()
                    {
                        connect = c.nmbr,
                        user = c.user,
                        system = c.system,
                        trades = HistoryOfConnect.Count(),
                        tariff_type = tariff.type,
                        tariff_tsize = tariff.tsize,
                        tariff_summa = tariff.summa,
                        reward = reward,
                        profit_trader = profit_trader,
                        profit_trader_pip = profit_trader_pip,
                        profit_client = profit_client,
                        profit_client_pip = profit_client_pip,
                        trader_balance = trader_balance,
                        trader_balance_nmbr = trader_add_balance.nmbr,
                        client_balance = client_balance,
                        client_balance_nmbr = client_add_balance.nmbr,
                        paid = paid,
                        dt_start = lastRecountTime,
                        dt_stop = stop,
                        dt_create = DateTime.UtcNow
                    });

                    DB.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return 0;
        } 
        #endregion

        #region GetBalances()
        /// <summary>
        /// Gets all active balances.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<balance> GetBalances()
        {
            return from b in DB.balances
                   where b.active == 1
                   select b;
        } 
        #endregion

        #region GetSystems()
        /// <summary>
        /// Gets all systems in history.
        /// </summary>
        /// <param name="history"></param>
        /// <returns></returns>
        private IEnumerable<system> GetSystems()
        {
            return from s in DB.systems
                   select s;
        } 
        #endregion

        #region GetHistory(DateTime start, DateTime stop)
        /// <summary>
        /// Gets all history of current period from start to stop.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="stop"></param>
        /// <returns></returns>
        public IEnumerable<history> GetHistory(DateTime start, DateTime stop)
        {
            int startUnix = start.DateToUnix();
            int stopUnix = stop.DateToUnix();

            return from h in DB.historys
                   where h.close_time >= startUnix && h.close_time < stopUnix
                   select h;
        } 
        #endregion

        #region GetConnects()
        /// <summary>
        /// Gets all enabled connects
        /// </summary>
        /// <returns></returns>
        private IEnumerable<connect> GetConnects()
        {
            return from c in DB.connects
                   where c.enabled == 1 && c.account2 > 0
                   select c;
        } 
        #endregion

        #region GetConnectHistory(IEnumerable<history> hist, connect con)
        /// <summary>
        /// Gets all connect history.
        /// </summary>
        /// <param name="hist">Main history.</param>
        /// <param name="con">Connects.</param>
        /// <returns>Returns history.</returns>
        private IEnumerable<history> GetConnectHistory(IEnumerable<history> hist, connect con)
        {
            return from h in hist
                   where h.connect == con.nmbr
                   select h;
        } 
        #endregion

        #region GetTariffs()
        /// <summary>
        /// Gets all tariff for this connects.
        /// </summary>
        /// <param name="connects">Connects</param>
        /// <returns></returns>
        private IEnumerable<tarif> GetTariffs()
        {
            return from t in DB.tarifs
                   select t;
        } 
        #endregion

        #region GetReward(IEnumerable<history> HistoryOfConnect, tarif tariff, DateTime connect_create, DateTime start, DateTime stop)
        /// <summary>
        /// Count reward to trader.
        /// </summary>
        /// <param name="HistoryOfConnect">All history of connect.</param>
        /// <param name="tariff">Current tariff.</param>
        /// <param name="connect_create">Time of creating connect.</param>
        /// <param name="start">Count from this time.</param>
        /// <param name="stop">Count to this time.</param>
        /// <returns></returns>
        private double GetReward(IEnumerable<history> HistoryOfConnect, tarif tariff, DateTime connect_create, DateTime start, DateTime stop)
        {
            try
            {
                double totalProfit = HistoryOfConnect.Sum(h => h.pl);
                double totalPips = HistoryOfConnect.Sum(h => h.pts);

                switch (tariff.type)
                {
                    case 1: // by days
                        DateTime _start = connect_create > start ? connect_create : start;
                        return (stop - _start).TotalDays.Round() / (double)tariff.tsize * tariff.summa;
                    case 2: // by pips
                        return totalPips > 0 ? totalPips / (double)tariff.tsize * tariff.summa : 0;
                    case 3: // by percent
                        return totalProfit > 0 ? totalProfit * tariff.tsize / 100 : 0;
                    default:
                        return 0;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        } 
        #endregion
    }
}
