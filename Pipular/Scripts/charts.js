﻿/// <reference path="teechart.js" />
/// <reference path="teechart-extras.js" />

function toDate() {
    return time = new Date(this.getFullYear(), this.getMonth(), this.getDate());
}

Date.prototype.toDate = toDate;

(function ($) {

    // JSON RegExp
    var rvalidchars = /^[\],:{}\s]*$/;
    var rvalidescape = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g;
    var rvalidtokens = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g;
    var rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g;
    var dateISO = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:[.,]\d+)?Z/i;
    var dateNet = /\/Date\((\d+)(?:-\d+)?\)\//i;

    // replacer RegExp
    var replaceISO = /"(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(?:[.,](\d+))?Z"/i;
    var replaceNet = /"\\\/Date\((\d+)(?:-\d+)?\)\\\/"/i;

    // determine JSON native support
    var nativeJSON = (window.JSON && window.JSON.parse) ? true : false;
    var extendedJSON = nativeJSON && window.JSON.parse('{"x":9}', function (k, v) { return "Y"; }) === "Y";
    var offset = new Date().getTimezoneOffset()/60;

    var jsonDateConverter = function (key, value) {
        if (typeof (value) === "string") {
            if (dateISO.test(value)) {
                return new Date(value);
            }
            if (dateNet.test(value)) {
                var time = new Date(parseInt(dateNet.exec(value)[1], 10));
                time.setHours(offset);
                return time;
            }
        }
        return value;
    };

    $.extend({
        parseJSON: function (data, convertDates) {
            /// <summary>Takes a well-formed JSON string and returns the resulting JavaScript object.</summary>
            /// <param name="data" type="String">The JSON string to parse.</param>
            /// <param name="convertDates" optional="true" type="Boolean">Set to true when you want ISO/Asp.net dates to be auto-converted to dates.</param>

            if (typeof data !== "string" || !data) {
                return null;
            }

            // Make sure leading/trailing whitespace is removed (IE can't handle it)
            data = $.trim(data);

            // Make sure the incoming data is actual JSON
            // Logic borrowed from http://json.org/json2.js
            if (rvalidchars.test(data
                .replace(rvalidescape, "@")
                .replace(rvalidtokens, "]")
                .replace(rvalidbraces, ""))) {
                // Try to use the native JSON parser
                if (extendedJSON || (nativeJSON && convertDates !== true)) {
                    return window.JSON.parse(data, convertDates === true ? jsonDateConverter : undefined);
                }
                else {
                    data = convertDates === true ?
                        data.replace(replaceISO, "new Date(parseInt('$1',10),parseInt('$2',10)-1,parseInt('$3',10),parseInt('$4',10),parseInt('$5',10),parseInt('$6',10),(function(s){return parseInt(s,10)||0;})('$7'))")
                            .replace(replaceNet, "new Date($1)") :
                        data;
                    return (new Function("return " + data))();
                }
            } else {
                $.error("Invalid JSON: " + data);
            }
        }
    });
})(jQuery);

function accumulate(data, field) {
    var count = 0;
    for (var item in data) {
        if (data[item].hasOwnProperty(field)) {
            count += data[item][field];
            data[item][field] = count;
            data[item]['equity'] = count - data[item]['drawdown'];
        }
    }
    return data;
}

function evalUnixTime(data, field) {
    for (var item in data) {
        if (data[item].hasOwnProperty(field)) {
            data[item][field] = new Date(data[item][field]*1000);
        }
    }
    return data;
}

function drawSystemChart(data, canvas, profit_chart_button, pips_chart_button) {
    var profitButton = $('#' + profit_chart_button);
    var pipsButton = $('#' + pips_chart_button);
    
    var Chart = new Tee.Chart(canvas);

    Chart.title.text = "";
    Chart.axes.left.ticks.length = 0;
    Chart.axes.left.format.stroke.size = 1;
    Chart.axes.left.format.stroke.fill = "#555";
    Chart.axes.bottom.labels.dateFormat = "mmm d 'yy\nHH:MM";
    Chart.axes.bottom.grid.visible = false;
    Chart.axes.bottom.format.stroke.size = 1;
    Chart.axes.bottom.format.stroke.fill = "#555";

    Chart.panel.transparent = true;
    Chart.panel.margins.bottom = 3;
    Chart.walls.back.visible = false;
    Chart.legend.visible = false;

    Chart.zoom.wheel.enabled = true;
    Chart.zoom.enabled = true;
    Chart.zoom.direction = "both";
    Chart.zoom.mouseButton = 2;
    Chart.scroll.mouseButton = 0;
    Chart.scroll.direction = "both";

    var chartData = $.parseJSON(window.JSON.stringify(data), true);
    
    var equity = Chart.addSeries(new Tee.Area(Chart));
    var drawdown = Chart.addSeries(new Tee.Bar(Chart));
    var profit_line = new Tee.Line(Chart);
    profit_line.stairs = false;
    var profit = Chart.addSeries(profit_line);

    var eqpips = Chart.addSeries(new Tee.Area(Chart));
    var ddpips = Chart.addSeries(new Tee.Bar(Chart));
    var pips_line = new Tee.Line(Chart);
    pips_line.stairs = false;
    var pips = Chart.addSeries(pips_line);
    
    equity.title = "Equity";
    equity.format.fill = "red";
    equity.format.shadow.visible = false;
    equity.format.transparency = 0.5;
    equity.format.gradient.visible = true;
    equity.format.gradient.colors = ["yellow", "orange", "red"];
    equity.format.gradient.stops = [0, 0.5, 1];
    
    profit.title = "Profit";
    profit.format.stroke.size = 2;
    profit.format.stroke.fill = "darkorange";
    profit.pointer.visible = true;
    if (data.profit.length < 30) {
        profit.pointer.setSize(15, 15);
    } else {
        profit.pointer.setSize(5, 5);
    }
    profit.pointer.height = 6;
    profit.pointer.width = 6;
    profit.pointer.style = "ellipse";
    profit.pointer.format.fill = "#fff";
    profit.pointer.format.stroke.fill = "darkorange";
    profit.hover.stroke.size = 1;
    profit.sideMargins = 30;

    drawdown.title = "Drawdown";
    drawdown.barSize = 90;
    drawdown.marks.visible = false;
    drawdown.format.gradient.visible = false;
    drawdown.format.stroke.fill = "transparent";
    drawdown.format.shadow.visible = false;
    drawdown.format.transparency = 0.3;
    drawdown.format.round.x = 0;
    drawdown.format.round.y = 0;
    drawdown.hover.stroke.size = 1;
    drawdown.colorEach = false;
    drawdown.offset = 0;

    eqpips.title = "Equity pips";
    eqpips.format.fill = "#FFEFBC";
    eqpips.format.shadow.visible = false;
    eqpips.format.transparency = 0.5;
    eqpips.format.gradient.visible = true;
    eqpips.format.gradient.colors = ["#003D7C", "#33597E", "#FFEFBC"];
    eqpips.format.gradient.stops = [0, 0.5, 1];

    pips.title = "Pips";
    pips.format.stroke.size = 2;
    pips.format.stroke.fill = "#9C001A";
    pips.pointer.visible = true;
    if (data.profit.length < 30) {
        pips.pointer.setSize(15, 15);
    } else {
        pips.pointer.setSize(5, 5);
    }
    pips.pointer.height = 6;
    pips.pointer.width = 6;
    pips.pointer.style = "ellipse";
    pips.pointer.format.fill = "#fff";
    pips.pointer.format.stroke.fill = "darkorange";
    pips.hover.stroke.size = 1;
    pips.sideMargins = 30;

    ddpips.title = "Drawdown pips";
    ddpips.barSize = 90;
    ddpips.marks.visible = false;
    ddpips.format.gradient.visible = false;
    ddpips.format.stroke.fill = "transparent";
    ddpips.format.shadow.visible = false;
    ddpips.format.transparency = 0.3;
    ddpips.format.round.x = 0;
    ddpips.format.round.y = 0;
    ddpips.hover.stroke.size = 1;
    ddpips.hover.stroke.fill = "#621A7E";
    ddpips.hover.shadow.color = "#04FBFF";
    ddpips.colorEach = false;
    ddpips.offset = 0;

    if (chartData.profit.length == 0) {
        noDataChart(Chart)
    }
    else {

        equity.data.x = [];
        profit.data.x = [];
        drawdown.data.x = [];
        eqpips.data.x = [];
        pips.data.x = [];
        ddpips.data.x = [];

        var start_date;
        if (chartData.drawdown.length > 0 && chartData.drawdown[0].date.valueOf() < chartData.profit[0].date.valueOf())
            start_date = chartData.drawdown[0].date;
        else start_date = chartData.profit[0].date;

        var end_date;
        if (chartData.drawdown.length > 0 && chartData.drawdown[chartData.drawdown.length - 1].date.valueOf() > chartData.profit[chartData.profit.length - 1].date.valueOf())
            end_date = chartData.drawdown[chartData.drawdown.length - 1].date;
        else end_date = chartData.profit[chartData.profit.length - 1].date;

        var curr_date = new Date(start_date);
        var start_eq = chartData.drawdown.length > 0 ? chartData.drawdown[0].date : new Date(); 
        var j = 0, k = 0;

        while (curr_date <= end_date) {

            if (chartData.profit[j] != null && chartData.profit[j].date.valueOf() == curr_date.valueOf()) {       
                profit.data.values.push(chartData.profit[j].pl);
                pips.data.values.push(chartData.profit[j].pts);

                profit.data.x.push(new Date(curr_date));
                pips.data.x.push(new Date(curr_date));

                j++;
            }

            if (curr_date >= start_eq && k < chartData.drawdown.length) {
    
                if (chartData.drawdown[k].date.valueOf() == curr_date.valueOf()) {

                    equity.data.values.push((j == 0 ? 0 : chartData.profit[j-1].pl) + chartData.drawdown[k].dd);
                    equity.data.x.push(new Date(curr_date));

                    eqpips.data.values.push((j == 0 ? 0 : chartData.profit[j - 1].pts) + chartData.drawdown[k].dd_pip);
                    eqpips.data.x.push(new Date(curr_date));

                    /*
                    var diff = Math.abs(profit.data.values[profit.data.values.length - 2] - profit.data.values[profit.data.values.length - 1]);
                    if (diff > 0 || chartData.drawdown[k].dd > diff)
                        equity.data.values.push(profit.data.values[profit.data.values.length - 2] + chartData.drawdown[k].dd);
                    else equity.data.values.push(chartData.profit[j].pl + chartData.drawdown[k].dd);
                    equity.data.x.push(new Date(curr_date));

                    var diff_pip = Math.abs(pips.data.values[pips.data.values.length - 2] - pips.data.values[pips.data.values.length - 1]);
                    if (diff_pip > 0 || chartData.drawdown[k].dd_pip > diff_pip)
                        eqpips.data.values.push(pips.data.values[pips.data.values.length - 2] + chartData.drawdown[k].dd_pip);
                    else eqpips.data.values.push(chartData.profit[j].pts + chartData.drawdown[k].dd_pip);
                    eqpips.data.x.push(new Date(curr_date));
                    */
                    drawdown.data.values.push(chartData.drawdown[k].dd);
                    drawdown.data.x.push(new Date(curr_date));
                    
                    ddpips.data.values.push(chartData.drawdown[k].dd_pip);
                    ddpips.data.x.push(new Date(curr_date));

                    k++;
                }
                else {

                    drawdown.data.values.push(0);
                    drawdown.data.x.push(new Date(curr_date));

                    ddpips.data.values.push(0);
                    ddpips.data.x.push(new Date(curr_date));
                }
            }
            
            curr_date.setDate(curr_date.getDate() + 1);
        }

        if (profit.data.x.length == 0) Chart.removeSeries(profit);
        if (equity.data.x.length == 0) Chart.removeSeries(equity);
        if (drawdown.data.x.length == 0) Chart.removeSeries(drawdown);
        else {
            for (var g = 0; g < 5; g++) {
                drawdown.data.x.push(new Date(drawdown.data.x[drawdown.data.x.length - 1].getTime() + 1000000));
                drawdown.data.values.push(0);
            }
        }

        if (pips.data.x.length == 0) Chart.removeSeries(pips);
        if (eqpips.data.x.length == 0) Chart.removeSeries(eqpips);
        if (ddpips.data.x.length == 0) Chart.removeSeries(ddpips);
        else {
            for (var g = 0; g < 5; g++) {
                ddpips.data.x.push(new Date(ddpips.data.x[ddpips.data.x.length - 1].getTime() + 1000000));
                ddpips.data.values.push(0);
            }
        }

        tip = new Tee.ToolTip(Chart);
        tip.render = "canvas";

        Chart.tools.add(tip);

        tip.format.fill = "#555";
        tip.format.transparency = 0.3;
        tip.format.font.fill = "black";
        //tip.format.gradient.colors = []
        tip.format.gradient.visible = true;
        //tip.format.font.textAlign = "left";
        tip.format.stroke.fill = "transparent";
        tip.margins.top = 3;
        tip.format.round.x = 0;
        tip.format.round.y = 0;
        //tip.transparent = true;

        tip.ongettext = function (tool, text, series, index) {
            var value = series.data.values[index];
            if (series.title == "Pips" || series.title == "Equity pips") value = value.toFixed(1);
            else value = value.toFixed(2);
            return series.data.x[index].format("mmm d, yyyy") + '\r\n' + series.title + ': ' + value;
        }
    }

    // buttons
    profitButton.click(function () {
        Chart.axes.left.title.text = "Profit";
        Chart.axes.left.labels.decimals = 2;
        
        eqpips.visible = false;
        ddpips.visible = false;
        pips.visible = false;

        equity.visible = true;
        drawdown.visible = true;
        profit.visible = true;

        pipsButton.button('enable');
        profitButton.button('disable');

        Chart.draw();

        profitButton.hide();
        pipsButton.show();
    });

    pipsButton.click(function () {
        Chart.axes.left.title.text = "Pips";
        Chart.axes.left.labels.decimals = 1;

        eqpips.visible = true;
        ddpips.visible = true;
        pips.visible = true;

        equity.visible = false;
        drawdown.visible = false;
        profit.visible = false;

        pipsButton.button('disable');
        profitButton.button('enable');

        Chart.draw();

        profitButton.show();
        pipsButton.hide();
    });

    $('#' + profit_chart_button).click();
}

function hideAxis(a) {
    a.format.stroke.fill = "";
    a.labels.visible = false;
    a.grid.visible = false;
    a.ticks.visible = false;
}

function cleanChart(c) {
  c.title.visible=false;
  c.panel.transparent=true;
  c.legend.visible=false;
  c.walls.back.visible=false;

  hideAxis(c.axes.left);
  hideAxis(c.axes.bottom);
}

function noDataChart(c) {
    a1 = new Tee.Annotation(c);
    a1.format.fill = "white";
    a1.format.font.style = "12px Tahoma";
    a1.format.font.fill = "black";
    a1.text = "Not enough data";
    a1.position.x = c.chartRect.width / 2 - 55;
    a1.position.y = c.chartRect.height / 2 - 10;
    a1.transparent = true;

    c.tools.add(a1);
}

function drawMiniChart(id, data, colors) {
    chart = new Tee.Chart(id);
    cleanChart(chart);

    chart.aspect.clip = false;
    chart.panel.margins.top = 20;
    chart.panel.margins.bottom = 20;

    if (data.length < 2) {
        noDataChart(chart);
    }
    else {

        var bar = new Tee.Area(chart);

        bar.colorEach = false;
        bar.marks.visible = false;
        bar.hover.enabled = false;

        bar.format.shadow.visible = false;
        bar.format.fill = "fff";
        
        bar.format.stroke.size = 1;

        bar.format.gradient.visible = true;
        bar.format.gradient.stops = [0, 0.99, 1];
        bar.format.gradient.direction = "bottomtop";

        var profit = chart.addSeries(bar);
        var graphWidth = $("#" + id).attr('width');
        var barWidth = 10;
        var period = 1; // days

        profit.data.x = [];
        /*
        var first_date = new Date(data[0].date);
        var last_date = new Date(data[data.length - 1].date);
        var next_date = new Date(last_date);
        var date = new Date(last_date);
        var pts;
        var j = data.length - 1;

        period = Math.ceil((last_date - first_date) / 24 / 60 / 60 / 1000 / graphWidth * barWidth);

        while (date > first_date && j > 0) {
            pts = data[j].pts;

            profit.data.values.push(pts);
            profit.data.x.push(new Date(date));
            
            next_date.setDate(next_date.getDate() - period);

            while (j >= 0 && data[j].date > next_date && data[j].date <= date) {
                j--;
            }
        

            date = new Date(next_date);
        }
        */
        var maxProfit = data[0].pts;
        var prof = data[data.length - 1].pts;

        for (var i = data.length - 1; i >= 0; i--) {

            maxProfit = data[i].pts > maxProfit ? data[i].pts : maxProfit;

            profit.data.values.push(data[i].pts);
            profit.data.x.push(data[i].date);
        }

        if (colors == undefined || colors.length < 2) {

            switch (true) {
                case (prof == maxProfit && prof > 0):
                    colors = ["#89b103", "#abd900", "#ffffff"]; // green
                    break;
                case (prof > maxProfit):
                    colors = ["#4b91b5", "#6bc9ed", "#ffffff"]; // blue
                    break;
                case (prof < 0):
                    colors = ["#fb9b00", "#ffb51d", "#ffffff"]; // orange
                    break;
                default:
                    colors = ["#4b91b5", "#6bc9ed", "#ffffff"]; // blue
                    break;
            }
        }

        bar.format.gradient.colors = colors;
        bar.format.stroke.fill = colors[0];

        /*
        cursor = chart.tools.add(new Tee.CursorTool(chart));
        cursor.render = "full";
        cursor.direction = "vertical";
        */
    }
    chart.draw();
}