﻿/// <reference path="../jquery-1.4.4-vsdoc.js" />
/// <reference path="../jquery.validate-vsdoc.js" />
/// <reference path="../jquery.validate.unobtrusive.js" />

/// Copyright (c) 2011 Andrey Veselov. All rights reserved.
/// WebSite: http://andrey.moveax.ru 
/// Email: andrey@moveax.ru
/// This source is subject to the Microsoft Public License (Ms-PL).

jQuery.validator.addMethod("equal", function (value, element, param) {
    if ($(element).attr("type") == "checkbox") {
        value = String($(element).attr("checked"));
        param = param.toLowerCase();
    }

    return (value == param);
});

jQuery.validator.unobtrusive.adapters.addSingleVal("equal", "valuetocompare");