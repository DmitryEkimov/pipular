﻿/// <reference path="jquery-1.9.1-vsdoc.js" />
/// <reference path="jquery-ui-vsdoc_1.js" />

/* ARRAYS */

if ([].indexOf) {

    var find = function (array, value) {
        return array.indexOf(value);
    }

} else {
    var find = function (array, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] === value) return i;
        }

        return -1;
    }

}

/* TIME */

function getTime() {
    var date = new Date();
    var utc = date.getTime();
    var zone = date.getTimezoneOffset() * 60 * 1000;
    return utc - zone;
}

//setCookie("zone", new Date().getTimezoneOffset(), "01-Jan-2100 00:00:00 GMT", "/");

function getClientWidth() {
    return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientWidth : document.body.clientWidth;
}

function getClientHeight() {
    return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientHeight : document.body.clientHeight;
}

/* PANEL MOVE */

var menuMaxWidth = 255, menuMinWidth = 85;

function moveContentRight(done) {
    $('body').stop();
    $('body').animate({ paddingLeft: menuMaxWidth }, 1000, 'easeOutCubic', done);
}
function moveContentLeft(done) {
    $('body').stop();
    $('body').animate({ paddingLeft: menuMinWidth }, 1000, 'easeOutCubic', done);
}

// POPUP
function loading() {
    $('#popup #popup_content').html('<div class="circle_loader"></div>');
}

function OpenPopup(title, width) {
    loading();
    $('#popup').dialog({
        modal: true,
        title: title,
        width: width,
        resizable: false,
        position: [getClientWidth()/2 - width/2, 100]
    });
}

function ClosePopup(title, width) {
    $('#popup').dialog('close');
}

function Error(element, message) {
    $(element).html(message);
}

/* ALERT */

function Message() {
    $('#alert').slideDown(500);
    setTimeout(function () { $('#alert').slideUp(500); }, 5000);
}

function fail(selector) {
    var precolor = $(selector).css('border-color');
    $(selector).css({ borderColor: 'Red' }).animate({
        borderColor: precolor
    }, 3000);
}

/* COOCKIES */

function saveToCookieElement(id) {
    var cookies = $.cookie("hiddenElements");
    var chMap = cookies == null || cookies == "" ? [] : cookies.split(',');

    if (chMap.indexOf(id) >= 0) {
        chMap.splice(chMap.indexOf(id), 1);
    }
    else chMap.push(id);

    $.cookie("hiddenElements", chMap.join(','));
}

function readCookieElements() {
    var cookies = $.cookie("hiddenElements");
    var chMap = cookies == null || cookies == "" ? [] : cookies.split(',');

    for (var i = 0; i < chMap.length; i++) {
        $('#' + chMap[i]).hide();
    }
}

/* EXPANDERS */

(function ($) {
    $.fn.expander = function (options) {
        var settings = $.extend({
            'expandedId': '',
            'openWord': 'show',
            'closeWord': 'hide',
            'speed': 500,
            'closed': false
        }, options);

        return this.each(function () {
            var _expander = $(this);
            var id = _expander.data('expandedid');
            settings.expandedId = id;
            var expElem = $('#' + settings.expandedId);

            if (settings.closed) expElem.hide();

            if (expElem.css('display') == 'none') _expander.text(settings.openWord);
            else _expander.text(settings.closeWord);            

            _expander.click(function () {
                if (expElem.css('display') == 'none') {
                    expElem.slideDown(settings.speed);
                    _expander.text(settings.closeWord);
                }
                else {
                    expElem.slideUp(settings.speed);
                    _expander.text(settings.openWord);
                }

                saveToCookieElement(settings.expandedId);
            });
        });
    }
})(jQuery);

(function ($) {
    var countCallers = 0,
        callersDone = 0;

    $.fn.expanderIcon = function (options) {
        var settings = $.extend({
            'openClass': 'icon-arrow-down',
            'closeClass': 'icon-arrow-up',
            'speed': 500,
            'loadOnce': true
        }, options);
        countCallers = this.length;

        return this.each(function () {
            var canLoad = true;
            var _expander = $(this);
            var id = _expander.data('expandedid');
            var done = _expander.data('done');
            var open = _expander.data('open');
            var loadTo = _expander.data('loadto');
            var firstLoadDone = _expander.data('first-load-done');
            var onDone = done ? new Function(done) : function () { };
            var onOpen = open ? new Function(open) : function () { };
            var onFirstLoadDone = firstLoadDone ? new Function(firstLoadDone) : function () { };
            var expandedId = id;
            var icon = _expander.find('i');
            var expElem = $('#' + expandedId);
            
            if (expElem.css('display') == 'none') {
                icon.addClass(settings.openClass);
                countCallers--;
            }
            else {
                icon.addClass(settings.closeClass);

                if ((expElem.children().length == 0 && open) || (loadTo && $("#" + loadTo).children().length == 0)) {
                    onOpen().done(function () {
                        expElem.slideDown(settings.speed, function () {
                            callersDone++;
                            if (countCallers == callersDone)
                                onDone();
                        });
                        icon.removeClass(settings.openClass);
                        icon.addClass(settings.closeClass);
                        onFirstLoadDone();
                    });
                    if (settings.loadOnce) canLoad = false
                }
            }

            _expander.click(function () {
                if (expElem.css('display') == 'none') {
                    if (open && canLoad) {
                        onOpen().done(function () {
                            expElem.slideDown(settings.speed, onDone);
                            icon.removeClass(settings.openClass);
                            icon.addClass(settings.closeClass);
                            onFirstLoadDone();
                        });
                        if (settings.loadOnce) canLoad = false
                    }
                    else {
                        expElem.slideDown(settings.speed, onDone);
                        icon.removeClass(settings.openClass);
                        icon.addClass(settings.closeClass);
                    }
                }
                else {
                    expElem.slideUp(settings.speed, onDone);
                    icon.removeClass(settings.closeClass);
                    icon.addClass(settings.openClass);
                }

                saveToCookieElement(expandedId);
            });
        });
    }
})(jQuery);

function iconToogle(tag, icon1, icon2) {
    var icon = tag.find('i');
    if (icon.hasClass(icon1)) {
        icon.removeClass(icon1);
        icon.addClass(icon2);
    }
    else {
        icon.removeClass(icon2);
        icon.addClass(icon1);
    }
}

function expand(id, func) {
    var elem = $(id);
    elem.slideToggle(500, func);
}

function expand(id, caller) {
    var elem = $(id);
    function _text() { textToogle($(caller), 'hide', 'show') };
    elem.slideToggle(500, _text);
    saveToCookieElement(id);
}

function expandIcon(id, caller) {
    var elem = $(id),
        dfd = $.Deferred();

    function icon() {
        iconToogle($(caller), 'icon-arrow-up', 'icon-arrow-down');
        dfd.resolve();
    };
    elem.slideToggle(500, icon);

    return dfd.promise();
}

function invisible(elements) {
    $(elements).each(function () {
        $(this).css('visibility', 'collapse');
        $(this).mouseenter(function () { $(this).css('visibility', 'visible'); });
        $(this).mouseout(function () { $(this).css('visibility', 'collapse'); });
    });
}


/* SLIDE */

var slideOpened = false;
var slideTime = 500;

function slideRightToggle(elem) {
    if (slideOpened) slideOpened = false; else slideOpened = true;
    $(elem).toggle('slide', { direction: 'right' }, slideTime);
}

function slideRightHide(elem) {
    slideOpened = false;
    $(elem).hide('slide', { direction: 'right' }, slideTime);
}

function slideRightShow(elem) {
    slideOpened = true;
    $(elem).show('slide', { direction: 'right' }, slideTime);
}

function slideRightHideAddit() { if (slideOpened) slideRightHide('#addit'); }
function slideRightShowAddit() { if (!slideOpened) slideRightShow('#addit'); }

function closeAddit() { slideRightHideAddit(); }

/**/

function replaceTo(element) {
    $('#popup_content').children().appendTo(element).hide().slideDown(500);
}

function updateTo(element) {
    $('#popup_content').children().replaceAll(element).hide().fadeIn(500);
}

function deleteElement(element, callback) {
    $(element).fadeOut(500, function () { $(this).remove(); if(callback != undefined) callback(); });
}

function deleteIfGood(element, callback) {
    if ($('#alert').find('.good').length > 0)
        deleteElement(element, callback);
}

/* TABS */

(function ($) {
    $.fn.notebook = function (options) {
        var settings = $.extend({
            'fixWidth': false,
            'fixHeight': false
        }, options);

        return this.each(function () {
            var $this = $(this);
            var win = $this.context.ownerDocument.defaultView;
            var tabs = $this.find('.tab');
            var panels = $this.find('.tab_panel');

            tabs.click(function () {
                var id = $(this).attr('data-id');

                tabs.filter('.selected').removeClass('selected');
                $(this).addClass('selected');

                panels.filter('.current').removeClass('current');
                $(id).addClass('current');
            });

            if (settings.fixWidth) {
                var h = panels.maxHeight();
                panels.height(h);
            }
        });
    }
})(jQuery);

(function ($) {
    $.fn.maxHeight = function () {
        var max = 0;

        this.each(function () {
            max = Math.max(max, $(this).height());
        });

        return max;
    };
})(jQuery);

(function ($) {

    $.fn.maxWidth = function () {

        var max = 0;

        this.each(function () {
            max = Math.max(max, $(this).width());
        });

        return max;
    };
})(jQuery);


(function ($) {
    $.fn.disable = function () {
        return this.each(function () {
            $(this).attr("disabled", "disabled");
        });
    };
})(jQuery);

(function ($) {
    $.fn.enable = function () {
        return this.each(function () {
            $(this).removeAttr("disabled");
        });
    };
})(jQuery);

/* COLUMNS */

(function ($) {

    var settings,
        container,
        childrens,
        childs,
        childHeight,
        colCount,
        maxHeight = 0;

    var totalWidth = function (elements) {
        var res = 0;

        for (var e = 0; e < elements.length; e++) {
            res += $(elements[e]).outerWidth(true);
        }

        return res;
    }

    var columnCount = function (elements) {
        var res = 0,
            count = 0,
            containerWidth = container.width();

        for (var e = 0; e < elements.length; e++) {
            res += $(elements[e]).outerWidth(true);
            if (res > containerWidth) return count;
            else count++;
        }

        return res;
    }

    var getMaxHeight = function (elements) {
        var max = 0,
            height = 0;

        for (var e = 0; e < elements.length; e++) {
            height = $(elements[e]).height();
            max = height > max ? height : max;
        }

        return max;
    }

    var methods = {
        init: function (options) {
            settings = $.extend({
            }, options);

            container = this;
            childrens = container.children();
            childsNum = childrens.length;
            childWidth = totalWidth(childrens);

            return this.each(function () {

                /* create columns */
                for (var c = 0; c < 6; c++) {
                    $('<div class="column" id="column' + c + '" style="float:left"></div>').appendTo(container);
                }

                var onResize = function () {
                    methods["optimize"].apply(container);
                }

                $(window).on('resize', $.debounce(250, onResize));
            });
        },
        optimize: function () {
            return this.each(function () {
                colCount = columnCount(childrens);
                maxHeight = getMaxHeight(childrens);

                for (var e = 0, c = 0; e < childsNum && c < colCount; e++) {
                    $(childrens[e]).appendTo('#column' + c);
                    if (($('#column' + c).height() > maxHeight / 2 ) && //|| (colCount - c) >= (childsNum - e))
                        (colCount > c + 1)) {
                        if ($('#column' + c).children().filter(':last').find('.content').css('display') != 'none')
                            c++;
                    }
                }
            });
        }
    };

    $.fn.columns = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для jQuery.columns');
        }
    };
})(jQuery);

/* LINKEDRADIO */

(function () {
    $.fn.linkedRadio = function () {
        var linkedElements = [];

        return this.each(function () {
            var activefor = $("#" + $(this).data("activefor"));
            linkedElements.push(activefor);
            activefor.disable();

            if (this.checked) activefor.enable();

            $(this).change(function () {
                $(linkedElements).disable();
                if (this.checked) activefor.enable();
            });
        });
    }
})(jQuery);

/* DARKBOX */

(function () {
    $.fn.darkbox = function () {

        var createDarkbox = function (_parentbox, id) {
            return $('<div class="darkbox" id="' + id + '"><div class="darkbox_loading"></div></div>').prependTo(_parentbox);
        }

        var activateDarkbox = function (_parentbox, _dbox) {
            _dbox.width(_parentbox.width());
            _dbox.height(_parentbox.height());
            _dbox.show();
        }

        var diactivateDarkbox = function (_dbox) {
            _dbox.hide();
        }

        return this.each(function () {
            var parentbox = $(this);
            var dbox_id = (parentbox.attr("id") + "_dbox");
            var dbox = createDarkbox(parentbox, dbox_id);

            parentbox.addClass("darkbox_owner");

            parentbox.find("a[data-ajax]").attr("data-ajax-loading", '#' + dbox_id);
            dbox.width(parentbox.width());
            dbox.height(parentbox.height());
        });
    }
})(jQuery);

/* MISC */

$(document).ready(function () {
    $('#menu_panel').on('mouseenter', moveContentRight);
    $('#menu_panel').on('mouseleave', moveContentLeft);

    $('#main').click(slideRightHideAddit);

    readCookieElements();
    $('.expander').each(function () { $(this).expander() });
    $('.expanderIcon').expanderIcon();
});