﻿/// <reference path="jquery-1.7.2-vsdoc.js" />
/// <reference path="jquery-ui-vsdoc_1.js" />
/// <reference path="common.js" />


// New tariff popup
function openNewTariff(system_name, system_id) {
    OpenPopup('Add new tariff for ' + system_name, 500);

    $.ajax({
        cache: false,
        url: 'NewTariff',
        data: 'id=' + system_id,
        beforeSend: function () { },
        success: function (result) { $('#popup #popup_content').html(result); }
    });
}

$(document).ready(function () {
    $('#portfolios .sortable, #noportfolios .sortable').sortable({
        connectWith: '.sortable',
        revert: 200,
        tolerance: 'pointer',
        helper: 'clone',
        start: function (event, ui) {
            start_portfolio = $(this);
            keyPressed = false;
            if (event.ctrlKey) {
                keyPressed = true;
                ui.item.show();
            }
        },
        receive: function (event, ui) {
            var account = $(ui.item).parent().parent().parent().attr('id');
            var id = $(ui.item).attr('data-connect');
            var url = 'Private/ChangePortfolio/';
            var portfolio_block = $(this).parent().parent();

            if (account == 'noportfolios') account = '0';

            var receive_system = ui.item.attr('data-system');

            if (account != '0' && $(portfolio_block).find('[data-system="' + receive_system + '"]').length > 1) {
                $(ui.sender).sortable('cancel');
                fail(portfolio_block);
            }
            else {
                if (keyPressed) {
                    url = 'Private/ConnectCopy/';
                    ui.item.clone().appendTo(start_portfolio);
                }

                $.ajax({
                    cache: false,
                    url: url + id,
                    data: 'account=' + account,
                    success: function (result) {
                        $(ui.item).replaceWith(result);
                    }
                });
            }
        }
    });

    // Configuration popup
    function openConfig(connect_id, title) {
        OpenPopup(title, 500);

        $.ajax({
            cache: false,
            url: 'Private/ConnectConfiguration/' + connect_id,
            beforeSend: function () { },
            success: function (result) { $('#popup #popup_content').html(result); }
        });
    }

    function portfolios(portfolios) {

        $(portfolios).each(function (i) {
            var portfolio = $(this);
            var id = $(portfolio).attr('id');

            $(portfolio).find('.public span').click(function () {
                var span = $(portfolio).find('.public span');
                $.ajax({
                    cache: false,
                    url: 'Private/PortfolioSwitch/' + id,
                    success: function () {
                        if ($(span).attr('title') == 'public') {
                            $(span).attr('title', 'not public');
                            $(span).css('color', '#696969');
                        }
                        else {
                            $(span).attr('title', 'public');
                            $(span).css('color', 'LimeGreen');
                        }
                    }
                });
            });
        });
    }

    portfolios('#portfolios .portfolio_block');

    $('#addSystem').button().click(function () {
        openNewSystem();
    });
});