﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc.Ajax;

namespace Pipular
{
    public static class Constants
    {
        public static string siteName = "FXBuilders Ltd.";

        public static AjaxOptions additAjaxOptions = new AjaxOptions()
        {
            UpdateTargetId = "addit_content",
            OnBegin = "slideRightHideAddit()",
            OnComplete = "slideRightToggle('#addit')"
        };
    }
}