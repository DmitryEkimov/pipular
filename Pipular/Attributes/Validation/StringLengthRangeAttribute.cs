﻿/// Copyright (c) 2011 Andrey Veselov. All rights reserved.
/// WebSite: http://andrey.moveax.ru 
/// Email: andrey@moveax.ru
/// This source is subject to the Microsoft Public License (Ms-PL).

namespace Pipular.Attributes.Validation
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class StringLengthRangeAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly int _minLength;
        private readonly int _maxLength;

        public StringLengthRangeAttribute(int minLength, int maxLength)
            : base("{0} length must be between {1} and {2}")
        {
            this._minLength = minLength;
            this._maxLength = maxLength;
        }

        #region ValidationAttribute overrides

        public override string FormatErrorMessage(string name)
        {
            return string.Format(
                this.ErrorMessageString, name, this._minLength, this._maxLength);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (this._minLength >= this._maxLength) {
                throw new ArgumentException();
            }

            string stringValue = value as string;
            if (stringValue == null) {
                return ValidationResult.Success;
            }

            if ((stringValue.Length < this._minLength) ||
                (stringValue.Length > this._maxLength)) {
                return new ValidationResult(
                    this.FormatErrorMessage(validationContext.DisplayName));
            }

            return ValidationResult.Success;
        }

        #endregion

        #region IClientValidatable Members

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(
            ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationStringLengthRule(
                this.FormatErrorMessage(metadata.DisplayName),
                this._minLength,
                this._maxLength);
        }

        #endregion
    }
}