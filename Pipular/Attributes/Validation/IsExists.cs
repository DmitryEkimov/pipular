﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pipular.Models;

namespace Pipular.Attributes.Validation
{
    public class IsExists
    {
        pipularEntities DB = new pipularEntities();

        public bool Login(string login) { return DB.users.Any(m => m.login == login); }

        public bool Email(string email) { return DB.users.Any(m => m.email == email); }
    }
}