﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pipular.Models;
using Pipular.Models.Charts;

namespace Pipular
{
    public static class SignalInfo
    {
        static pipularEntities DB = new pipularEntities();
        
        public static void GetInfo(signal signal, out LiveTradeSignal live_signal, out LiveSystemModel live_system)
        {
            user trader = null;
            live_signal = null;
            live_system = null;
            double profit_num = 0;
            
            system system = ((!DB.systems.Any(s => s.nmbr == signal.system)) ? null : DB.systems.First(s => s.nmbr == signal.system)); 

            if (system != null)
            {
                trader = ((!DB.users.Any(u => u.nmbr == system.user)) ? null : DB.users.First(u => u.nmbr == system.user));
                live_system = new LiveSystemModel()
                {
                    name = DB.systems.FirstOrDefault(s => s.nmbr == signal.system).name,
                    data = DB.rsystems.FirstOrDefault(s => s.system == signal.system),
                    profit = from dp in DB.daily_profit
                             where dp.system == signal.system && dp.connect == 0
                             select new ProfitChart()
                                    {
                                        pl = dp.pl,
                                        pts = dp.pts,
                                        date = dp.date
                                    }
                };
            }

            string sl_str = "";
            string tp_str = "";
            LiveTradeSignal.GetModify(signal, out sl_str, out tp_str);

            live_signal = new LiveTradeSignal()
            {
                nmbr = signal.nmbr,
                uid = signal.uid,
                system_id = ((system == null) ? -1 : system.nmbr),
                system = ((system == null) ? "no system" : system.name),
                trader_id = ((trader == null) ? -1 : trader.nmbr),
                trader = ((trader == null) ? "no trader" : trader.login),
                terminal_type = LiveTradeSignal.GetTerminalType(signal.ttype),
                dt_terminal = signal.dt_terminal,
                terminal_time = Extension.UnixToDate(signal.dt_terminal),
                command = LiveTradeSignal.GetCMD(signal.cmd),
                command_nmbr = signal.cmd,
                signal_type = LiveTradeSignal.GetSignalType(signal.stype),
                sl = signal.sl,
                sl_str = sl_str,
                tp = signal.tp,
                tp_str = tp_str,
                symbol = signal.symbol,
                price = LiveTradeSignal.GetPrice(signal),
                profit_str = GetProfit(signal, out profit_num),
                profit = profit_num,
                digits = signal.digits,
                signal_type_nmbr = signal.stype,
                lot = (double)signal.volume / 100,
                public_time = DateTime.Now
            };
        }

        public static string GetProfit(signal signal, out double profit)
        {
            profit = 0;
            string result = "";

            if (signal.cmd < 2 && signal.stype == 2)
            {
                if (signal.cmd == 0) profit = signal.close_price - signal.open_price;
                if (signal.cmd == 1) profit = signal.open_price - signal.close_price;

                if (signal.point == 0.00001 || signal.point == 0.0001) profit *= 10000;
                if (signal.point == 0.001 || signal.point == 0.01) profit *= 100;

                if (profit > 0) result = "+" + profit.ToString("F1");
                else result = profit.ToString("F1");
            }

            return result;
        }
    }
}