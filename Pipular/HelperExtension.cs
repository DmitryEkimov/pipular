﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Mvc.Ajax;
using System.Text;
using System.IO;
using Pipular.Models;
using System.Web.Routing;
using System.Linq.Expressions;

namespace Pipular
{
    public static class HelperExtension
    {
        public static MvcHtmlString Pager(this AjaxHelper ajax, string update_id, int index, int total, string action, object data)
        {
            StringBuilder result = new StringBuilder();

            result.Append("<ul>");
            for (int i = 1; i <= total; i++)
            {
                MvcHtmlString page_link;

                if (i == index) page_link = MvcHtmlString.Create(i.ToString());
                else
                    page_link = ajax.ActionLink(i.ToString(), action, new { data, pageNum = i - 1 }, new AjaxOptions()
                    {
                        InsertionMode = InsertionMode.Replace,
                        UpdateTargetId = update_id
                    }, null);
                
                result.Append("<li>" + page_link.ToString() + "</li>");
            }
            result.Append("</ul>");

            return MvcHtmlString.Create(result.ToString());
        }

        public static MvcHtmlString PageList(this HtmlHelper hmml, int totalPages, string _class, string _id, string innerClass)
        {
            StringBuilder result = new StringBuilder();

            for (int i = 1; i <= totalPages; i++)
            {
                result.Append("<div class=\"" + innerClass + "\">" + i.ToString() + "</div>");
            }

            TagBuilder div = new TagBuilder("div");

            if (_class.Length > 0) div.AddCssClass(_class);
            if (_id.Length > 0) div.MergeAttribute("id", _id);
            div.InnerHtml = result.ToString();

            return MvcHtmlString.Create(div.ToString());
        }

        public static Div Div(this HtmlHelper htmlHelper, string _class, string _id)
        {
            var div = new TagBuilder("div");
            if (_class.Length > 0) div.AddCssClass(_class);
            div.MergeAttribute("id", _id);
            htmlHelper.ViewContext.Writer.WriteLine(div.ToString(TagRenderMode.StartTag));
            return new Div(htmlHelper.ViewContext.Writer);
        }

        public static A BeginActionLink(this AjaxHelper ajaxHelper, string actionName, string controllerName, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes)
        {
            string targetUrl = UrlHelper.GenerateUrl(null, actionName, controllerName, new RouteValueDictionary(routeValues), ajaxHelper.RouteCollection, ajaxHelper.ViewContext.RequestContext, true);
            TagBuilder a = new TagBuilder("a");
            if (htmlAttributes != null)
            {
                var html_attributes = new RouteValueDictionary(htmlAttributes);
                var new_html_attributes = new RouteValueDictionary();
                foreach(var at in html_attributes) { new_html_attributes.Add(at.Key.Replace('_', '-'), at.Value); }
                a.MergeAttributes<string, object>(new_html_attributes);
            }
            a.MergeAttribute("href", targetUrl);
            a.MergeAttributes<string, object>(ajaxOptions.ToUnobtrusiveHtmlAttributes());
            ajaxHelper.ViewContext.Writer.WriteLine(a.ToString(TagRenderMode.StartTag));
            return new A(ajaxHelper.ViewContext.Writer);
        }

        public static A BeginActionLink(this HtmlHelper htmlHelper, string actionName, string controllerName, object routeValues, object htmlAttributes)
        {
            string targetUrl = UrlHelper.GenerateUrl(null, actionName, controllerName, new RouteValueDictionary(routeValues), htmlHelper.RouteCollection, htmlHelper.ViewContext.RequestContext, true);
            TagBuilder a = new TagBuilder("a");
            if (htmlAttributes != null)
            {
                var html_attributes = new RouteValueDictionary(htmlAttributes);
                var new_html_attributes = new RouteValueDictionary();
                foreach (var at in html_attributes) { new_html_attributes.Add(at.Key.Replace('_', '-'), at.Value); }
                a.MergeAttributes<string, object>(new_html_attributes);
            }
            a.MergeAttribute("href", targetUrl);
            htmlHelper.ViewContext.Writer.WriteLine(a.ToString(TagRenderMode.StartTag));
            return new A(htmlHelper.ViewContext.Writer);
        }

        public static MvcHtmlString Expander(this HtmlHelper helper, string id)
        {
            return new MvcHtmlString("<a class=\"expander\" data-expandedId=\"" + id + "\">hide/show</a>");
        }

        public static A ExpanderIcon(this HtmlHelper helper, string id)
        {
            return ExpanderIcon(helper, id, null);
        }

        public static A ExpanderIcon(this HtmlHelper helper, string id, object attributes)
        {
            TagBuilder a = new TagBuilder("a");
            a.Attributes.Add("class", "expanderIcon");
            a.Attributes.Add("data-expandedId", id);
            if (attributes != null)
            {
                RouteValueDictionary attr = new RouteValueDictionary(attributes);
                foreach (var item in attr) a.Attributes.Add(item.Key.Replace('_', '-'), item.Value.ToString()); 
            }
            helper.ViewContext.Writer.WriteLine(a.ToString(TagRenderMode.StartTag));
            return new A(helper.ViewContext.Writer);
        }

        #region CheckBoxLabel<>
        public static MvcHtmlString CheckBoxLabel<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, bool>> express)
        {
            string checkbox;
            string label;

            checkbox = InputExtensions.CheckBoxFor<TModel>(helper, express).ToString();
            label = helper.LabelFor(express).ToString();

            return MvcHtmlString.Create(checkbox.Insert(checkbox.IndexOf("<input", 10), label));
        }

        public static MvcHtmlString CheckBoxLabel<TModel>(this HtmlHelper<TModel> helper, string name, string text, object htmlAttributes)
        {
            string checkbox;
            string label;

            checkbox = InputExtensions.CheckBox(helper, name, htmlAttributes).ToString();
            label = "<label for=\"" + name + "\">" + text + "</label>";

            return MvcHtmlString.Create(checkbox.Insert(checkbox.IndexOf("<input", 10), label));
        }

        public static MvcHtmlString CheckBoxLabel<TModel>(this HtmlHelper<TModel> helper, string name, string text)
        {
            return CheckBoxLabel(helper, name, text, null);
        } 
        #endregion

        #region RadioButtonLabelFor<>
        public static MvcHtmlString RadioButtonLabelFor<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            string text,
            Expression<Func<TModel, TProperty>> express,
            object value,
            object attr,
            bool @checked,
            bool disabled)
        {
            string id = "";
            IDictionary<string, object> htmlAttributes = attr == null ? new RouteValueDictionary() : new RouteValueDictionary(attr);
            if (@checked) htmlAttributes.Add("checked", "checked");
            if (disabled) htmlAttributes.Add("disabled", "disabled");

            if (!htmlAttributes.Any(a => a.Key == "id"))
            {
                id = ((MemberExpression)express.Body).Member.Name + value.ToString();
                htmlAttributes.Add("id", id);
            }
            else { id = htmlAttributes["id"].ToString(); }

            string radio = InputExtensions.RadioButtonFor<TModel, TProperty>(helper, express, value, htmlAttributes).ToString();
            string label = helper.Label(id, text).ToString();

            return MvcHtmlString.Create(radio + label);
        }

        public static MvcHtmlString RadioButtonLabelFor<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            string text,
            Expression<Func<TModel, TProperty>> express,
            object value)
        {
            return RadioButtonLabelFor(helper, text, express, value, null, false, false);
        }

        public static MvcHtmlString RadioButtonLabelFor<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            string text,
            Expression<Func<TModel, TProperty>> express,
            object value,
            bool @checked,
            bool disabled)
        {
            return RadioButtonLabelFor(helper, text, express, value, null, @checked, disabled);
        } 
        #endregion

        #region RadioButtonLabel
        public static MvcHtmlString RadioButtonLabel(
            this HtmlHelper helper,
            string name,
            string text,
            object value,
            object attr,
            bool @checked,
            bool disabled)
        {
            IDictionary<string,object> htmlAttributes = attr == null ? new RouteValueDictionary() : new RouteValueDictionary(attr);
            if (@checked) htmlAttributes.Add("checked", "checked");
            if (disabled) htmlAttributes.Add("disabled", "disabled");

            ModelMetadata meta = ModelMetadata.FromStringExpression(name, helper.ViewData);
            htmlAttributes = helper.GetUnobtrusiveValidationAttributes(name, meta);
            htmlAttributes.Add("id", name + value.ToString());

            string radio = InputExtensions.RadioButton(helper, name, value, htmlAttributes).ToString();
            string label = helper.Label(name + value.ToString(), text).ToString();

            return MvcHtmlString.Create(radio + label);
        }

        public static MvcHtmlString RadioButtonLabel(
            this HtmlHelper helper,
            string name,
            string text,
            object value)
        {
            return RadioButtonLabel(helper, name, text, value, null, false, false);
        }
        /*
        public static MvcHtmlString RadioButtonLabelFor<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> express,
            string text,
            object value,
            object attr,
            bool @checked,
            bool disabled)
        {
            IDictionary<string, object> htmlAttributes = attr == null ? new RouteValueDictionary() : new RouteValueDictionary(attr);
            if (@checked) htmlAttributes.Add("checked", "checked");
            if (disabled) htmlAttributes.Add("disabled", "disabled");

            string radio = InputExtensions.RadioButtonFor(helper, express, value, htmlAttributes).ToString();
            string label = helper.LabelFor(express, text).ToString();
            
            string label = (htmlAttributes["id"] == null ?
                helper.Label(name, text) : helper.Label(htmlAttributes["id"].ToString(), text)).ToString();
            
            return MvcHtmlString.Create(radio + label);
        }*/
        
        #endregion
    }

    #region Generate </DIV>
    public class Div : IDisposable
    {
        private readonly TextWriter _writer;
        
        public Div(TextWriter writer)
        {
            _writer = writer;
        }

        public void Dispose()
        {
            _writer.WriteLine("</div>");
        }
    }
    #endregion

    #region Generate </a>
    public class A : IDisposable
    {
        private readonly TextWriter _writer;

        public A(TextWriter writer)
        {
            _writer = writer;
        }

        public void Dispose()
        {
            _writer.WriteLine("</a>");
        }
    }
    #endregion
}