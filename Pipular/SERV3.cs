﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.IO;
using NLog;
using System.Configuration;

namespace Pipular
{
    public class SERV3
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();

        public Socket SendCMD(string cmd, string table, string nmbr, string user)
        {
            string result = "no result";
            string crypt_message = "";
            string message = "<EXTERNAL><CMD>" + cmd + "</CMD><TABLE>" + table + "</TABLE><NMBR>" + nmbr + "</NMBR><USER>" + user + "</USER>";
            int attempts = int.Parse(ConfigurationManager.AppSettings["CMD_attemps"]);
            int timeout = int.Parse(ConfigurationManager.AppSettings["CMD_timeout"]);
            string server = ConfigurationManager.AppSettings["CMD_server"];
            int port = int.Parse(ConfigurationManager.AppSettings["CMD_port"]);
            int a = 1;

            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(server, port);
            socket.SendTimeout = timeout;
            socket.ReceiveTimeout = timeout;

            crypt_message = XorCrypt(message, 0x55) + "\r\n";

            byte[] msg = Encoding.UTF8.GetBytes(crypt_message);
            byte[] bytes = new byte[256];

            for (int i = 0; i < bytes.Length; i++) { bytes[i] = 0; }
            
            lock (this) { logger.Info(message); }
            try
            {
                while (result != "1" & a <= attempts)
                {
                    // Blocks until send returns.
                    int byteCount = socket.Send(msg, 0, msg.Length, SocketFlags.None);

                    // Get reply from the server.
                    byteCount = socket.Receive(bytes, 0, 255, SocketFlags.None);

                    string reply = ASCIIEncoding.ASCII.GetString(bytes);
                    reply = reply.Substring(0, reply.IndexOf("\r\n"));

                    result = XorCrypt(reply, 0x55);

                    lock (this)
                    {
                        logger.Info("result: " + result + ";  attempt " + a);
                    }

                    a++;
                }
            }
            catch (SocketException error)
            {
                result = (String.Format("{0} Error code: {1}.", error.Message, error.ErrorCode));
                socket.Close(timeout);
            }

            return socket;
        }

        #region Send command
        public void SendCMD(object sender, DoWorkEventArgs e)
        {
            string[] param = (string[])e.Argument;
            SendCMD(param[0], param[1], param[2], param[3]).Close();
        }
        #endregion

        #region Send copy command
        public void SendCopyCMD(dynamic Caller, string cmd, string table, string nmbr, int user, out int RESULT, out int STATUS, out int ERR, out double price)
        {
            RESULT = -1000;
            STATUS = -1000;
            ERR = -1000;
            price = 0;
            int stage = 0;
            string error = "", result = "";
  
            // Send command
            Socket socket = SendCMD(cmd, table, nmbr, user.ToString());

            try
            {
                // Get reply from the server.
                Hashtable hash_result = new Hashtable();
                byte[] bytes = new byte[256];
                for (int i = 0; i < bytes.Length; i++) { bytes[i] = 0; }

                stage = 1;
                Caller.Response("Connecting to server...");
                hash_result = GetSocketResponse(socket, bytes, 5000);

                if (!hash_result.Contains("RESULT")) Caller.Response("No response from server or parse error.");
                else
                {
                    RESULT = int.Parse((string)hash_result["RESULT"]);
                    if (RESULT == 1)  // accept by server
                    {
                        stage = 2;
                        Caller.Response("Execution of request on server...");
                        hash_result = GetSocketResponse(socket, bytes, 5000);

                        if (!hash_result.Contains("RESULT") || !hash_result.Contains("STATUS")) Caller.Response("No response from server or parse error.");
                        else
                        {
                            RESULT = int.Parse((string)hash_result["RESULT"]);
                            STATUS = int.Parse((string)hash_result["STATUS"]);
                            if (RESULT == 1 && STATUS == 0) // perform by server
                            {
                                stage = 3;
                                Caller.Response("Sending to client...");
                                hash_result = GetSocketResponse(socket, bytes, 5000);

                                if (!hash_result.Contains("RESULT") || !hash_result.Contains("STATUS")) Caller.Response("No response from server or parse error.");
                                else
                                {
                                    RESULT = int.Parse((string)hash_result["RESULT"]);
                                    STATUS = int.Parse((string)hash_result["STATUS"]);
                                    if (RESULT == 1 && STATUS == 1) // sent to client
                                    {
                                        stage = 4;
                                        Caller.Response("Client accept request...");
                                        hash_result = GetSocketResponse(socket, bytes, 15000);

                                        if (!hash_result.Contains("RESULT") || !hash_result.Contains("STATUS")) Caller.Response("No response from server or parse error.");
                                        else
                                        {
                                            RESULT = int.Parse((string)hash_result["RESULT"]);
                                            STATUS = int.Parse((string)hash_result["STATUS"]);
                                            if (RESULT == 1 && STATUS == 2) // accept by client
                                            {
                                                stage = 5;
                                                Caller.Response("Execution on client...");
                                                hash_result = GetSocketResponse(socket, bytes, 130000);

                                                if (!hash_result.Contains("RESULT") || !hash_result.Contains("STATUS") || !hash_result.Contains("ERR")) Caller.Response("No response from server or parse error.");
                                                else
                                                {
                                                    RESULT = int.Parse((string)hash_result["RESULT"]);
                                                    STATUS = int.Parse((string)hash_result["STATUS"]);
                                                    ERR = int.Parse((string)hash_result["ERR"]);
                                                    if (RESULT == 1 && STATUS == 3 && (ERR == 1 || ERR == 0)) // perform by client
                                                    {
                                                        string success = "Execution success at";

                                                        if (hash_result.Contains("PRICE"))
                                                        {
                                                            if (double.TryParse(hash_result["PRICE"].ToString(), out price))
                                                            {
                                                                if (price > 0) success += " " + hash_result["PRICE"].ToString() + ".";
                                                                else success += " [Price error: " + price + "]";
                                                            }
                                                            else success += " [wrong price]";
                                                        }
                                                        else success += " unknown price.";

                                                        Caller.Response(success);
                                                    }
                                                }
                                            }
                                            else Caller.Response("Error: not accepted by client.");
                                        }
                                    }
                                    else Caller.Response("Error: client sanding failed.");
                                }
                            }
                            else Caller.Response("Error: command execution failed on server.");
                        }
                    }
                    else Caller.Response("Error: not accepted by server.");
                }

                #region RESULT errors
                if (hash_result.Contains("RESULT"))
                {
                    RESULT = int.Parse((string)hash_result["RESULT"]);

                    if (RESULT != 1)
                    {
                        error += " SERVER RESPONSE:";
                        switch (RESULT)
                        {
                            case 0: error += " Unknown error." + " (" + RESULT + ")"; break;
                            case -1: error += " External commands from the same address are not allowed." + " (" + RESULT + ")"; break;
                            case -2: error += " Unknown command." + " (" + RESULT + ")"; break;
                            case -3: error += " There are no required parameters." + " (" + RESULT + ")"; break;
                            case -4: error += " Description of the copy command not found." + " (" + RESULT + ")"; break;
                            case -5: error += " Description copied signal not found." + " (" + RESULT + ")"; break;
                            case -6: error += " Client account for copy signal is not found." + " (" + RESULT + ")"; break;
                            case -7: error += " Description of the client not found." + " (" + RESULT + ")"; break;
                            case -8: error += " Description of the network connection not found." + " (" + RESULT + ")"; break;
                            case -9: error += " External commands disabled by administrator." + " (" + RESULT + ")"; break;
                            case -10: error += " Timeout." + " (" + RESULT + ")"; break;
                            case -11: error += " Unacceptable broker to receive signals" + " (" + RESULT + ")"; break;
                            default: error += " Result error: " + RESULT; break;
                        }
                    }
                }
                #endregion

                #region STATUS errors
                if (hash_result.Contains("STATUS"))
                {
                    STATUS = int.Parse((string)hash_result["STATUS"]);
                    if (STATUS < 0)
                        switch (STATUS)
                        {
                            default: error += " Status error: " + STATUS; break;
                        }
                }
                #endregion

                #region Terminal errors
                if (hash_result.Contains("ERR"))
                {
                    ERR = int.Parse((string)hash_result["ERR"]);
                    if (ERR != 1 && ERR != 0)
                    {
                        error += " TERMINAL RESPONSE:";
                        switch (ERR)
                        {
                            //case 0: error += " No error returned."; break;
                            case 2: error += " Common error." + " (" + ERR + ")"; break;
                            case 3: error += " Invalid trade parameters." + " (" + ERR + ")"; break;
                            case 4: error += " Trade server is busy." + " (" + ERR + ")"; break;
                            case 5: error += " Old version of the client terminal." + " (" + ERR + ")"; break;
                            case 6: error += " No connection with trade server." + " (" + ERR + ")"; break;
                            case 7: error += " Not enough rights." + " (" + ERR + ")"; break;
                            case 8: error += " Too frequent requests." + " (" + ERR + ")"; break;
                            case 9: error += " Malfunctional trade operation." + " (" + ERR + ")"; break;
                            case 64: error += " Account disabled." + " (" + ERR + ")"; break;
                            case 65: error += " Invalid account." + " (" + ERR + ")"; break;
                            case 128: error += " Trade timeout." + " (" + ERR + ")"; break;
                            case 129: error += " Invalid price." + " (" + ERR + ")"; break;
                            case 130: error += " Invalid stops." + " (" + ERR + ")"; break;
                            case 131: error += " Invalid trade volume." + " (" + ERR + ")"; break;
                            case 132: error += " Market is closed." + " (" + ERR + ")"; break;
                            case 133: error += " Trade is disabled." + " (" + ERR + ")"; break;
                            case 134: error += " Not enough money." + " (" + ERR + ")"; break;
                            case 135: error += " Price changed." + " (" + ERR + ")"; break;
                            case 136: error += " Off quotes." + " (" + ERR + ")"; break;
                            case 137: error += " Broker is busy." + " (" + ERR + ")"; break;
                            case 138: error += " Requote." + " (" + ERR + ")"; break;
                            case 139: error += " Order is locked." + " (" + ERR + ")"; break;
                            case 140: error += " Long positions only allowed." + " (" + ERR + ")"; break;
                            case 141: error += " Too many requests." + " (" + ERR + ")"; break;
                            case 145: error += " Modification denied because an order is too close to market." + " (" + ERR + ")"; break;
                            case 146: error += " Trade context is busy." + " (" + ERR + ")"; break;
                            case 147: error += " Expirations are denied by broker." + " (" + ERR + ")"; break;
                            case 148: error += " The amount of opened and pending orders has reached the limit set by a broker." + " (" + ERR + ")"; break;
                            default: error += " Terminal error: " + ERR; break;
                        }
                    }
                }
                #endregion

                if (error != "") Caller.Response(error);

                result = "Result: " + RESULT + "  Status: " + STATUS + "  Error: " + ERR;
            }
            #region Catch
            catch (SocketException e)
            {
                result = (String.Format("{0} Error code: {1}.", e.Message, e.ErrorCode));
                if (e.ErrorCode == 10060)
                {
                    switch (stage)
                    {
                        case 0: error = "Server response timeout."; break;
                        case 1: error = "Timeout. Server didn't accept command"; break;
                        case 2: error = "Timeout. Server doesn't execute command"; break;
                        case 3: error = "Timeout. Server doesn't send a command to the client"; break;
                        case 4: error = "Timeout. Command wasn't accepted by client"; break;
                        case 5: error = "Timeout. The command is not executed by client"; break;
                    }
                }
                else error = "Fatal error: " + e.ErrorCode;

                Caller.Response(error);

                socket.Close();
            }
            #endregion

            lock (this)
            {
                logger.Info("result: " + result + ";  stage " + stage);
            }

            socket.Close();
        }
        #endregion

        #region Get socket response
        public static Hashtable GetSocketResponse(Socket socket, byte[] bytes, int timeout)
        {
            return Extension.XMLParse(GetSocketResponseString(socket, bytes, timeout));
        }
        #endregion

        #region Get Socket Response String
        public static string GetSocketResponseString(Socket socket, byte[] bytes, int timeout)
        {
            int byteCount;
            string reply = "";

            socket.ReceiveTimeout = timeout;
            byteCount = socket.Receive(bytes, 0, 255, SocketFlags.None);
            reply = UTF8Encoding.UTF8.GetString(bytes);
            reply = reply.Substring(0, reply.IndexOf("\r\n"));

            return XorCrypt(reply, 0x55);
        }
        #endregion

        #region XOR crypt
        public static string XorCrypt(string text, int key)
        {
            string newText = "";

            for (int i = 0; i < text.Length; i++)
            {
                // Получаем ASCII-код символа
                int charValue = Convert.ToInt32(text[i]);
                // XOR-им символ
                if (charValue != key) charValue ^= key;

                // Преобразуем результат обратно в строку
                newText += char.ConvertFromUtf32(charValue);
            }
            return newText;
        }
        #endregion

        #region Reply result
        static List<string> ReplyResult(string str)
        {
            str = str.TrimEnd(new Char[] { (char)0x00 });
            
            List<string> result = new List<string>();

            Regex regex = new Regex(@"(\w+)(?=\\r\\n)");
            MatchCollection match = regex.Matches(str);
            result.Add(match[0].ToString());

            return result;
        }
        #endregion
    }
}