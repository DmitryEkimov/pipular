﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Pipular.Accounting;

namespace Pipular
{
    // Примечание: Инструкции по включению классического режима IIS6 или IIS7 
    // см. по ссылке http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "TradeStream", // Имя маршрута
                "TradeStream", // URL-адрес с параметрами
                new { controller = "Home", action = "TradeStream", id = UrlParameter.Optional } // Параметры по умолчанию
            );

            routes.MapRoute(
                "Dashboard", // Имя маршрута
                "Dashboard", // URL-адрес с параметрами
                new { controller = "Private", action = "Dashboard", id = UrlParameter.Optional } // Параметры по умолчанию
            );

            routes.MapRoute(
                "Traders", // Имя маршрута
                "Traders", // URL-адрес с параметрами
                new { controller = "Home", action = "Traders", id = UrlParameter.Optional } // Параметры по умолчанию
            );

            routes.MapRoute(
                "Markets", // Имя маршрута
                "Markets", // URL-адрес с параметрами
                new { controller = "Home", action = "Markets", id = UrlParameter.Optional } // Параметры по умолчанию
            );

            routes.MapRoute(
                "Default", // Имя маршрута
                "{controller}/{action}/{id}", // URL-адрес с параметрами
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Параметры по умолчанию
            );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RouteTable.Routes.MapHubs();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            RouteHandler<RedirectHandler>.Assign(RouteTable.Routes);

            Accounting.Accounting accounting = new Accounting.Accounting(AccountingManager.Instance);
        }
    }
}