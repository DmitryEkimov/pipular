﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pipular.Models;
using Pipular.Models.Charts;
using System.Configuration;
using System.Data.Objects.DataClasses;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Threading;
using System.Globalization;
using NLog;
using MathNet.Numerics;
using MathNet.Numerics.Distributions;

namespace Pipular.Controllers
{
    public class PrivateController : BaseController
    {
        BackgroundWorker bw = new BackgroundWorker();
        SERV3 serv3 = new SERV3();

        #region Download PipRunner
        [Authorize]
        public FilePathResult GetPipRunner()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "soft/";

            List<FileNames> file_list = GetFiles(path);

            if (!file_list.Any()) return null;

            FileNames file = file_list.OrderByDescending(f => f.version).First();

            int user_nmbr = int.Parse(User.Identity.Name);

            DB.users.First(u => u.nmbr == user_nmbr).soft = file.version;
            DB.SaveChanges();

            Response.Cookies["new_version"].Expires = DateTime.Now.AddDays(-1);

            string fileName = file.name;
            return File(path + fileName, "text/plain", fileName);
        }
        
        public class FileNames
        {
            public int version { get; set; }
            public string name { get; set; }
        }

        public static List<FileNames> GetFiles(string path)
        {
            List<FileNames> file_list = new List<FileNames>();
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            Regex regex = new Regex(@"(?<=\(version )(\d+)(?=\))");

            foreach (FileInfo item in dirInfo.GetFiles())
            {
                int ver;

                if(!int.TryParse(regex.Match(item.Name).Value, out ver)) ver = 0;

                file_list.Add(new FileNames()
                {
                    name = item.Name,
                    version = ver
                });
            }

            return file_list;
        }
        #endregion

        // DASHBOARD -----------------------------------------------------
        
        #region Dashboard
        [Authorize]
        public ActionResult Dashboard()
        {
            int user_nmbr = int.Parse(User.Identity.Name);

            ViewBag.Portfolios = GetUserPortfolios(user_nmbr);

            ViewBag.Copies = GetUserCopies(user_nmbr);

            ViewBag.Systems = GetUserSystems(user_nmbr);

            return View();
        } 
        #endregion

        // follow panel

        #region Portfolio switch
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public EmptyResult PortfolioSwitch(int id)
        {
            try
            {
                account acc = DB.accounts.Single(a => a.nmbr == id);
                acc.@public = acc.@public == 0 ? 1 : 0;

                DB.SaveChanges();
            }
            catch { }

            return null;
        }
        #endregion

        #region Portfolio delete (delete account)
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PortfolioDelete(int id)
        {
            int user_nmbr = int.Parse(User.Identity.Name);

            Message mess = new Message();

            try
            {
                account acc = DB.accounts.Single(a => a.nmbr == id && a.user == user_nmbr);
                if (acc != null)
                {
                    DB.accounts.DeleteObject(acc);

                    IEnumerable<connect> connects = from c in DB.connects
                                                    where c.account2 == acc.nmbr
                                                    select c;
                    foreach (connect c in connects)
                    {
                        c.account2 = 0;
                    }

                    IEnumerable<trade> delete = from t in DB.trades
                                                join c in DB.connects on t.connect equals c.nmbr
                                                select t;
                    foreach (trade t in delete)
                        DB.trades.DeleteObject(t);

                    DB.SaveChanges(); 
                }
            }
            catch (Exception error)
            {
                mess.text = error.Message + error.InnerException;
                mess.type = MessageType.Bad;

                return PartialView("Message", mess);
            }

            mess.text = "Portfolio successfully removed.";
            mess.type = MessageType.Good;

            return PartialView("Message", mess);
        }
        #endregion

        #region Connect block
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult ConnectBlock(int id)
        {
            ConnectModel model = GetConnect(id);

            return PartialView(model);
        } 
        #endregion

        #region Change portfolio
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult ChangePortfolio(int id, int account)
        {
            ConnectModel model = new ConnectModel();

            try
            {
                connect con = DB.connects.Single(c => c.nmbr == id);
                con.account2 = account;
                con.dt_last = DateTime.UtcNow;
                DB.SaveChanges();

                bw.DoWork += serv3.SendCMD;
                bw.RunWorkerAsync(new string[] { "EDIT_CONNECT", "connects", id.ToString(), User.Identity.Name });
            }
            catch { }

            return RedirectToAction("ConnectBlock", new { id = id });
        } 
        #endregion

        #region ConnectSwitch
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult ConnectSwitch(int id)
        {
            connect con = DB.connects.First(c => c.nmbr == id);

            con.enabled = (con.enabled == 1) ? 0 : 1;
            con.dt_last = DateTime.UtcNow;

            DB.SaveChanges();

            bw.DoWork += serv3.SendCMD;
            bw.RunWorkerAsync(new string[] { "EDIT_CONNECT", "connects", con.nmbr.ToString(), User.Identity.Name });

            return RedirectToAction("ConnectBlock", new { id = id });
        } 
        #endregion

        #region Connect delete
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult ConnectDelete(int id)
        {
            Message mess = new Message();
            
            try
            {
                connect con = DB.connects.First(c => c.nmbr == id);

                con.enabled = 2;
                con.dt_last = DateTime.UtcNow;

                DB.SaveChanges();

                bw.DoWork += serv3.SendCMD;
                bw.RunWorkerAsync(new string[] { "DEL_CONNECT", "connects", con.nmbr.ToString(), User.Identity.Name });

                mess.text = "Connect deleted successfully.";
                mess.type = MessageType.Good;
            }
            catch (Exception error) 
            {
                mess.type = MessageType.Bad;
                mess.text = error.Message;
            }

            return PartialView("Message", mess);
        }
        #endregion

        #region Sending type switch
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult SendingTypeSwitch(int id, int type)
        {
            try
            {
                connect con = DB.connects.First(c => c.nmbr == id);

                switch (type)
                {
                    case 1: // MT4
                        if (con.mt4 == 1) con.mt4 = 0;
                        else con.mt4 = 1;
                        break;
                    case 2: // EMAIL
                        if (con.email == 1) con.email = 0;
                        else con.email = 1;
                        break;
                    case 3: // ICQ
                        if (con.icq == 1) con.icq = 0;
                        else con.icq = 1;
                        break;
                    case 4: // PHONE
                        if (con.phone == 1) con.phone = 0;
                        else con.phone = 1;
                        break;
                    case 5: // SKYPE
                        if (con.skype == 1) con.skype = 0;
                        else con.skype = 1;
                        break;
                }

                con.dt_last = DateTime.UtcNow;

                DB.SaveChanges();

                bw.DoWork += serv3.SendCMD;
                bw.RunWorkerAsync(new string[] { "EDIT_CONNECT", "connects", con.nmbr.ToString(), User.Identity.Name });
            }
            catch { }

            return RedirectToAction("ConnectBlock", new { id = id });
        }
        #endregion

        #region Connect copy
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult ConnectCopy(int id, int account)
        {
            try
            {
                connect con = DB.connects.First(c => c.nmbr == id);
                
                connect new_con = new connect()
                    {
                        account = con.account,
                        account2 = account,
                        dt_last = DateTime.UtcNow,
                        dt_create = DateTime.UtcNow,
                        email = con.email,
                        enabled = con.enabled,
                        icq = con.icq,
                        mt4 = con.mt4,
                        phone = con.phone,
                        portfolio = con.portfolio,
                        skype = con.skype,
                        system = con.system,
                        tarif = con.tarif,
                        ttype = con.ttype,
                        user = con.user
                    };

                DB.connects.AddObject(new_con);
                DB.SaveChanges();

                id = new_con.nmbr;

                bw.DoWork += serv3.SendCMD;
                bw.RunWorkerAsync(new string[] { "ADD_CONNECT", "connects", new_con.nmbr.ToString(), User.Identity.Name });
            }
            catch { }

            return RedirectToAction("ConnectBlock", new { id = id });
        }
        #endregion

        #region Connect details
        public ActionResult ConnectDetails(int system, int connect, DateTime? start, DateTime? stop)
        {
            IQueryable<history> history_connect = Query.History(system, connect, start, stop);
            IQueryable<trade> trade_connect = Query.Trades(system, connect, start, stop);

            Statistic stat = new Statistic();
            Normal n = new Normal(0, 1);

            rsystem system_stat = (from rs in DB.rsystems
                                   where rs.system == system && rs.connect == connect
                                   select rs).FirstOrDefault();

            if (system_stat == null) system_stat = new rsystem()
            {
                system = system,
                signals = 0,
                trades = 0,
                longs = 0,
                profitable = 0,
                longs_prof = 0,
                lots = 0,
                all_time_trade = 0,
                variance = 0
            };

            int totalTrades = system_stat.trades;

            int profitTrades = system_stat.profitable;

            int lossTrades = system_stat.trades - system_stat.profitable;

            int P = 2 * profitTrades * lossTrades;

            stat.trades = totalTrades;
            stat.profit_trades = profitTrades;
            stat.loss_trades = lossTrades;
            stat.longs = system_stat.longs;
            stat.shorts = system_stat.trades - system_stat.longs;
            stat.pips = Math.Round(system_stat.profit_pip + system_stat.loss_pip, 1);
            stat.avg_profit = system_stat.trades > 0 ? Math.Round(system_stat.profit / system_stat.profitable, 2) : 0;
            stat.avg_profit_pips = system_stat.trades > 0 ? Math.Round(system_stat.profit_pip / system_stat.profitable, 2) : 0;
            stat.avg_loss = system_stat.trades > 0 ? Math.Round(system_stat.loss / lossTrades, 2) : 0;
            stat.avg_loss_pips = system_stat.trades > 0 ? Math.Round(system_stat.loss_pip / lossTrades, 2) : 0;
            stat.lots = system_stat.lots / 100;
            stat.profit_long = system_stat.longs_prof;
            stat.profit_short = system_stat.profitable - system_stat.longs_prof;
            stat.best = system_stat.best;
            stat.best_pips = system_stat.best_pip;
            stat.worst = system_stat.worst;
            stat.worst_pips = system_stat.worst_pip;
            stat.avg_time_trade = system_stat.trades > 0 ? (system_stat.all_time_trade / system_stat.trades).UnixTimeSpan().TimeSpanToString() : "";
            stat.profit_factor = lossTrades > 0 ? Math.Round((double)profitTrades / (double)lossTrades, 2) : 0;
            stat.expected_payoff = system_stat.trades > 0 ? Math.Round((system_stat.profit + system_stat.loss) / system_stat.trades, 2) : 0;
            stat.expected_payoff_pips = system_stat.trades > 0 ? Math.Round((system_stat.profit_pip + system_stat.loss_pip) / system_stat.trades, 2) : 0;
            stat.standart_deviation = system_stat.trades > 1 ? Math.Round(Math.Sqrt(stat.trades / (stat.trades - 1) * system_stat.variance), 2) : 0;
            // Z=(N*(R-0.5)-P)/((P*(P-N))/(N-1))^(1/2)
            stat.zscore = system_stat.trades > 0 ? Math.Round((totalTrades * ((double)system_stat.series - 0.5) - P) / Math.Sqrt((double)(P * (P - totalTrades)) / (double)(totalTrades - 1)), 2) : 0;
            stat.prob = system_stat.trades > 0 ? Math.Round(n.CumulativeDistribution(Math.Abs(stat.zscore)) * 100, 2) : 0;
            stat.sharpe = system_stat.trades > 0 ? Math.Round(stat.expected_payoff / stat.standart_deviation, 3) : 0;

            ViewBag.System = system;
            ViewBag.Connect = connect;
            ViewData["history_connect"] = history_connect;
            ViewData["trade_connect"] = trade_connect;
            ViewData["statistic"] = stat;
            ViewData["start"] = start;
            ViewData["stop"] = stop;

            var model = new Dictionary<string, string>();

            model.Add("system_name", DB.systems.FirstOrDefault(s => s.nmbr == system).name);

            if (Request.IsAjaxRequest())
                return PartialView(model);
            return View(model);
        }
        #endregion

        #region Connect configuration
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult ConnectConfiguration(int id)
        {
            return PartialView(DB.connects.FirstOrDefault(c => c.nmbr == id));
        }
        #endregion

        #region Save configuration
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult SaveConfiguration(int id, FormCollection collection)
        {
            try
            {
                connect con = DB.connects.First(c => c.nmbr == id);

                if (collection["cfg_mvolume"] != null)
                    collection["cfg_mvolume"] = collection["cfg_mvolume"].ReplaceDecimalSeparator();
                if (collection["cfg_fixed"] != null)
                    collection["cfg_fixed"] = collection["cfg_fixed"].ReplaceDecimalSeparator();
                if (collection["cfg_koeff"] != null)
                    collection["cfg_koeff"] = collection["cfg_koeff"].ReplaceDecimalSeparator();

                decimal _mvolume = 0;
                if (decimal.TryParse(collection["cfg_mvolume"], out _mvolume))
                {
                    collection["cfg_mvolume"] = ((int)(_mvolume * 100)).ToString();
                }

                decimal _fixed = 0;
                if(decimal.TryParse(collection["cfg_fixed"], out _fixed))
                {
                    collection["cfg_fixed"] = ((int)(_fixed * 100)).ToString();
                }

                UpdateModel<connect>(con, collection);

                DB.SaveChanges();

                bw.DoWork += serv3.SendCMD;
                bw.RunWorkerAsync(new string[] { "EDIT_CONNECT", "connects", con.nmbr.ToString(), User.Identity.Name });
            }
            catch { return PartialView("Error"); }

            return RedirectToAction("ConnectConfiguration", new { id = id });
        }
        #endregion

        #region Select portfolio (after follow)
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult SelectPortfolio(int id) // id = new connect
        {
            int user_nmbr = int.Parse(User.Identity.Name);

            connect con = DB.connects.Single(c => c.nmbr == id);

            List<Portfolio> portfolio_list = GetUserPortfolios(user_nmbr).Where(p => p.number > 0).ToList();

            ViewBag.ConnectId = id;
            ViewBag.SystemId = con.system;

            return PartialView(portfolio_list);
        }
        #endregion

        #region Add connect to portfolio
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public EmptyResult AddConnectToPortfolio(int connect_id, int portfolio_id)
        {
            connect con = DB.connects.First(c => c.nmbr == connect_id);
            
            if (!DB.connects.Any(c => c.account2 == portfolio_id && c.system == con.system && c.enabled != 2))
            {
                con.account2 = portfolio_id;
                DB.SaveChanges();

                bw.DoWork += serv3.SendCMD;
                bw.RunWorkerAsync(new string[] { "EDIT_CONNECT", "connects", connect_id.ToString(), User.Identity.Name });
            }

            return null;
        }
        #endregion

        // copy panel


        // lead panel

        #region System block
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult SystemBlock(int id)
        {
            SystemModel model = GetSystem(id);

            return PartialView(model);
        }
        #endregion

        #region New system
        [Authorize]
        public ActionResult NewSystem()
        {
            return PartialView();
        } 
        #endregion

        #region New system POST
        [HttpPost]
        [Authorize]
        public ActionResult NewSystem(AddSystemModel model)
        {
            int uid = int.Parse(User.Identity.Name);

            try
            {
                system new_system = new system()
                {
                    user = uid,
                    name = model.Name,
                    desc = model.Desc,
                    desc_html = model.Desc_html,
                    trade_type = AccountController.GetCheckBoxCode(model.Trade_type),
                    trade_style = AccountController.GetCheckBoxCode(model.Trade_style),
                    trade_markets = AccountController.GetCheckBoxCode(model.Trade_markets),
                    trade_sessions = AccountController.GetCheckBoxCode(model.Trade_sessions),
                    dt_create = DateTime.UtcNow,
                    enabled = 1
                };

                DB.systems.AddObject(new_system);
                DB.SaveChanges();

                bw.DoWork += serv3.SendCMD;
                bw.RunWorkerAsync(new string[] { "ADD_SYSTEM", "systems", new_system.nmbr.ToString(), User.Identity.Name });

                return RedirectToAction("SystemBlock", new { id = new_system.nmbr });
            }
            catch (Exception error)
            {
                //HttpContext.Response.StatusCode = 500;
                ModelState.AddModelError("", error.Message);
                return PartialView(model);
            }
        }
        #endregion

        #region System Switch
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult SystemSwitch(int id)
        {
            system sys = DB.systems.First(c => c.nmbr == id);

            sys.enabled = (sys.enabled == 1) ? 0 : 1;
            sys.dt_last = DateTime.UtcNow;

            DB.SaveChanges();

            bw.DoWork += serv3.SendCMD;
            bw.RunWorkerAsync(new string[] { "EDIT_SYSTEM", "systems", sys.nmbr.ToString(), User.Identity.Name });

            return RedirectToAction("SystemBlock", new { id = id });
        }
        #endregion

        #region System delete
        [Authorize]
        public ActionResult SystemDelete(int id)
        {
            Message mess = new Message();
            
            system sys = (from system in DB.systems
                          where system.nmbr == id
                          select system).First();

            if (sys.user != int.Parse(User.Identity.Name))
            {
                mess.type = MessageType.Bad;
                mess.text = "Access denied. You are trying to delete foreign system.";
                //HttpContext.Response.StatusCode = 500;
                return PartialView("Message", mess);
            }
            if (DB.connects.Any(t => t.system == id & t.enabled == 1))
            {
                mess.type = MessageType.Bad;
                mess.text = "This system is used by clients. Removing is only possible without connections to it.";
                //HttpContext.Response.StatusCode = 500;
                return PartialView("Message", mess);
            }

            try
            {
                sys.enabled = 2;
                sys.dt_last = DateTime.Now;

                IEnumerable<tarif> tariffs = from t in DB.tarifs
                                             where t.system == sys.nmbr
                                             select t;
                foreach (tarif t in tariffs)
                    t.enabled = 2;

                DB.SaveChanges();
            }
            catch (Exception error)
            {
                ViewBag.Message = error.Message;
                View(id);
            }

            bw.DoWork += serv3.SendCMD;
            bw.RunWorkerAsync(new string[] { "DEL_SYSTEM", "systems", sys.nmbr.ToString(), User.Identity.Name });

            mess.type = MessageType.Good;
            mess.text = "System was successfully removed.";
            //HttpContext.Response.StatusCode = 500;
            return PartialView("Message", mess);
        }
        #endregion

        #region New tariff
        [Authorize]
        public ActionResult NewTariff(int id)
        {
            tarif model = new tarif() { system = id };

            return PartialView(model);
        } 
        #endregion

        #region New tariff [POST]
        [Authorize]
        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult NewTariff(tarif tariff)
        {
            if (DB.systems.First(s => s.nmbr == tariff.system).user != int.Parse(User.Identity.Name))
                return RedirectToAction("AccessDenied", "Private", new { message = "Access denied. You are trying to add tariff on foreign system" });

            tarif new_tariff = new tarif()
            {
                system = tariff.system,
                enabled = 1,
                @for = tariff.@for,
                icq = 1,
                mt4 = 1,
                phone = 1,
                skype = 1,
                type = tariff.type,
                summa = tariff.summa,
                dt_last = DateTime.UtcNow,
                dt_create = DateTime.UtcNow
            };

            try
            {
                DB.tarifs.AddObject(new_tariff);
                DB.SaveChanges();

                return RedirectToAction("TariffBlock", new { id = new_tariff.nmbr });
            }
            catch { return PartialView("Error"); }
/*
            {
                Message mess = new Message();

                if (DB.systems.First(s => s.nmbr == tariff.system).user != int.Parse(User.Identity.Name))
                {
                    mess.type = MessageType.Bad;
                    mess.text = "Access denied. You are trying to edit foreign tariff.";
                    //HttpContext.Response.StatusCode = 500;
                    return PartialView("Message", mess);
                }

                if (DB.connects.Any(c => c.tarif == tariff.nmbr && c.enabled != 2))
                {
                    mess.type = MessageType.Bad;
                    mess.text = "This tariff is used. Editing is only possible without connections to it.";
                    //HttpContext.Response.StatusCode = 500;
                    return PartialView("Message", mess);
                }

                tarif Tariff = (from tarif in DB.tarifs
                                where tarif.nmbr == tariff.nmbr
                                select tarif).Single();
                try
                {
                    Tariff.type = tariff.type;
                    Tariff.tsize = tariff.tsize;
                    Tariff.summa = tariff.summa;
                    Tariff.dt_last = DateTime.UtcNow;

                    DB.SaveChanges();
                }
                catch { }

                return RedirectToAction("TariffBlock", new { id = Tariff.nmbr });
            }
*/
        }
        #endregion

        #region Edit tariff
        [Authorize]
        public ActionResult EditTariff(int id)
        {
            tarif model = Tariff(id);

            if (DB.systems.First(s => s.nmbr == model.system).user != int.Parse(User.Identity.Name))
                return RedirectToAction("AccessDenied", "Private", new { message = "Access denied. You are trying to edit foreign tariff" });

            return PartialView(model);
        }
        #endregion

        #region Edit tariff POST
        [Authorize]
        [HttpPost]
        public ActionResult EditTariff(int id, FormCollection collection)
        {
            tarif tariff = Tariff(id);

            if (DB.systems.First(s => s.nmbr == tariff.system).user != int.Parse(User.Identity.Name))
                return RedirectToAction("AccessDenied", "Private", new { message = "Access denied. You are trying to edit foreign tariff" });

            try
            {
                tariff.dt_last = DateTime.UtcNow;
                UpdateModel<tarif>(tariff, collection);
                DB.SaveChanges();
            }
            catch { }

            return RedirectToAction("TariffBlock", new { id = tariff.nmbr });
        }
        #endregion

        #region Delete tariff
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult DeleteTariff(int id)
        {
            Message mess = new Message();

            tarif tariff = Tariff(id);

            if (DB.systems.First(s => s.nmbr == tariff.system).user != int.Parse(User.Identity.Name))
                return RedirectToAction("AccessDenied", "Private", new { message = "Access denied. You are trying to delete foreign tariff" });

            if (DB.connects.Any(t => t.tarif == id && t.enabled != 2))
            {
                mess.type = MessageType.Bad;
                mess.text = "This tariff is used. Removing is only possible without connections to it.";
                //HttpContext.Response.StatusCode = 426;
                return PartialView("Message", mess);
            }

            try
            {
                tariff.enabled = 2;
                tariff.dt_last = DateTime.Now;

                DB.SaveChanges();
            }
            catch (Exception error)
            {
                mess.type = MessageType.Bad;
                mess.text = error.Message;
                //HttpContext.Response.StatusCode = 500;
                return PartialView("Message", mess);
            }

            mess.type = MessageType.Good;
            mess.text = "Tariff was successfully removed";

            return PartialView("Message", mess);
        }
        #endregion

        #region Tariff block
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult TariffBlock(int id)
        {
            tarif model = Tariff(id);

            return PartialView(model);
        }
        #endregion

        // non action
        #region Get user's portfolios
        private List<Portfolio> GetUserPortfolios(int user_nmbr)
        {
            // select all user's connects
            IEnumerable<ConnectModel> connects = from c in DB.connects
                                                 join s in DB.systems on c.system equals s.nmbr
                                                 join t in DB.tarifs on c.tarif equals t.nmbr
                                                 where c.user == user_nmbr && c.enabled != 2
                                                 select new ConnectModel()
                                                 {
                                                     system_name = s.name,
                                                     connect = c,
                                                     tariff = t
                                                 };

            // select user's all accounts to portfolios
            List<Portfolio> portfolios = (from a in DB.accounts
                                          join c in connects on a.nmbr equals c.connect.account2 into con
                                          where a.user == user_nmbr
                                          select new Portfolio()
                                          {
                                              connects = from c in con
                                                         where c.connect.account2 == a.nmbr
                                                         select c,
                                              atype = a.atype,
                                              balance = a.balance,
                                              company = a.company,
                                              currency = a.currency,
                                              dt_create = a.dt_create,
                                              dt_last = a.dt_last,
                                              equity = a.equity,
                                              label = a.label,
                                              name = a.name,
                                              nmbr = a.nmbr,
                                              number = a.number,
                                              pl = a.pl,
                                              @public = a.@public,
                                              public_balance = a.public_balance,
                                              public_equity = a.public_equity,
                                              public_number = a.public_number,
                                              serv3online = a.serv3online,
                                              ttype = a.ttype,
                                              uid = a.uid,
                                              user = a.user,
                                              webtrade = a.webtrade
                                          }).ToList();

            portfolios.Add(new Portfolio()
            {
                connects = connects.Where(c => c.connect.account2 == 0)
            });

            return portfolios;
        } 
        #endregion

        #region Get user copies
        public IEnumerable<CopyModel> GetUserCopies(int user_nmbr)
        {
            return (from cop in DB.copys
                    join a in DB.accounts on cop.account equals a.nmbr
                    join sig in DB.signals on cop.signal equals sig.nmbr
                    join sys in DB.systems on sig.system equals sys.nmbr
                    where cop.user == user_nmbr && cop.err == 1
                    select new CopyModel()
                    {
                        enabled = cop.enabled,
                        account_number = a.number,
                        account_company = a.company,
                        system_nmbr = sys.nmbr,
                        system_name = sys.name,
                        pl = cop.profit_client,
                        pts = cop.profit_client_pip,

                        nmbr = cop.nmbr,
                        volume = cop.volume,
                        price = cop.price,
                        symbol = sig.symbol,
                        digits = sig.digits,
                        dt_create = cop.dt_create
                    });
        }
        #endregion

        #region Get user systems
        public List<SystemModel> GetUserSystems(int user_nmbr)
        {
            return (from s in DB.systems
                    join t in DB.tarifs on s.nmbr equals t.system into tariffs
                    join rs in DB.rsystems on s.nmbr equals rs.system
                    where s.user == user_nmbr && s.enabled != 2 && rs.connect == 0
                    orderby s.enabled descending, s.desc
                    select new SystemModel()
                    {
                        nmbr = s.nmbr,
                        name = s.name,

                        profit = from dp in DB.daily_profit
                                 where dp.connect == 0 && dp.system == s.nmbr
                                 orderby dp.nmbr
                                 select new ProfitChart
                                 {
                                     date = dp.date,
                                     pl = dp.pl,
                                     pts = dp.pts,
                                 },

                        tariffs = (from t in tariffs
                                   where t.system == s.nmbr && t.enabled != 2
                                   select t),

                        pl = rs.profit + rs.loss,
                        pts = rs.profit_pip + rs.loss_pip,

                        drawdown = rs.maxdd,
                        pdrawdown = s.pdrawdown, // maxdd_pip

                        clients = rs.clients,

                        opened = (from trade in DB.trades
                                  where trade.system == s.nmbr & trade.connect == 0
                                  select trade).Count(),

                        closed = rs.trades,

                        signals = rs.signals,

                        last_signal_time = null,

                        serv3online = s.serv3online,
                        enabled = s.enabled,
                        dt_create = s.dt_create
                    }).ToList();
        }
        #endregion

        // ---------------------------------------------------------------

        // PROFILE

        #region Profile
        [Authorize]
        public ActionResult Profile()
        {
            int nmbr = int.Parse(User.Identity.Name);
            
            user usr = (from user in DB.users
                       where user.nmbr == nmbr
                       select user).First();

            RegisterModel model = new RegisterModel()
            {
                Email = usr.email,
                FIO = usr.fio,
                Addr = usr.addr,
                Foto = usr.photo,
                Phone = usr.phone,
                ICQ = usr.icq,
                Skype = usr.skype,
                Experience = usr.experience,
                Trade_type = ConvertToCheckBox(usr.trade_type),
                Trade_style = ConvertToCheckBox(usr.trade_style),
                Trade_markets = ConvertToCheckBox(usr.trade_markets),
                Trade_sessions = ConvertToCheckBox(usr.trade_sessions),
                Desc = usr.desc
            };

            ViewData["ExperienceList"] = new SelectList(Pipular.Controllers.AccountController.experienceList, "Value", "Text", usr.experience);

            return View(model);
        }
        #endregion

        #region Profile POST
        [HttpPost]
        [Authorize]
        public ActionResult Profile(RegisterModel model)
        {
            // get user
            int nmbr = int.Parse(User.Identity.Name);

            user usr = (from user in DB.users
                        where user.nmbr == nmbr
                        select user).First();     

            usr.email = model.Email;
            usr.fio = model.FIO;

            if (model.Password != usr.password)
                usr.password = Pipular.Controllers.AccountController.GetMD5Hash(model.ConfirmPassword);
            
            usr.photo = model.Foto;
            usr.phone = model.Phone;
            usr.addr = model.Addr;
            usr.icq = model.ICQ;
            usr.skype = model.Skype;
            usr.experience = model.Experience ?? 0;
            usr.trade_type = Pipular.Controllers.AccountController.GetCheckBoxCode(model.Trade_type);
            usr.trade_style = Pipular.Controllers.AccountController.GetCheckBoxCode(model.Trade_style);
            usr.trade_markets = Pipular.Controllers.AccountController.GetCheckBoxCode(model.Trade_markets);
            usr.trade_sessions = Pipular.Controllers.AccountController.GetCheckBoxCode(model.Trade_sessions);
            usr.desc = model.Desc;

            // Compare email
            if (model.Email.Trim() != usr.email)
            {
                //send conf email
                try
                {
                    DateTime dt_conf = DateTime.Now.AddMinutes(int.Parse(ConfigurationManager.AppSettings["Delay_Confirm_Email"])); // with time delay
                    string dt_conf_hash = Pipular.Controllers.AccountController.GetMD5Hash(dt_conf.ToString());
                    usr.conf_code = dt_conf_hash;

                    string domen = Request.ServerVariables["HTTP_HOST"];
                    string confirmation_link = "http://" + domen + "/Account/Confirm_email?uid=" + usr.nmbr + "&code=" + dt_conf_hash;
                    string message = "Hellow " + usr.fio + "!<p><b>You change the password. Your confirmation link: </b><a href=\"" + confirmation_link + "\" target=_blank >" + confirmation_link + "</a></p>";

                    Pipular.Controllers.AccountController.SendMail(usr.email, message, "E-mail was changed. Confirm please.");
                }
                catch (Exception error)
                {
                    ModelState.AddModelError("", error.Message);
                    return View(model);
                }
            }

            // Save database
            try
            {
                DB.SaveChanges();
            }
            catch (Exception error)
            {
                ModelState.AddModelError("", error.Message);
                return View(model);
            }

            bw.DoWork += serv3.SendCMD;
            bw.RunWorkerAsync(new string[] { "EDIT_USER", "users", usr.nmbr.ToString() });

            return RedirectToAction("Profile", "Private");
        }
        #endregion

        // MY SYSTEMS

        #region My systems
        [Authorize]
        public ActionResult MySystems()
        {
            int nmbr = int.Parse(User.Identity.Name);

            List<SystemModel> my_systems = (from system in DB.systems
                                              where system.user == nmbr & system.enabled != 2
                                              orderby system.enabled descending, system.desc
                                              select new SystemModel()
                                              {
                                                  nmbr = system.nmbr,
                                                  name = system.name,

                                                  pl = (from history in DB.historys
                                                        where history.system == system.nmbr & history.connect == 0
                                                        select history.pl).Sum(),
                                                        /*
                                                        .Union(from trade in DB.trades
                                                                                 where trade.system == system.nmbr
                                                                                 select trade.pl)
                                                                                 */
                                                  pts = (from history in DB.historys
                                                         where history.system == system.nmbr & history.connect == 0
                                                         select history.pts).Sum(),

                                                  drawdown = system.drawdown,
                                                  pdrawdown = system.pdrawdown,

                                                  clients = (from connect in DB.connects
                                                             where connect.system == system.nmbr & connect.enabled == 1
                                                             select connect).Count(),

                                                  opened = (from trade in DB.trades
                                                            where trade.system == system.nmbr & trade.connect == 0
                                                            select trade).Count(),

                                                  closed = (from history in DB.historys
                                                            where history.system == system.nmbr & history.connect == 0
                                                            select history).Count(),

                                                  signals = (from signal in DB.signals
                                                             where signal.system == system.nmbr
                                                             select signal).Count(),

                                                  last_signal_time = (from signal in DB.signals
                                                                      where signal.system == system.nmbr
                                                                      select signal.dt_create).Max(),
                                                  
                                                  serv3online = system.serv3online,
                                                  enabled = system.enabled,
                                                  dt_create = system.dt_create
                                              }).ToList();

            List<tarif> my_tarifs = (from system in DB.systems
                                          join t in DB.tarifs on system.nmbr equals t.system
                                          where system.user == nmbr & t.enabled != 2
                                          select t).ToList();

            ViewData["my_tarifs"] = my_tarifs;
            ViewData["my_systems"] = my_systems;
            
            return View();
        }
        #endregion

        #region Edit my system
        [Authorize]
        public ActionResult EditMySystem(int id)
        {
            system sys = (from system in DB.systems
                          where system.nmbr == id
                          select system).First();

            if (sys.user != int.Parse(User.Identity.Name))
                return RedirectToAction("AccessDenied", "Private", new { message = "Access denied. You are trying to edit foreign system" });

            AddSystemModel model = new AddSystemModel()
            {
                nmbr = sys.nmbr,
                enabled = sys.enabled,
                Name = sys.name,
                Desc = sys.desc,
                Desc_html = sys.desc_html,
                Trade_type = ConvertToCheckBox(sys.trade_type),
                Trade_style = ConvertToCheckBox(sys.trade_style),
                Trade_markets = ConvertToCheckBox(sys.trade_markets),
                Trade_sessions = ConvertToCheckBox(sys.trade_sessions)
            };

            return View(model);
        }
        #endregion

        #region Edit my system POST
        [Authorize]
        [HttpPost]
        public ActionResult EditMySystem(AddSystemModel model, int id)
        {
            try
            {
                system sys = (from system in DB.systems
                              where system.nmbr == id
                              select system).First();

                if (sys.user != int.Parse(User.Identity.Name))
                    return RedirectToAction("AccessDenied", "Private", new { message = "Access denied. You are trying to edit foreign system" });

                sys.enabled = model.enabled;
                sys.name = model.Name;
                sys.desc = model.Desc;
                sys.desc_html = model.Desc_html;
                sys.trade_type = AccountController.GetCheckBoxCode(model.Trade_type);
                sys.trade_style = AccountController.GetCheckBoxCode(model.Trade_style);
                sys.trade_markets = AccountController.GetCheckBoxCode(model.Trade_markets);
                sys.trade_sessions = AccountController.GetCheckBoxCode(model.Trade_sessions);

                DB.SaveChanges();

                bw.DoWork += serv3.SendCMD;
                bw.RunWorkerAsync(new string[] { "EDIT_SYSTEM", "systems", sys.nmbr.ToString(), User.Identity.Name });
            }
            catch (Exception error)
            {
                ModelState.AddModelError("", error.Message);
                return View(model);
            }
            
            return RedirectToAction("MySystems", "Private");
        }
        #endregion

        // FOLLOW SYSTEM

        #region Go to follow
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult GoToFollow(int id)
        {
            FollowModel model = new FollowModel()
            {
                tariffs = Tariffs(t => t.enabled == 1 && t.system == id && t.@for == 0),
                system_name = DB.systems.Single(s => s.nmbr == id).name
            };

            return PartialView(model);
        } 
        #endregion
        
        #region Connect system async
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult ConnectSystem(int tarif_id)
        {
            Message mess = new Message();

            try
            {
                tarif tar = Tariff(tarif_id);

                system sys = (from system in DB.systems
                              where system.nmbr == tar.system
                              select system).First();
                
                int user_nmbr = int.Parse(User.Identity.Name);

                /* Connect the same system
                if (DB.connects.Any(c => c.user == user_nmbr & c.system == sys.nmbr & c.enabled != 2))
                {
                    mess.text = "You are already connected to this system.";
                    mess.type = MessageType.Middle;
                    return PartialView("Message", mess);
                }
                */

                if (DB.systems.Any(s => s.nmbr == sys.nmbr && s.user == user_nmbr))
                {
                    mess.text = "You can't follow your self system.";
                    mess.type = MessageType.Middle;
                    return PartialView("Message", mess);
                }

                if (!DB.balances.Any(b => b.user == user_nmbr))
                {
                    mess.text = "Not enough money.";
                    mess.type = MessageType.Middle;
                    return PartialView("Message", mess);
                }

                double user_balance = (from balance in DB.balances
                                       where balance.user == user_nmbr & balance.active == 1
                                       orderby balance.dt_create
                                       select balance.summa).Sum();
                if (user_balance < 0)
                {
                    mess.text = "Negative balance.";
                    mess.type = MessageType.Middle;
                    return PartialView("Message", mess);
                }

                int now7 = (int)(DateTime.Now.AddDays(7) - new DateTime(1899, 12, 30)).TotalDays;

                int mt4_end = now7;
                int email_end = now7;
                int icq_end = now7;
                int phone_end = now7;
                int skype_end = now7;

                switch (tar.type)
                {
                    case 0: // Days
                        if (user_balance >= tar.summa)
                        {
                            int now_tsize = (int)(DateTime.Now.AddDays(tar.tsize) - new DateTime(1899, 12, 30)).TotalDays;

                            mt4_end = now_tsize;
                            email_end = now_tsize;
                            icq_end = now_tsize;
                            phone_end = now_tsize;
                            skype_end = now_tsize;
                        }
                        else
                        {
                            mess.text = "Not enough money.";
                            mess.type = MessageType.Middle;
                            return PartialView("Message", mess);
                        }
                        break; 
                    default:  // Pips and percents
                        break;
                }

                connect new_connect = new connect()
                    {
                        system = sys.nmbr,
                        tarif = tar.nmbr,
                        user = int.Parse(User.Identity.Name),
                        enabled = 1,
                        dt_create = DateTime.Now,

                        mt4 = tar.mt4,
                        email = tar.email,
                        icq = tar.icq,
                        phone = tar.phone,
                        skype = tar.skype,

                        mt4_end = mt4_end,
                        email_end = email_end,
                        icq_end = icq_end,
                        phone_end = phone_end,
                        skype_end = skype_end
                    };

                DB.connects.AddObject(new_connect);
                DB.SaveChanges();

                ViewBag.code = 0;

                bw.DoWork += serv3.SendCMD;
                bw.RunWorkerAsync(new string[] { "ADD_CONNECT", "connects", new_connect.nmbr.ToString(), User.Identity.Name });

                return RedirectToAction("ConnectBlock", new { id = new_connect.nmbr });
            }
            catch (Exception error)
            {
                mess.text = error.Message;
                mess.type = MessageType.Bad;
                return PartialView("Message", mess);
            }
        }
        #endregion

        // SIGNALS

        #region Go to copy
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult GoToCopy(int id, string uid, int time)
        {
            int user_nmbr = 0;
            
            if (!int.TryParse(User.Identity.Name, out user_nmbr)) 
                ViewBag.Error = "Wrong user number. Try to relogin.";
            else
            {
                signal signal;
                IQueryable<tarif> tariffs;

                if (id > 0)
                    signal = DB.signals.First(s => s.nmbr == id);
                else
                    signal = DB.signals.First(s => s.uid == uid && s.dt_terminal == time);

                if (signal == null)
                    ViewBag.Error = "Can't detect signal information.";
                else
                {
                    tariffs = DB.tarifs.Where(t => t.system == signal.system && t.enabled == 1);

                    if (tariffs == null)
                        ViewBag.Error = "This system have no tariffs.";
                    else
                    {
                        if (DB.connects.Any(c => c.user == user_nmbr && c.system == signal.system && c.enabled == 1 && c.mt4 == 1))
                            ViewBag.Error = "You can't copy signals from connected systems.";
                        else
                        {
                            if (DB.signals.Any(s => s.uid == uid && s.stype == 2))
                                ViewBag.Error = "Trade already closed.";
                            else
                            {
                                LiveTradeSignal live_signal = null;
                                LiveSystemModel live_system = null;

                                SignalInfo.GetInfo(signal, out live_signal, out live_system);

                                ViewData["live_signal"] = live_signal;
                                ViewData["tariffs"] = tariffs;

                                ViewData["user_accounts"] = DB.accounts.Where(a => a.user == user_nmbr).ToList();

                                return PartialView();
                            }
                        }
                    }
                }
            }

            return PartialView("GoCopyError");
        }
        #endregion

        // BALANCE

        #region Balance
        [Authorize]
        public ActionResult Balance()
        {
            int user_nmbr = int.Parse(User.Identity.Name);

            List<balance> model = (from balance in DB.balances
                                   where balance.user == user_nmbr & balance.active == 1
                                   orderby balance.dt_create descending
                                   select balance).ToList();

            return View(model);
        }
        #endregion

        #region Fill up balance
        [Authorize]
        public ActionResult FillUpBalance()
        {
            return PartialView();
        }
        #endregion

        #region Fill up async
        [Authorize]
        [HttpPost]
        public ActionResult FillUp(FillUpBalanceModel model)
        {
            try
            {
                balance new_balance = new balance()
                {
                    user = int.Parse(User.Identity.Name),
                    summa = model.summa,
                    comment = model.comment,
                    active = 1,
                    dt_create = DateTime.Now
                };

                DB.balances.AddObject(new_balance);
                DB.SaveChanges();
            }
            catch (Exception error)
            {
                ViewBag.Message = error.Message;
                return PartialView();
            }

            ViewBag.Message = "Balance refilled successfully.";

            return PartialView();
        }
        #endregion

        // OTHER

        #region Accounts
        [Authorize]
        public ActionResult Accounts()
        {
            int nmbr = int.Parse(User.Identity.Name);

            List<account> account_list = (from account in DB.accounts
                                         where account.user == nmbr
                                         select account).ToList();

            ViewData["Account_list"] = account_list;

            return View();
        }
        #endregion

        #region Access denied
        public ActionResult AccessDenied(string message)
        {
            ViewBag.Message = message;
            
            return View();
        }
        #endregion
        
        // NON ACTION

        #region  Find checked checkboxes
        [NonAction]
        public static List<int> ConvertToCheckBox(int value)
        {
            List<int> list = new List<int>();

            string bits = Convert.ToString(value, 2).TrimStart('0');

            int l = bits.Length - 1;

            for (int i = 0; i <= l; i++)
            {
                if (bits[i] == '1') list.Add((int)Math.Pow(2, l-i));
            }

            return list;
        }
        #endregion

        #region Get all connects
        [NonAction]
        public List<ConnectModel> GetConnects()
        {
            int user_nmbr = int.Parse(User.Identity.Name);

            List<ConnectModel> model = (from connect in DB.connects
                                        join system in DB.systems on connect.system equals system.nmbr
                                        join tarif in DB.tarifs on connect.tarif equals tarif.nmbr
                                        join account in DB.accounts on connect.account equals account.nmbr into accounts
                                        where connect.user == user_nmbr & connect.enabled != 2
                                        from a in accounts.DefaultIfEmpty()
                                        select new ConnectModel()
                                        {
                                            system_name = system.name,
                                            connect = connect,
                                            tariff = tarif
                                        }).ToList();
            return model;
        }
        #endregion

        #region Get connects by portfolio
        [NonAction]
        public List<ConnectModel> GetConnects(int portfolio)
        {
            return GetConnects().Where(m => m.connect.account2 == portfolio).ToList();
        }
        #endregion

        #region Get connect (by nmbr)
        private ConnectModel GetConnect(int nmbr)
        {
            return (from c in DB.connects
                    join s in DB.systems on c.system equals s.nmbr
                    join t in DB.tarifs on c.tarif equals t.nmbr
                    where c.nmbr == nmbr
                    select new ConnectModel()
                    {
                        system_name = s.name,
                        connect = c,
                        tariff = t
                    }).Single();
        } 
        #endregion

        #region Get System (by nmbr)
        private SystemModel GetSystem(int nmbr)
        {
            return (from s in DB.systems
                    join t in DB.tarifs on s.nmbr equals t.system into tariffs
                    join rs in DB.rsystems on s.nmbr equals rs.system into rsys
                    from rs in rsys.DefaultIfEmpty()
                    where s.nmbr == nmbr && s.enabled != 2 && rs.connect == 0
                    orderby s.enabled descending, s.desc
                    select new SystemModel()
                    {
                        nmbr = s.nmbr,
                        name = s.name,

                        profit = from dp in DB.daily_profit
                                 where dp.connect == 0 && dp.system == s.nmbr
                                 orderby dp.nmbr
                                 select new ProfitChart
                                 {
                                     date = dp.date,
                                     pl = dp.pl,
                                     pts = dp.pts,
                                 },

                        tariffs = (from t in tariffs
                                   where t.system == s.nmbr && t.enabled != 2
                                   select t),

                        pl = rs.profit + rs.loss,
                        pts = rs.profit_pip + rs.loss_pip,

                        drawdown = rs.maxdd,
                        pdrawdown = s.pdrawdown, // maxdd_pip

                        clients = rs.clients,

                        opened = (from trade in DB.trades
                                  where trade.system == s.nmbr & trade.connect == 0
                                  select trade).Count(),

                        closed = rs.trades,

                        signals = rs.signals,

                        last_signal_time = null,

                        serv3online = s.serv3online,
                        enabled = s.enabled,
                        dt_create = s.dt_create
                    }).Single();
        } 
        #endregion
    }
}