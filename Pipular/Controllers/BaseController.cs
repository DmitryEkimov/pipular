﻿using Pipular.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Pipular.Controllers
{
    public class BaseController : Controller
    {
        public pipularEntities DB = new pipularEntities();

        public tarif Tariff(int nmbr) { return GetTariff(DB, nmbr); }
        public IQueryable<tarif> Tariffs(Expression<Func<tarif, bool>> predicate) { return GetTariffs(DB).Where(predicate); }

        // get tariff by id
        public readonly Func<pipularEntities, int, tarif> GetTariff =
            CompiledQuery.Compile<pipularEntities, int, tarif>((p, id) => p.tarifs.FirstOrDefault(t => t.nmbr == id));
        // get tariffs
        public readonly Func<pipularEntities, IQueryable<tarif>> GetTariffs =
            CompiledQuery.Compile<pipularEntities, IQueryable<tarif>>((p) => p.tarifs);
    }
}
