﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pipular.Models;
using Pipular.Accounting;

namespace Pipular.Controllers
{
    public class AdminController : Controller
    {
        pipularEntities DB = new pipularEntities();

        public ActionResult Index()
        {            
            return View();
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Accounting()
        {
            return View();
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult DeleteRecount(DateTime start, DateTime stop)
        {
            try
            {
                RecountResultCollection.Delete(new RecountResult() { start = start, stop = stop });
                return RecountResultTable();
            }
            catch (Exception ex) { return PartialView("RecountResult", String.Format("Error message: {0}\r\nInner exception: {1}", ex.Message, ex.InnerException) as object); }
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Recount()
        {
            try
            {
                Accounting.Accounting service = new Accounting.Accounting(AccountingManager.Instance);
                service.Recount();

                return RecountResultTable();
            }
            catch (Exception ex) { return PartialView("RecountResult", String.Format("Message: {0}\r\nInner exception: {1}", ex.Message, ex.InnerException) as object); }
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult RecountThis(RecountResult recountResult)
        {
            try
            {
                Accounting.Accounting service = new Accounting.Accounting(AccountingManager.Instance);
                service.RecountThis(recountResult);

                return PartialView("RecountResult", "Complete" as object);
            }
            catch (Exception ex) { return PartialView("RecountResult", String.Format("Message: {0}\r\nInner exception: {1}", ex.Message, ex.InnerException) as object); }
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult RecountFromLast()
        {
            try
            {
                Accounting.Accounting service = new Accounting.Accounting(AccountingManager.Instance);
                service.RecountFromLast();

                return RecountResultTable();
            }
            catch (Exception ex) { return PartialView("RecountResult", String.Format("Message: {0}\r\nInner exception: {1}", ex.Message, ex.InnerException) as object); }
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult RecountingDetails(RecountResult recountResult)
        {
            IEnumerable<accounting> model = from a in DB.accountings.Include("User_").Include("System_")
                                            where a.dt_start == recountResult.start && a.dt_stop == recountResult.stop
                                            select a;

            return View(model);
        }

        public ActionResult RecountResultTable()
        {
            return PartialView("RecountResultTable", RecountResultCollection.Load());
        }
    }
}
