﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pipular.Models;
using Pipular.Models.Charts;

namespace Pipular.Controllers
{
    public class ChartController : Controller
    {
        // JSON

        pipularEntities DB = new pipularEntities();

        [HttpGet]
        [CompressFilter]
        public JsonResult SystemHistory(int id, DateTime? start, DateTime? stop)
        {
            return ConnectHistory(id, 0, start, stop);
        }

        [HttpGet]
        [CompressFilter]
        public JsonResult SystemMiniHistory()
        {
            var profit = from p in DB.daily_profit
                         where p.connect == 0 && p.System_.enabled == 1
                         group p by p.system into pGroup
                         select new
                         {
                             system = pGroup.Key,
                             profit = from pg in pGroup
                                      orderby pg.date
                                      select new ProfitChart
                                      {
                                          date = pg.date,
                                          pl = pg.pl,
                                          pts = pg.pts
                                      }
                         };

            return this.Json(profit, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CompressFilter]
        public JsonResult ConnectHistory(int system, int connect, DateTime? start, DateTime? stop)
        {
            return GetHistory(system, connect, start, stop);
        }

        public JsonResult GetHistory(int system, int connect, DateTime? start, DateTime? stop)
        {
            var dailyProfit = start.HasValue && stop.HasValue ? GetDailyProfit(system, connect, start.Value, stop.Value) : GetDailyProfit(system, connect);
            var dailyDD = start.HasValue && stop.HasValue ? GetDailyDD(system, connect, start.Value, stop.Value) : GetDailyDD(system, connect);

            var chart = new { profit = dailyProfit, drawdown = dailyDD };

            return this.Json(chart, JsonRequestBehavior.AllowGet);
        }

        private IQueryable<ProfitChart> GetDailyProfit(int system, int connect)
        {
            return from dp in DB.daily_profit
                   where dp.system == system && dp.connect == connect
                   select new ProfitChart
                   {
                       pl = dp.pl,
                       pts = dp.pts,
                       date = dp.date
                   };
        }

        private IQueryable<ProfitChart> GetDailyProfit(int system, int connect, DateTime start, DateTime stop)
        {
            return GetDailyProfit(system, connect).Where(p => p.date > start && p.date <= stop);
        }

        private IQueryable<DailyDD> GetDailyDD(int system, int connect)
        {
            return from dd in DB.daily_dd
                   where dd.system == system && dd.connect == connect
                   select new DailyDD
                   {
                       dd = dd.dd,
                       dd_pip = dd.dd_pip,
                       date = dd.date
                   };
        }

        private IQueryable<DailyDD> GetDailyDD(int system, int connect, DateTime start, DateTime stop)
        {
            return GetDailyDD(system, connect).Where(d => d.date > start && d.date <= stop);
        }
    }
}
