﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Globalization;
using Itenso.TimePeriod;
using Pipular.Models;
using System.Linq.Expressions;

namespace Pipular
{
    public static class Extension
    {
        /// <summary>
        /// Parse a xml string to hashtable
        /// </summary>
        /// <param name="xml">Xml string</param>
        public static Hashtable XMLParse(string xml)
        {
            Hashtable ht = new Hashtable();

            if (xml != "")
            {
                xml = xml.Insert(0, @"<?xml version=""1.0""?><reply>");
                xml += "</reply>"; 
                
                XDocument xd = XDocument.Parse(xml);
                IEnumerable<XNode> list = xd.Root.Nodes();
                
                foreach (XElement element in list)
                    ht.Add(element.Name.LocalName, element.Value);
            }

            return ht;
        }

        public static DateTime UnixToDate(this int unix)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(unix);
        }

        public static TimeSpan UnixTimeSpan(this int seconds)
        {
            return new TimeSpan(0, 0, seconds);
        }

        public static TimeSpan UnixTimeSpan(this double seconds)
        {
            return UnixTimeSpan((int)Math.Round(seconds, 0));
        }

        public static string TimeSpanToString(this TimeSpan ts)
        {
            int days = (int)Math.Truncate(ts.TotalDays);
            int hours = (int)Math.Truncate(ts.TotalHours);
            int min = (int)Math.Truncate(ts.TotalMinutes);

            if (days == 0)
            {
                if (hours == 0)
                {
                    if (min == 0) return String.Format("{0} sec", ts.Seconds);
                    return String.Format("{0} min", ts.Minutes);
                }
                return String.Format("{0} hours {1} min", ts.Hours, ts.Minutes);
            }

            return String.Format("{0} days {1} hours {2} min", ts.Days, ts.Hours, ts.Minutes);
        }

        public static int DateToUnix(this DateTime dt)
        {
            return (int)(dt - new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public static int Round(this double n)
        {
            return (int)Math.Round(n, 0);
        }

        public static double Round(this double n, int numbers)
        {
            return Math.Round(n, numbers);
        }

        public static DateTime GetPeriod(int period)
        {
            DateTime now = DateTime.UtcNow;
            
            switch (period)
            {
                case 1: // Last hour
                    return now.AddHours(-1);
                case 2: // Last 3 hours
                    return now.AddHours(-3);
                case 3: // Today
                    return DateTime.Today;
                case 4: // Last day
                    return now.AddDays(-1);
                case 5: // This week
                    return now.StartOfWeek(DayOfWeek.Monday);
                case 6: // Last week
                    return now.AddDays(-7);
                case 7: // This month
                    return now.AddDays(-now.Day);
                case 8: // Last month
                    return now.AddMonths(-1);
                case 9: // Last 3 months
                    return now.AddMonths(-3);
                case 10: // Last year
                    return now.AddYears(-1);
                case 0: // Ever
                    return new DateTime(1,1,1);
                default: 
                    return now;
            }
        }

        private static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public static string GetTimeSpanDescription(TimeSpan timeSpan, int precision = int.MaxValue)
        {
            return GetTimeSpanDescription(DateTime.UtcNow, timeSpan, precision);
        } // GetTimeSpanDescription

        public static double Variance(this IEnumerable<double> sourse)
        {
            double res = 0;
            double avg = sourse.Average();

            foreach (double s in sourse) { res += Math.Pow(s - avg, 2); }

            return res / sourse.Count();
        }

        public static DateTime Round(this DateTime dt)
        {
            return new DateTime((long)Math.Truncate((double)dt.Ticks / 10000000) * 10000000);
        }

        // ----------------------------------------------------------------------
        public static string GetTimeSpanDescription(DateTime moment, TimeSpan timeSpan, int precision = int.MaxValue)
        {
            if (timeSpan == TimeSpan.Zero)
            {
                return string.Empty;
            }

            bool isPositive = timeSpan > TimeSpan.Zero;
            DateTime date1 = isPositive ? moment : moment.Subtract(timeSpan);
            DateTime date2 = isPositive ? moment.Add(timeSpan) : moment;

            return new DateDiff(date1, date2).GetDescription(precision);
        }

        private static double ServerOffset()
        {
            return DateTimeOffset.Now.Offset.TotalMinutes;
        }
        
        private static double GetOffsetByIP()
        {
            double offset = 0;

            try
            {
                string ip = HttpContext.Current.Request.UserHostAddress;
                if (ip == "127.0.0.1") ip = "89.189.191.40";
                WebRequest reqGET = WebRequest.Create(@"http://api.ipinfodb.com/v3/ip-city/?format=xml&key=f4c33c210690a902ec299f757fe10b1fa8d4601ca73aee3861fcf8dcce34b725&ip=" + ip);
                WebResponse resp = reqGET.GetResponse();
                Stream stream = resp.GetResponseStream();
                StreamReader sr = new StreamReader(stream);
                string result = sr.ReadToEnd();
                XDocument resultXMLDoc = XDocument.Parse(result);
                IEnumerable<XElement> root = resultXMLDoc.Root.Elements();
                if (root.Any(e => e.Name.LocalName == "timeZone"))
                {
                    string offset_str = root.Single(e => e.Name.LocalName == "timeZone").Value;
                    DateTimeOffset dt_offset;
                    DateTimeOffset.TryParseExact(offset_str, "zzz", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt_offset);
                    offset = Math.Round(dt_offset.Offset.TotalMinutes) - ServerOffset();
                }
            }
            catch (WebException exeption)
            {
                HttpContext.Current.Response.Cookies["error"].Value = exeption.Message;
                return 0;
            }

            return offset;
        }

        // Time offset between Server and Client
        public static double ClientServerOffset()
        {
            HttpRequest request = HttpContext.Current.Request;
            double offset = 0;
            
            if (request.Cookies["offset"] == null)
            {
                offset = GetOffsetByIP();               
                HttpContext.Current.Response.Cookies["offset"].Value = offset.ToString();
            }
            else
            {
                double.TryParse(request.Cookies["offset"].Value, out offset);
            }

            return offset; 
        }

        public static DateTime ToClientTime(this DateTime dt)
        {
            return dt == null ? new DateTime() : dt.AddMinutes(ClientServerOffset());
        }

        public static DateTime ToClientTime(this DateTime? dt)
        {
            return dt == null ? new DateTime() : dt.Value.AddMinutes(ClientServerOffset());
        }
        
        public static IEnumerable<T> ToClientTime<T>(this IEnumerable<T> sourse, string field, Func<T,DateTime> func)
        {
            double offset = ClientServerOffset();
            
            foreach (T item in sourse)
            {
                DateTime dt = ((DateTime)item.GetType().GetProperty(field).GetValue(item, null)).AddMinutes(offset);
                item.GetType().GetProperty(field).SetValue(item, func(item).AddMinutes(offset), null);
            }

            return sourse;
        }
        
        /// <summary>
        /// Get client local time
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourse"></param>
        /// <param name="func">field selector</param>
        /// <returns></returns>
        public static IEnumerable<T> ToClientTime<T>(this IEnumerable<T> sourse, Action<T, double> func)
        {
            double offset = ClientServerOffset();

            foreach (T item in sourse)
            {
                func(item, offset);
            }

            return sourse;
        }

        /// <summary>
        /// Cut string end
        /// </summary>
        /// <param name="str">Sourse string</param>
        /// <param name="symbols">Number of total symbols</param>
        /// <returns></returns>
        public static string Cut(this string str, int symbols)
        {
            if (str.Length > symbols) return str.Substring(0, symbols - 2) + "...";
            else return str;
        }

        // returns the number of milliseconds since Jan 1, 1970 (useful for converting C# dates to JS dates)
        public static double UnixTicks(this DateTime dt)
        {
            DateTime d1 = new DateTime(1970, 1, 1);
            DateTime d2 = dt.ToUniversalTime();
            TimeSpan ts = new TimeSpan(d2.Ticks - d1.Ticks);
            return ts.TotalMilliseconds;
        }

        // returns the number of milliseconds since Jan 1, 1970 (useful for converting C# dates to JS dates)
        public static IEnumerable<T> UnixTicks<T>(this IEnumerable<T> sourse, Converter<DateTime, Double> converter)
        {

            return sourse;
        }
    }
}