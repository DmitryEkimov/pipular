﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Pipular.Controllers;
using System.Text;

namespace Pipular
{
    public static class SystemInfo
    {
        public static MvcHtmlString GetTradeType(this HtmlHelper helper, int trade_type)
        {
            string s = "<span class=\"block\"><span class=\"underline\">Trade type:</span> ";
            switch (trade_type)
            {
                case 1: // automatic
                    return MvcHtmlString.Create(s + "automatic</span>");
                case 2: // automatic
                    return MvcHtmlString.Create(s + "semi automatic</span>");
                case 4: // automatic
                    return MvcHtmlString.Create(s + "manual</span>");
                default:
                    return MvcHtmlString.Empty;
            }
        }

        public static MvcHtmlString GetTradeStyle(this HtmlHelper helper, int trade_style)
        {
            StringBuilder sb = new StringBuilder();

            if (trade_style == 0) return MvcHtmlString.Empty;

            List<int> style = PrivateController.ConvertToCheckBox(trade_style);
            sb.Append("<span class=\"block\"><span class=\"underline\">Trade style:</span> ");
            foreach (int s in style)
            {
                switch (s)
                {
                    case 1:
                        sb.Append(" scalping");
                        break;
                    case 2:
                        sb.Append(" swing");
                        break;
                    case 4:
                        sb.Append(" news trading");
                        break;
                }
                sb.Append(",");
            }

            return MvcHtmlString.Create(sb.ToString().TrimEnd(',').Insert(sb.Length-1, "</span>"));
        }

        public static MvcHtmlString GetTradeMarkets(this HtmlHelper helper, int trade_markets)
        {
            StringBuilder sb = new StringBuilder("<span class=\"block\"><span class=\"underline\">Markets:</span> ");

            if (trade_markets == 0) return MvcHtmlString.Empty;

            List<int> markets = PrivateController.ConvertToCheckBox(trade_markets);
            
            foreach (int m in markets)
            {
                switch (m)
                {
                    case 1:
                        sb.Append(" Forex");
                        break;
                    case 2:
                        sb.Append(" Futures");
                        break;
                    case 4:
                        sb.Append(" Metals");
                        break;
                    case 8:
                        sb.Append(" CFDs");
                        break;
                    case 16:
                        sb.Append(" Stocks");
                        break;
                }
                sb.Append(",");
            }

            return MvcHtmlString.Create(sb.ToString().TrimEnd(',').Insert(sb.Length - 1, "</span>"));
        }

        public static MvcHtmlString GetTradeSessions(this HtmlHelper helper, int trade_sessions)
        {
            StringBuilder sb = new StringBuilder("<span class=\"block\"><span class=\"underline\">Sessions:</span> ");

            if (trade_sessions == 0) return MvcHtmlString.Empty;

            List<int> sessions = PrivateController.ConvertToCheckBox(trade_sessions);

            foreach (int s in sessions)
            {
                switch (s)
                {
                    case 1:
                        sb.Append(" Asian");
                        break;
                    case 2:
                        sb.Append(" US");
                        break;
                    case 4:
                        sb.Append(" European");
                        break;
                    case 8:
                        sb.Append(" UK");
                        break;
                }
                sb.Append(",");
            }

            return MvcHtmlString.Create(sb.ToString().TrimEnd(',').Insert(sb.Length - 1, "</span>"));
        }
    }
}