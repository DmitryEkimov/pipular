﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Mvc.Ajax;
using System.Text;
using System.IO;
using Pipular.Models;

namespace Pipular
{
    public static class HelperExtension
    {
        public static MvcHtmlString Pager(this AjaxHelper ajax, string update_id, int index, int total, string action, object data)
        {
            StringBuilder result = new StringBuilder();

            result.Append("<ul>");
            for (int i = 1; i <= total; i++)
            {
                MvcHtmlString page_link;

                if (i == index) page_link = MvcHtmlString.Create(i.ToString());
                else
                    page_link = ajax.ActionLink(i.ToString(), action, new { data, pageNum = i - 1 }, new AjaxOptions()
                    {
                        InsertionMode = InsertionMode.Replace,
                        UpdateTargetId = update_id
                    }, null);
                
                result.Append("<li>" + page_link.ToString() + "</li>");
            }
            result.Append("</ul>");

            return MvcHtmlString.Create(result.ToString());
        }
        /*
        public static MvcHtmlString PageList(this HtmlHelper hmml, int totalPages, string _class, string _id)
        {
            StringBuilder result = new StringBuilder();

            for (int i = 1; i <= totalPages; i++)
            {
                result.Append("<li>" + i.ToString() + "</li>");
            }

            TagBuilder ul = new TagBuilder("ul");

            if (_class.Length > 0) ul.AddCssClass(_class);
            if (_id.Length > 0) ul.MergeAttribute("id", _id);
            ul.InnerHtml = result.ToString();

            return MvcHtmlString.Create(ul.ToString());
        }
        */

        public static MvcHtmlString PageList(this HtmlHelper hmml, int totalPages, string _class, string _id, string innerClass)
        {
            StringBuilder result = new StringBuilder();

            for (int i = 1; i <= totalPages; i++)
            {
                result.Append("<div class=\"" + innerClass + "\">" + i.ToString() + "</div>");
            }

            TagBuilder div = new TagBuilder("div");

            if (_class.Length > 0) div.AddCssClass(_class);
            if (_id.Length > 0) div.MergeAttribute("id", _id);
            div.InnerHtml = result.ToString();

            return MvcHtmlString.Create(div.ToString());
        }

        public static Div Div(this HtmlHelper htmlHelper, string _class, string _id)
        {
            var div = new TagBuilder("div");
            if (_class.Length > 0) div.AddCssClass(_class);
            div.MergeAttribute("id", _id);
            htmlHelper.ViewContext.Writer.WriteLine(div.ToString(TagRenderMode.StartTag));
            return new Div(htmlHelper.ViewContext.Writer);
        }
    }

    #region Generate </DIV>
    public class Div : IDisposable
    {
        private readonly TextWriter _writer;
        
        public Div(TextWriter writer)
        {
            _writer = writer;
        }

        public void Dispose()
        {
            _writer.WriteLine("</div>");
        }
    }
    #endregion
}