﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pipular
{
    public static class Contests
    {
        public static void GetTerms(int type, out string text, out string kind)
        {
            switch (type)
            {
                case 1:
                    text = "Most profitable system";
                    kind = "pips";
                    break;
                case 2:
                    text = "Most popular system";
                    kind = "followers";
                    break;
                case 3:
                    text = "System with Best trade";
                    kind = "pips";
                    break;
                default:
                    text = "[No terms]";
                    kind = "[No kind]";
                    break;
            }
        }

        public static MvcHtmlString GetPeriod(this HtmlHelper helper, DateTime start, DateTime end)
        {
            string text = "";

            TimeSpan ts = end - start;

            text = Extension.GetTimeSpanDescription(ts);

            return new MvcHtmlString(text);
        }
    }
}