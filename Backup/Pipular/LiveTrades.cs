﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Pipular.Models;
using Pipular.Controllers;
using System.Net;
using System.Net.Sockets;
using SignalR.Hubs;
using SignalR.Hosting.AspNet;
using SignalR.Hosting;
using SignalR.Infrastructure;
using System.Globalization;
using System.Configuration;
using System.Text;

namespace Pipular
{
    [HubName("liveTrades")]
    public class LiveTradesHub : Hub
    {
        private readonly LiveTradeManager _manager;
        
        public LiveTradesHub() : this(LiveTradeManager.Instance) { }

        public LiveTradesHub(LiveTradeManager manager)
        {
            _manager = manager;
        }

        public void SendLiveTrade(string signal_view, string system_view)
        {  
            try
            {
                _manager.Broadcast(signal_view, system_view);
            }
            catch (Exception error)
            {
                _manager.Broadcast("<div class=\"signal_contaner\">" + error.Message + "</div>", "<div class=\"system_contaner\">" + error.Message + "</div>");
            }
        }
    }

    public class LiveTradeManager
    {
        private readonly static Lazy<LiveTradeManager> _instance = new Lazy<LiveTradeManager>(() => new LiveTradeManager());

        public static LiveTradeManager Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        public void Broadcast(string signal, string system)
        {
            GetClients().Update(signal, system);
        }

        private static dynamic GetClients()
        {
            return SignalR.GlobalHost.ConnectionManager.GetHubContext<LiveTradesHub>().Clients;
        }
    }

    [HubName("copyResult")]
    public class CopyResultHub : Hub
    {
        public void Copy(dynamic lot, dynamic signal, dynamic uid, dynamic account, dynamic tariff)
        {
            int user_nmbr = 0;
            double dlot = 0;
            int volume = 0;
            double price = 0;
            int RESULT, STATUS, ERR;
            pipularEntities DB = new pipularEntities();
            SERV3 serv3 = new SERV3();
            int signal_id = int.Parse(signal);
            string signal_uid = uid;
            int account_id = int.Parse(account);
            int tariff_id = int.Parse(tariff);
            tarif Tariff = DB.tarifs.First(t => t.nmbr == tariff_id);

            try
            {
                if (!int.TryParse(HttpContext.Current.User.Identity.Name, out user_nmbr))
                    Caller.Response("Wrong user number. Try to relogin.");
                else
                {
                    if (!double.TryParse(lot, NumberStyles.AllowDecimalPoint, CultureInfo.CreateSpecificCulture("en-US"), out dlot))
                        Caller.Response("Wrong lot.");
                    else
                    {
                        if (DB.copys.Any(c => c.account == account_id && c.signal == signal_id)) // if copy exists
                        {
                            copy old_copy = DB.copys.First(c => c.account == account_id && c.signal == signal_id);

                            if (old_copy.err == 1 || old_copy.err == 0)
                                Caller.Response("Signal has been copied before.");
                            else
                            {
                                serv3.SendCopyCMD(Caller, "ADD_COPY", "copys", old_copy.nmbr.ToString(), user_nmbr, out RESULT, out STATUS, out ERR, out price);

                                old_copy.result = RESULT;
                                old_copy.status = STATUS;
                                old_copy.err = ERR;
                                old_copy.price = price;
                                old_copy.tariff_type = Tariff.type2;
                                old_copy.tariff_tsize = Tariff.tsize2;
                                old_copy.tariff_summa = Tariff.summa2;
                                old_copy.dt_create = DateTime.UtcNow;

                                DB.SaveChanges();
                            }
                        }
                        else
                        {
                            volume = (int)Math.Round(dlot * 100);

                            copy new_copy = new copy()
                            {
                                user = user_nmbr,
                                account = account_id,
                                signal = signal_id,
                                volume = volume,
                                enabled = 1,
                                tariff_type = Tariff.type2,
                                tariff_tsize = Tariff.tsize2,
                                tariff_summa = Tariff.summa2,
                                dt_create = DateTime.UtcNow
                            };

                            DB.copys.AddObject(new_copy);
                            DB.SaveChanges();

                            serv3.SendCopyCMD(Caller, "ADD_COPY", "copys", new_copy.nmbr.ToString(), user_nmbr, out RESULT, out STATUS, out ERR, out price);

                            new_copy.result = RESULT;
                            new_copy.status = STATUS;
                            new_copy.err = ERR;
                            new_copy.price = price;

                            DB.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception error)
            {
                Caller.Response(error.Message + "Inner exception: " + error.InnerException);
                Caller.End();
            }

            Caller.End();
        }
    }
}