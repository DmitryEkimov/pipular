﻿/// <reference path="jquery-1.7.2-vsdoc.js" />
/// <reference path="jquery-ui-vsdoc_1.js" />
/// <reference path="common.js" />

$(document).ready(function () {
    $('#portfolios .expandable, #noportfolios .expandable').sortable({
        connectWith: '.expandable',
        revert: 200,
        tolerance: 'pointer',
        helper: 'clone',
        start: function (event, ui) {
            start_portfolio = $(this);
            keyPressed = false;
            if (event.ctrlKey) {
                keyPressed = true;
                ui.item.show();
            }
        },
        receive: function (event, ui) {
            var account = $(ui.item).parent().parent().parent().attr('id');
            var id = $(ui.item).attr('id');
            var url = 'ChangePortfolio/';
            var portfolio_block = $(this).parent().parent();

            if (account == 'noportfolios') account = '0';

            var receive_system = ui.item.find('.system').attr('id');

            if (account != '0' && $(portfolio_block).find('.system#' + receive_system).length > 1) {
                $(ui.sender).sortable('cancel');
                fail(portfolio_block);
            }
            else {
                if (keyPressed) {
                    url = 'ConnectCopy/';
                    ui.item.clone().appendTo(start_portfolio);
                }

                $.ajax({
                    cache: false,
                    url: url + id,
                    data: 'account=' + account,
                    success: function (result) {
                        $(ui.item).replaceWith(result);
                        avail(portfolio_block);
                    }
                });
            }

            recountExpander('.portfolio_block .connects');
        }
    });

    // Configuration popup
    function openConfig(connect_id, title) {
        OpenPopup(title, 500);

        $.ajax({
            cache: false,
            url: 'ConnectConfiguration/' + connect_id,
            beforeSend: function () { },
            success: function (result) { $('#popup #popup_content').html(result); }
        });
    }

    function portfolios(portfolios) {

        $(portfolios).each(function (i) {
            var portfolio = $(this);
            var id = $(portfolio).attr('id');

            $(portfolio).find('.public span').click(function () {
                var span = $(portfolio).find('.public span');
                $.ajax({
                    cache: false,
                    url: 'PortfolioSwitch/' + id,
                    success: function () {
                        if ($(span).attr('title') == 'public') {
                            $(span).attr('title', 'not public');
                            $(span).css('color', '#696969');
                        }
                        else {
                            $(span).attr('title', 'public');
                            $(span).css('color', 'LimeGreen');
                        }
                    }
                });
            });
        });
    }

    expander('.portfolio_block .connects', 500);

    recountExpander('.portfolio_block .connects');

    expander('#noportfolios .connects', 500);
    expander('#systems', 500);

    portfolios('#portfolios .portfolio_block');

    // LEAD

    function Systems(systems) {

        $(systems).each(function (i) {
            var system = $(this);
            var id = $(system).attr('id');

            $(system).find('.enabled span').click(function () {
                var span = $(system).find('.enabled span');
                $.ajax({
                    cache: false,
                    url: 'SystemSwitch/' + id,
                    success: function () {
                        if ($(span).attr('title') == 'on') {
                            $(span).attr('title', 'off');
                            $(span).css('color', '#696969');
                        }
                        else {
                            $(span).attr('title', 'on');
                            $(span).css('color', 'LimeGreen');
                        }
                    }
                });
            });
        });
    }

    Systems('#my_systems .system_block');

    // New system popup
    function openNewSystem() {
        OpenPopup('Add new system', 500);

        $.ajax({
            cache: false,
            url: 'NewSystem',
            beforeSend: function () { },
            success: function (result) { $('#popup #popup_content').html(result); }
        });
    }

    // New tariff popup
    function openNewTariff(system_name, system_id) {
        OpenPopup('Add new tariff for ' + system_name, 500);

        $.ajax({
            cache: false,
            url: 'NewTariff',
            data: 'id=' + system_id,
            beforeSend: function () { },
            success: function (result) { $('#popup #popup_content').html(result); }
        });
    }

    $('#addSystem').button().click(function () {
        openNewSystem();
    });

    expander('#my_systems .system_block .tariffs', 500);

    recountExpander('.system_block .tariffs');
});