﻿/// <reference path="jquery-1.7.2-vsdoc.js" />
/// <reference path="jquery-ui-1.8.23.custom.min.js" />
/// <reference path="jquery-1.8.0.min.js" />
/// <reference path="jquery-ui-vsdoc_1.js" />

function setCookie(name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : ""); 
}

function getCookie(name) {
    var cookie = " " + document.cookie;
    var search = " " + name + "=";
    var setStr = null;
    var offset = 0;
    var end = 0;
    if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = cookie.indexOf(";", offset)
            if (end == -1) {
                end = cookie.length;
            }
            setStr = unescape(cookie.substring(offset, end));
        }
    }
    return (setStr);
}

function getTime() {
    var date = new Date();
    var utc = date.getTime();
    var zone = date.getTimezoneOffset() * 60 * 1000;
    return utc - zone;
}

//setCookie("zone", new Date().getTimezoneOffset(), "01-Jan-2100 00:00:00 GMT", "/");

function getClientWidth() {
    return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientWidth : document.body.clientWidth;
}

function getClientHeight() {
    return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientHeight : document.body.clientHeight;
}

/* PANEL MOVE */

var menuMaxWidth = 250, menuMinWidth = 80;

function moveContentRight() {
    $('body').animate({ paddingLeft: menuMaxWidth }, { duration: 1000, easing: 'easeOutCubic' });
}
function moveContentLeft() {
    $('body').animate({ paddingLeft: menuMinWidth }, { duration: 1000, easing: 'easeOutCubic' });
}

// POPUP
function loading() {
    $('#popup #popup_content').html('<div class="circle_loader"></div>');
}

function OpenPopup(title, width) {
    loading();
    $('#popup').dialog({
        modal: true,
        title: title,
        width: width,
        resizable: false,
        position: [getClientWidth()/2 - width/2, 100]
    });
}

function ClosePopup(title, width) {
    $('#popup').dialog('close');
}

function Error(element, message) {
    $(element).html(message);
}

/* ALERT */

function Message() {
    $('#alert').slideDown(500);
    setTimeout(function () { $('#alert').slideUp(500); }, 5000);
}

function unavail(selector) {
    $(selector).css('opacity', 0.3);
}

function avail(selector) {
    var precolor = $(selector).css('border-color');
    $(selector).css({ borderColor: 'LimeGreen' }).animate({
        borderColor: precolor
    }, 3000);
}

function fail(selector) {
    var precolor = $(selector).css('border-color');
    $(selector).css({ borderColor: 'Red' }).animate({
        borderColor: precolor
    }, 3000);
}

function cookieSet(selector) {
    var mass = [];
    $(selector).each(function () {
        var $el = $(this);
        if ($el.css('display') == 'none')
            mass.push($el.attr("id"));
    });

    $.cookie(selector.selector, mass.join(','));
}

function cookieGet(selector) {
    if ($.cookie(selector.selector) != null && $.cookie(selector.selector).length > 0) {
        var chMap = $.cookie(selector.selector).split(',');
        for (var i in chMap)
            $(selector.selector).filter('#' + chMap[i]).css('display', 'none');
    }
}

function expander(blocks, speed) {
    var all_content = $(blocks).find('.expandable');

    cookieGet(all_content);

    function updateExpanders(content, button) {
        if ($(content).css('display') == 'none') {
            $(button).find('span.ui-icon').removeClass('ui-icon-carat-1-s');
            $(button).find('span.ui-icon').addClass('ui-icon-carat-1-s');
        }
        else {
            $(button).find('span.ui-icon').removeClass('ui-icon-carat-1-s');
            $(button).find('span.ui-icon').addClass('ui-icon-carat-1-n');
        }
    }

    $(blocks).each(function () {
        var button = $(this).find('.expander');
        var content = $(this).find('.expandable');

        updateExpanders(content, button);

        $(button).unbind('click');
        
        $(button).click(function () {
            $(content).slideToggle(speed, function () { cookieSet(all_content); updateExpanders(content, button); });
        });
    });
}

function recountExpander(expander) {
    $(expander).each(function () {
        var total = $(this).find('.expandable').children().length;
        $(this).find('.count').text(total);
    });
}

function invisible(elements) {
    $(elements).each(function () {
        $(this).css('visibility', 'collapse');
        $(this).mouseenter(function () { $(this).css('visibility', 'visible'); });
        $(this).mouseout(function () { $(this).css('visibility', 'collapse'); });
    });
}

function replaceTo(element) {
    $('#popup_content').children().appendTo(element).hide().slideDown(500);
}

function updateTo(element) {
    $('#popup_content').children().replaceAll(element).hide().fadeIn(500);
}

function deleteElement(element, callback) {
    $(element).fadeOut(500, function () { $(this).remove(); if(callback != undefined) callback(); });
}

function deleteIfGood(element, callback) {
    if ($('#alert').find('.good').length > 0)
        deleteElement(element, callback);
}

$(document).ready(function () {
    $('#menu_panel').mouseenter(function () { moveContentRight(); });
    $('#menu_panel').mouseleave(function () { moveContentLeft(); });
});