﻿/// <reference path="jquery-1.8.0.min.js" />
/// <reference path="jquery-1.7.2-vsdoc.js" />

function timeUpdate(server, contaner, from, to, interval) {
    var client_time = parseInt(new Date().getTime() / 1000);
    var server_time = parseInt($(server).text());
    var client_server_span = server_time - client_time;

    setInterval(function () {
        $(contaner).each(function (i) {
            var client_time = parseInt(new Date().getTime() / 1000);
            var time_from = $(this).find(from);
            var time_to = $(this).find(to);
            var signal_time = parseInt($(time_from).text());
            var span = client_time + client_server_span - signal_time;

            if (span >= 0 && span < 60) $(time_to).text(span + ' seconds ago');
            else if (span >= 60 && span < 120) $(time_to).text('one minute ago');
            else if (span >= 120 && span < 3600) $(time_to).text(Math.round(span/60) + ' minutes ago');
            else if (span >= 3600 && span < 7200) $(time_to).text('hour ago');
            else if (span >= 7200) $(time_to).text(Math.round(span / 3600) + ' hours ago');
        });
    }, interval);
}