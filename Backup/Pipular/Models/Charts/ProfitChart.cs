﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pipular.Models.Charts
{
    public class ProfitChart
    {
        public double pl { get; set; }
        public double pts { get; set; }
        public DateTime date { get; set; }
    }
}