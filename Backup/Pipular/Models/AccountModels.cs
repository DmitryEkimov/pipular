﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using Pipular.Attributes.Validation;


namespace Pipular.Models
{

    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Текущий пароль")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("NewPassword", ErrorMessage = "Новый пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }
    }

    public class LogOnModel
    {
        [Required]
        [Display(Name = "Login or e-mail")]
        public string LoginOrEmail { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Login")]
        public virtual string Login { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-mail")]
        public virtual string Email { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "First and last name")]
        public string FIO {  get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The value {0} must be at least {2} characters.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Repeat password")]
        [Compare("Password", ErrorMessage = "Passwords do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Your foto (Url)")]
        [DataType(DataType.ImageUrl)]
        public string Foto { get; set; }

        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        
        [Display(Name = "Address")]
        [DataType(DataType.MultilineText)]
        public string Addr { get; set; }
        
        [Display(Name = "ICQ")]
        public string ICQ { get; set; }        

        [Display(Name = "Skype")]
        public string Skype { get; set; }

        [Display(Name = "Trade type")]
        public List<int> Trade_type { get; set; }

        [Display(Name = "Trade experience")]
        public int Experience { get; set; }

        [Display(Name = "Trade style")]
        public List<int> Trade_style { get; set; }

        [Display(Name = "Trade markets")]
        public List<int> Trade_markets { get; set; }

        [Display(Name = "Trade sessions")]
        public List<int> Trade_sessions { get; set; }

        [Display(Name = "Some description")]
        [DataType(DataType.MultilineText)]
        public string Desc { get; set; }
    }

    public class AdditionRegisterModel : RegisterModel, IValidatableObject
    {
        [Remote("IsLoginExists", "Account", HttpMethod = "POST", ErrorMessage = "This login already exists.")]
        [StringLengthRange(3, 64)]
        public override string Login { get; set; }

        [Remote("IsEmailExists", "Account", HttpMethod = "POST", ErrorMessage = "User with the same e-mail already exist.")]
        [StringLengthRange(6, 32)]
        public override string Email { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            IsExists exist_validator = new IsExists();

            if (exist_validator.Login(this.Login))
            {
                yield return new ValidationResult("This login already exists.", new string[] { "Login" });
            }

            yield break;
        }
    }
}
