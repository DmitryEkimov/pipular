﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pipular.Models.Charts;

namespace Pipular.Models
{
    public class LiveTradeSignal
    {
        public int nmbr { get; set; }
        public string uid { get; set; }
        public string system { get; set; }
        public int system_id { get; set; }
        public string trader { get; set; }
        public int trader_id { get; set; }
        public string terminal_type { get; set; }
        public int dt_terminal { get; set; }
        public DateTime terminal_time { get; set; }
        public string command { get; set; }
        public int command_nmbr { get; set; }
        public string signal_type { get; set; }
        public int signal_type_nmbr { get; set; }
        public double sl { get; set; }
        public double tp { get; set; }
        public string sl_str { get; set; }
        public string tp_str { get; set; }
        public string symbol { get; set; }
        public double price { get; set; }
        public string profit_str { get; set; }
        public double profit { get; set; }
        public int digits { get; set; }
        public double lot { get; set; }
        public DateTime public_time { get; set; }

        public static string GetCMD(int cmd)
        {
            switch (cmd)
            {
                case 0:
                    return "buy";
                case 1:
                    return "sell";
                case 2:
                    return "buylimit";
                case 3:
                    return "selllimit";
                case 4:
                    return "buystop";
                case 5:
                    return "sellstop";
                default:
                    return "no cmd";
            }
        }

        public static string GetTerminalType(int type)
        {
            switch(type)
            {
                case 0:
                    return "MT4";
                case 1:
                    return "MT5";
                default:
                    return "unknown";
            }
        }

        public static string GetSignalType(int type)
        {
            switch(type)
            {
                case 0:
                    return "open";
                case 1:
                    return "modify";
                case 2:
                    return "close";
                default:
                    return "unknown";
            }
        }

        public static double GetPrice(signal signal)
        {
            switch (signal.stype)
            {
                case 2:
                    return signal.close_price;
                default:
                    return signal.open_price;
            }
        }

        public static void GetModify(signal signal, out string sl, out string tp)
        {
            sl = "";
            tp = "";

            if (signal.stype == 1)
            {
                if (signal.sl > 0) sl = signal.sl.ToString("F" + signal.digits);
                else sl = "no s/l";
                if (signal.tp > 0) tp = signal.tp.ToString("F" + signal.digits);
                else tp = "no t/p";
            }
        }
    }

    public class LiveSystemModel
    {
        public string name { get; set; }
        public rsystem data { get; set; }
        public IEnumerable<ProfitChart> profit { get; set; }
    }
}