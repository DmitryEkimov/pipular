﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pipular.Models
{
    public class Profit
    {
        public double? dp { get; set; }

        public double? dpp { get; set; }

        public DateTime dt { get; set; }
    }

    public class Drawdown
    {
        public double? dd { get; set; }

        public double? ddp { get; set; }

        public DateTime dt { get; set; }
    }

    public class SystemSeriesElement
    {
        public double? eq { get; set; }

        public double? eqp { get; set; }

        public double? pl { get; set; }

        public double? pts { get; set; }

        public double? dp { get; set; }

        public double? dpp { get; set; }

        public double? dd { get; set; }

        public double? ddp { get; set; }

        public DateTime dt { get; set; }
    }

    public class Statistic
    {
        public int trades { get; set; }
        public double pips { get; set; }
        public double avg_profit { get; set; }
        public double avg_profit_pips { get; set; }
        public double avg_loss { get; set; }
        public double avg_loss_pips { get; set; }
        public double lots { get; set; }

        public int profit_trades { get; set; }
        public int loss_trades { get; set; }
        public int profit_long { get; set; }
        public int profit_short { get; set; }
        public int longs { get; set; }
        public int shorts { get; set; }
        public double? best { get; set; }
        public double? best_pips { get; set; }
        public double? worst { get; set; }
        public double? worst_pips { get; set; }
        public string avg_time_trade { get; set; }

        public double profit_factor { get; set; }
        public double expected_payoff { get; set; }
        public double expected_payoff_pips { get; set; }
        public double standart_deviation { get; set; }
        public double zscore { get; set; }
        public double prob { get; set; }
        public double sharpe { get; set; }
    }

    public class rating
    {
        public string system_name { get; set; }
        public int system_nmbr { get; set; }
        public double pl { get; set; }
        public double pts { get; set; }
        public int clients { get; set; }
        public int signals { get; set; }
        public int trades { get; set; }
        public double frequency { get; set; }
        public double risk { get; set; }
        public double maxdd { get; set; }
        public bool online { get; set; }
    }
}