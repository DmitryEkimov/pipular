﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pipular.Models
{
    public enum MessageType : byte { Bad, Middle, Good, Special }

    public class TraderModel
    {
        public int nmbr { get; set; }

        public string name { get; set; }

        public int systems { get; set; }

        public int connects { get; set; }

        public int experience { get; set; }
    }

    public class Message
    {
        public MessageType type { get; set; }

        public string text { get; set; }
    }
}