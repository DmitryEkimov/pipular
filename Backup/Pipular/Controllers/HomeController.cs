﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pipular.Models;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Text;
using MathNet.Numerics;
using MathNet.Numerics.Distributions;
using NLog;
using Pipular.Models.Charts;
using System.Net;

namespace Pipular.Controllers
{
    public class HomeController : Controller
    {
        pipularEntities DB = new pipularEntities();

        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Systems
        public ActionResult Systems()
        {
            if (User.Identity.IsAuthenticated)
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + "soft/";
                int user_nmbr = int.Parse(User.Identity.Name);

                if(DB.users.First(u => u.nmbr == user_nmbr).soft < PrivateController.GetFiles(path).OrderByDescending(f => f.version).First().version)
                    Response.Cookies["new_version"].Value = "true";
            }
            return View();
        }
        #endregion

        #region System table
        public ActionResult SystemTable()
        {
            return PartialView("SystemTable", GetAllSystems());
        }
        #endregion

        #region System details
        [CompressFilter]
        public ActionResult SystemDetails(int id, DateTime? start, DateTime? stop)
        {
            IQueryable<history> history_system;
            IQueryable<trade> trade_system;
            IQueryable<signal> signal_system;
            Statistic stat = new Statistic();

            int time_delay = 0;
            bool IsConnected = false;
            bool IsOwner = false;
            int user_nmbr = 0;
            
            system sys = DB.systems.First(s => s.nmbr == id);
            string trader = DB.users.First(u => u.nmbr == sys.user).login;

            if (User.Identity.IsAuthenticated)
            {
                user_nmbr = int.Parse(User.Identity.Name);
                
                if (DB.connects.Any(c => c.user == user_nmbr & c.enabled == 1 & c.system == id))
                {
                    IsConnected = true;
                    
                    List<portfolio> used_portfolios = (from c in DB.connects
                                                       join p in DB.portfolios on c.portfolio equals p.nmbr
                                                       where c.enabled == 1 & c.system == id & c.user == user_nmbr
                                                       select p).ToList();
                    
                    ViewData["portfolios"] = used_portfolios;
                }

                if (sys.user == user_nmbr) IsOwner = true;
            }

            if (IsConnected || IsOwner) // add for admin
            {
                history_system = Query.History(id, 0, start, stop);
                trade_system = Query.Trades(id, 0, start, stop);
                signal_system = Query.Signals(id, start, stop);
            }
            else
            {
                time_delay = int.Parse(ConfigurationManager.AppSettings["Time_delay_of_history"]);
                DateTime time_limit = DateTime.UtcNow.AddHours(-time_delay);

                history_system = Query.History(id, 0, time_limit);
                trade_system = Query.Trades(id, 0, time_limit);
                signal_system = Query.Signals(id, time_limit);
            }

            history default_history = new history() { pl = 0, uid = "", pts = 0 };
            signal default_signal = new signal() { sorder = 0, stype = 0, uid = "", dt_terminal = 0 };

            Normal n = new Normal(0, 1);

            rsystem system_stat = (from rs in DB.rsystems
                                   where rs.system == id
                                   select rs).FirstOrDefault();

            if (system_stat == null) system_stat = new rsystem()
            {
                system = id,
                signals = 0,
                trades = 0,
                longs = 0,
                profitable = 0,
                longs_prof = 0,
                lots = 0,
                all_time_trade = 0,
                variance = 0
            };

            int totalTrades = system_stat.trades;

            int profitTrades = system_stat.profitable;

            int lossTrades = system_stat.trades - system_stat.profitable;

            int P = 2 * profitTrades * lossTrades;

            stat.trades = totalTrades;
            stat.profit_trades = profitTrades;
            stat.loss_trades = lossTrades;
            stat.longs = system_stat.longs;
            stat.shorts = system_stat.trades - system_stat.longs;
            stat.pips = Math.Round(system_stat.profit_pip + system_stat.loss_pip, 1);
            stat.avg_profit = system_stat.trades > 0 ? Math.Round(system_stat.profit / system_stat.profitable, 2) : 0;
            stat.avg_profit_pips = system_stat.trades > 0 ? Math.Round(system_stat.profit_pip / system_stat.profitable, 2) : 0;
            stat.avg_loss = system_stat.trades > 0 ? Math.Round(system_stat.loss / lossTrades, 2) : 0;
            stat.avg_loss_pips = system_stat.trades > 0 ? Math.Round(system_stat.loss_pip / lossTrades, 2) : 0;
            stat.lots = system_stat.lots / 100;
            stat.profit_long = system_stat.longs_prof;
            stat.profit_short = system_stat.profitable - system_stat.longs_prof;
            stat.best = system_stat.best;
            stat.best_pips = system_stat.best_pip;
            stat.worst = system_stat.worst;
            stat.worst_pips = system_stat.worst_pip;
            stat.avg_time_trade = system_stat.trades > 0 ? (system_stat.all_time_trade / system_stat.trades).UnixTimeSpan().TimeSpanToString() : "";
            stat.profit_factor = lossTrades > 0 ? Math.Round((double)profitTrades / (double)lossTrades, 2) : 0;
            stat.expected_payoff = system_stat.trades > 0 ? Math.Round((system_stat.profit + system_stat.loss) / system_stat.trades, 2) : 0;
            stat.expected_payoff_pips = system_stat.trades > 0 ? Math.Round((system_stat.profit_pip + system_stat.loss_pip) / system_stat.trades, 2) : 0;
            stat.standart_deviation = system_stat.trades > 0 ? Math.Round(Math.Sqrt(stat.trades / (stat.trades - 1) * system_stat.variance), 2) : 0;
            // Z=(N*(R-0.5)-P)/((P*(P-N))/(N-1))^(1/2)
            stat.zscore = system_stat.trades > 0 ? Math.Round((totalTrades * ((double)system_stat.series - 0.5) - P) / Math.Sqrt((double)(P * (P - totalTrades)) / (double)(totalTrades - 1)), 2) : 0;
            stat.prob = system_stat.trades > 0 ? Math.Round(n.CumulativeDistribution(Math.Abs(stat.zscore)) * 100, 2) : 0;
            stat.sharpe = system_stat.trades > 0 ? Math.Round(stat.expected_payoff / stat.standart_deviation, 3) : 0;
            
            ViewData["history_system"] = history_system;
            ViewData["trade_system"] = trade_system;
            ViewData["signal_system"] = signal_system;
            ViewData["IsConnected"] = IsConnected;
            ViewData["IsOwner"] = IsOwner;
            ViewData["time_delay"] = time_delay.ToString();
            ViewData["system"] = sys;
            ViewData["trader"] = trader;
            ViewData["statistic"] = stat;
            ViewData["start"] = start;
            ViewData["stop"] = stop;
            
            
            if (Request.IsAjaxRequest())
                return PartialView();
            return View();
        }
        #endregion

        #region Trader details
        public ActionResult TraderDetails(string id)
        {
            user trader_inf = DB.users.First(t => t.login == id);

            int user_nmbr = trader_inf.nmbr;

            ViewData["trader_inf"] = trader_inf;
            ViewData["trader_systems"] = GetUserSystems(user_nmbr);

            if (Request.IsAjaxRequest()) return PartialView();
            else return View();
        }
        #endregion

        #region Traders
        public ActionResult Traders()
        {
            List<TraderModel> traders = (from user in DB.users
                                         select new TraderModel()
                                         {
                                             nmbr = user.nmbr,
                                             name = user.login,
                                             systems = (from rs in DB.rsystems
                                                        join s in DB.systems on rs.system equals s.nmbr
                                                        where rs.trades > 0 && rs.connect == 0 && DB.tarifs.Any(tar => tar.system == rs.system & tar.enabled == 1) && s.user == user.nmbr
                                                        select rs).Count(),
                                             connects = DB.connects.Count(s => s.user == user.nmbr & s.enabled == 1),
                                             experience = user.experience
                                         }).ToList();
            
            ViewData["traders"] = traders;

            return View();
        }
        #endregion

        public IEnumerable<rsystem> GetSystemList()
        {
            return from rs in DB.rsystems
                   where rs.trades > 0 && rs.connect == 0 && DB.tarifs.Any(tar => tar.system == rs.system & tar.enabled == 1)
                   select rs;
        }

        public IEnumerable<rsystem> GetSystemList(int user)
        {
            return from rs in DB.rsystems
                   join s in DB.systems on rs.system equals s.nmbr
                   where rs.trades > 0 && rs.connect == 0 && DB.tarifs.Any(tar => tar.system == rs.system & tar.enabled == 1) && s.user == user
                   select rs;
        }

        public List<rating> GetRatingList(IEnumerable<rsystem> system_list)
        {
            List<rating> model = (from rs in system_list
                                  join s in DB.systems on rs.system equals s.nmbr
                                  select new rating()
                                  {
                                      system_name = s.name,
                                      system_nmbr = s.nmbr,
                                      pl = rs.profit + rs.loss,
                                      pts = rs.profit_pip + rs.loss_pip,
                                      clients = rs.clients,
                                      signals = rs.signals,
                                      trades = rs.trades,
                                      frequency = rs.trades / ((s.dt_last.HasValue ? s.dt_last.Value : s.dt_create.AddDays(1)) - s.dt_create).TotalDays,
                                      maxdd = rs.maxdd,
                                      online = s.serv3online == 1 ? true : false
                                  }).ToList();
            return model;
        }

        public List<rating> GetAllSystems()
        {
            return GetRatingList(GetSystemList());
        }

        public List<rating> GetUserSystems(int user)
        {
            return GetRatingList(GetSystemList(user));
        }

        public ActionResult TradeStream()
        {
            return View();
        }

        // Live trades

        #region Render Partial View to string
        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            ViewBag.NewSignal = true;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion

        #region Add signal to TradeStream
        [HttpGet]
        public ActionResult AddSignal(signal signal)
        {
            if (Request.Url.Host == "test.pipular.com")
            {
                string request = "http://pipular.com/Home/AddSignal?" + Request.QueryString.ToString();
                logger.Info(request);
                WebRequest req = WebRequest.Create(request);
                req.Method = "GET";
                WebResponse res = req.GetResponse();
            }

            string allowed_ip = ConfigurationManager.AppSettings["ALLOWED_IP"];
            string remote_addr = Request.ServerVariables["REMOTE_ADDR"];
            logger.Info(remote_addr);
            if (remote_addr != allowed_ip && remote_addr != "::1" && remote_addr != "test.pipular.com")
            {
                ViewBag.Response = -2;
                return PartialView();
            }
            try
            {
                string order_view = null;
                string system_view = null;
                LiveTradeSignal live_signal = null;
                LiveSystemModel live_system = null;

                SignalInfo.GetInfo(signal, out live_signal, out live_system);

                order_view = RenderRazorViewToString("LiveSignalPartial", live_signal);
                system_view = RenderRazorViewToString("LiveSystemPartial", live_system);

                LiveTradesHub hub = new LiveTradesHub(LiveTradeManager.Instance);
                hub.SendLiveTrade(order_view, system_view);

                ViewBag.Response = 1;
            }
            catch(Exception e)
            {
                ViewBag.Response = -1;
                lock (this) 
                { 
                    logger.Info(String.Format("TradeStream error: {0}\r\nInner exeption: {1}\r\nCMD: {2}", e.Message, e.InnerException, Request.QueryString.ToString())); 
                }
            }

            return PartialView();
        }
        #endregion      

        #region Get signals
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult GetSignals(int id = 0, string uid = "", int dt = 0, int count = 10)
        {
            List<LiveTradeSignal> model = new List<LiveTradeSignal>();

            int last_signal_nmbr = 2147483647;

            if (id > 0) 
                last_signal_nmbr = id;
            else if (uid != "")
                last_signal_nmbr = DB.signals.OrderByDescending(s => s.nmbr).First(s => s.uid == uid && s.dt_terminal == dt).nmbr;

            try
            {
                var temp = (from sig in DB.signals
                            join sys in DB.systems on sig.system equals sys.nmbr
                            join t in DB.users on sys.user equals t.nmbr
                            orderby sig.nmbr descending
                            select new
                            {
                                nmbr = sig.nmbr,
                                uid = sig.uid,
                                system_id = ((sys == null) ? -1 : sys.nmbr),
                                system = ((sys == null) ? "no system" : sys.name),
                                trader_id = ((t == null) ? -1 : t.nmbr),
                                trader = ((t == null) ? "no trader" : t.login),
                                ttype = sig.ttype,
                                dt_terminal = sig.dt_terminal,
                                cmd = sig.cmd,
                                stype = sig.stype,
                                sl = sig.sl,
                                tp = sig.tp,
                                symbol = sig.symbol,
                                sig = sig,
                                digits = sig.digits,
                                volume = sig.volume,
                                dt_create = sig.dt_create
                            }).Where(s => s.nmbr < last_signal_nmbr).Take(count).ToList();

                foreach (var i in temp)
                {
                    double profit_num = 0;
                    string sl_str = "";
                    string tp_str = "";
                    LiveTradeSignal.GetModify(i.sig, out sl_str, out tp_str);

                    model.Add(new LiveTradeSignal()
                    {
                        nmbr = i.nmbr,
                        uid = i.uid,
                        system_id = i.system_id,
                        system = i.system,
                        trader_id = i.trader_id,
                        trader = i.trader,
                        terminal_type = LiveTradeSignal.GetTerminalType(i.ttype),
                        dt_terminal = i.dt_terminal,
                        terminal_time = Extension.UnixToDate(i.dt_terminal),
                        command = LiveTradeSignal.GetCMD(i.cmd),
                        command_nmbr = i.cmd,
                        signal_type = LiveTradeSignal.GetSignalType(i.stype),
                        signal_type_nmbr = i.stype,
                        sl = i.sl,
                        tp = i.tp,
                        sl_str = sl_str,
                        tp_str = tp_str,
                        symbol = i.symbol,
                        price = LiveTradeSignal.GetPrice(i.sig),
                        profit_str = SignalInfo.GetProfit(i.sig, out profit_num),
                        profit = profit_num,
                        digits = i.digits,
                        lot = (double)i.volume / 100,
                        public_time = i.dt_create
                    });
                }
            }
            catch(Exception error)
            {
                ViewBag.Response = error.Message;
            }

            if (model.Count == 0) ViewBag.Response = "No more signals";

            //model.ToClientTime((s, offset) => s.public_time = s.public_time.AddMinutes(offset));

            return PartialView(model);
        }
        #endregion

        #region Get systems
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult GetSystems(int id = 0, int count = 10)
        {
            List<LiveSystemModel> model = new List<LiveSystemModel>();  

            try
            {
                IEnumerable<signal> last_signals = (from s in DB.signals
                                                    orderby s.nmbr descending
                                                    select s).ToList().Distinct(new SignalComparer());

                model = (from sig in last_signals
                         join p in DB.daily_profit on sig.system equals p.system
                         join s in DB.systems on sig.system equals s.nmbr
                         where p.connect == 0 && s.enabled == 1
                         group p by p.system into pGroup
                         select new LiveSystemModel()
                         {
                             name = DB.systems.FirstOrDefault(s => s.nmbr == pGroup.Key).name,
                             data = DB.rsystems.FirstOrDefault(rs => rs.system == pGroup.Key && rs.connect == 0),
                             profit = from pg in pGroup
                                      select new ProfitChart()
                                          {
                                              pl = pg.pl,
                                              pts = pg.pts,
                                              date = pg.date
                                          }
                         }).Take(count).ToList();

                if (id > 0) model = model.Skip(model.IndexOf(model.Single(s => s.data.system == id)) + 1).Take(count).ToList();

                if (model.Count == 0) ViewBag.Response = "No more systems";
            }
            catch(Exception error)
            {
                ViewBag.Response = error.Message;
            }

            return PartialView(model);
        } 
        #endregion

        #region System signals
        public ActionResult SystemSignals(int system, int pageSize)
        {
            int TotalSignals = (from signal in DB.signals
                                where signal.system == system
                                orderby signal.dt_create descending
                                select signal).ToClientTime((h, offset) => h.dt_create = h.dt_create.AddMinutes(offset)).Count();

            int TotalPages = (int)(Math.Ceiling(TotalSignals / (decimal)pageSize));

            ViewBag.TotalSignals = TotalSignals;
            ViewBag.TotalPages = TotalPages;
            ViewBag.System = system;

            return PartialView();
        } 
        #endregion

        #region MoreSignals
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult MoreSignals(int system, int pageNum = 0)
        {
            int pageSize = 0;
            if (!int.TryParse(ConfigurationManager.AppSettings["PAGE_SIZE"], out pageSize)) pageSize = 10;

            List<signal> signals = (from signal in DB.signals
                                    where signal.system == system
                                    orderby signal.dt_create descending
                                    select signal).ToClientTime((h, offset) => h.dt_create = h.dt_create.AddMinutes(offset)).ToList();
            
            List<signal> model = signals.Skip(pageSize * pageNum).Take(pageSize).ToList();

            //ViewBag.Index = pageNum + 1;
            //ViewBag.Total = (int)(Math.Ceiling(signals.Count / (decimal)pageSize));

            return PartialView(model);
        }
        #endregion

        // TOP FIVE

        #region Top systems
        public ActionResult TopSystems(int period = 0, int count = 5)
        {
            DateTime lowTime = Extension.GetPeriod(period);

            List<history> hist = GetSystemHistory(lowTime);

            List<TopSystemsModel> top_systems = MostProfitableSystems(hist, count);

            return PartialView(top_systems);
        } 
        #endregion

        #region Popular systems
        public ActionResult PopSystems(int count)
        {
            List<vsystem> pop_systems = (from vs in DB.vsystems
                                         where vs.enabled == 1
                                         orderby vs.followers descending
                                         select vs).Take(count).ToList();

            return PartialView(pop_systems);
        } 
        #endregion

        #region System followers
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult SystemFollowers(int system, int pageSize)
        {
            int Totalfollowers = (int)DB.vsystems.Single(s => s.nmbr == system).followers;

            int TotalPages = (int)(Math.Ceiling(Totalfollowers / (decimal)pageSize));

            ViewBag.TotalFollowers = Totalfollowers;
            ViewBag.TotalPages = TotalPages;
            ViewBag.System = system;

            return PartialView();
        } 
        #endregion

        #region More followers
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult MoreFollowers(int system, int pageNum = 0)
        {
            int pageSize = 0;
            if (!int.TryParse(ConfigurationManager.AppSettings["PAGE_SIZE"], out pageSize)) pageSize = 10;

            List<user> followers = (from c in DB.connects
                                    join u in DB.users on c.user equals u.nmbr
                                    where c.system == system && c.enabled == 1
                                    select u).Distinct().OrderBy(u => u.dt_create).Skip(pageSize * pageNum).Take(pageSize).ToList();

            return PartialView(followers);
        } 
        #endregion

        #region Top trades
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult TopTrades(int period = 0, int count = 5)
        {
            DateTime lowTime = Extension.GetPeriod(period);

            List<TopTradesModel> top_trades = MostProfitableTrades(lowTime, DateTime.UtcNow.AddDays(1), count);

            return PartialView(top_trades);
        } 
        #endregion

        // Contests

        #region Contests
        public ActionResult Contests()
        {
            List<ContestModel> contests = (from c in DB.contests
                                           where c.@public == 1 && c.enabled == 1
                                           orderby c.dt_end descending
                                           select new ContestModel()
                                           {
                                               nmbr = c.nmbr,
                                               leader = c.leader,
                                               value = c.value,
                                               desc = c.desc,
                                               dt_begin = c.dt_begin,
                                               dt_end = c.dt_end,
                                               prize = c.prize,
                                               type = c.type,
                                               dt_create = c.dt_create
                                           }).ToList();
            // check a leader
            foreach (ContestModel con in contests)
            {
                if ((DateTime.Now >= con.dt_begin && DateTime.Now <= con.dt_end) || con.leader == null)
                {
                    GetContestResult(con);
                }
                else
                {
                    con.leader_name = DB.systems.Single(s => s.nmbr == con.leader).name;
                }
            }

            DB.SaveChanges();

            return PartialView(contests);
        } 
        #endregion

        #region Get winner
        public ActionResult GetWinner(int id)
        {
            ContestModel model = (from c in DB.contests
                                  join s in DB.systems on c.leader equals s.nmbr
                                  where c.nmbr == id
                                  select new ContestModel()
                                  {
                                      nmbr = c.nmbr,
                                      leader = c.leader,
                                      leader_name = s.name,
                                      value = c.value,
                                      desc = c.desc,
                                      dt_begin = c.dt_begin,
                                      dt_end = c.dt_end,
                                      prize = c.prize,
                                      type = c.type,
                                      dt_create = c.dt_create
                                  }).Single();

            GetContestResult(model);

            DB.SaveChanges();

            return PartialView("ContestWinner", model);
        } 
        #endregion

        #region Get contest result
        private void GetContestResult(ContestModel con)
        {
            switch (con.type)
            {
                case 1:
                    List<history> hist = GetSystemHistory(con.dt_begin, con.dt_end);
                    if (hist.Count > 0)
                    {
                        List<TopSystemsModel> top_systems = MostProfitableSystems(hist);
                        if (top_systems.Count > 0)
                        {
                            TopSystemsModel top_system = top_systems.FirstOrDefault();
                            con.leader = top_system.nmbr;
                            con.leader_name = top_system.name;
                            con.value = top_system.pips;
                        }
                    }
                    break;
                case 2:
                    List<PopSystemsModel> pop_systems = MostPopularSystem(con.dt_begin, con.dt_end);
                    if (pop_systems.Count > 0)
                    {
                        PopSystemsModel pop_system = pop_systems.FirstOrDefault();
                        con.leader = pop_system.nmbr;
                        con.leader_name = pop_system.name;
                        con.value = pop_system.followers;
                    }
                    break;
                case 3:
                    List<TopTradesModel> top_trades = MostProfitableTrades(con.dt_begin, con.dt_end);
                    if (top_trades.Count > 0)
                    {
                        TopTradesModel top_trade = top_trades.FirstOrDefault();
                        con.leader = top_trade.system;
                        con.leader_name = top_trade.system_name;
                        con.value = top_trade.pts;
                    }
                    break;
            }
            if (con.leader != null)
            {
                contest contest = DB.contests.First(c => c.nmbr == con.nmbr);
                contest.leader = con.leader;
                contest.value = con.value;

            }
        } 
        #endregion

        // Statistics*

        #region Top instruments
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult TopInstruments(int period = 0, int count = 5)
        {
            DateTime lowTime = Extension.GetPeriod(period);

            List<TopInstrumentsModel> top_instruments = (from s in DB.signals
                                                         where s.dt_create >= lowTime
                                                         group s by s.symbol into symbolGroup
                                                         select new TopInstrumentsModel()
                                                         {
                                                             symbol = symbolGroup.Key,
                                                             signals = (from sig in symbolGroup 
                                                                        where sig.symbol == symbolGroup.Key 
                                                                        select sig.nmbr).Count()
                                                         }).OrderByDescending(s => s.signals).Take(count).ToList();

            foreach (TopInstrumentsModel ti in top_instruments)
            {
                ti.buys = (from h in DB.historys 
                           where h.cmd == 0 && h.dt_create >= lowTime && h.symbol == ti.symbol
                           select h.nmbr).Union(from t in DB.trades
                                                where t.cmd == 0 && t.dt_create >= lowTime && t.symbol == ti.symbol
                                                select t.nmbr).Count();
                ti.total_signals = (from h in DB.historys
                                    where h.cmd < 2 && h.dt_create >= lowTime && h.symbol == ti.symbol
                                    select h.nmbr).Union(from t in DB.trades
                                                         where t.cmd < 2 && t.dt_create >= lowTime && t.symbol == ti.symbol
                                                         select t.nmbr).Count();
            }

            return PartialView(top_instruments);
        } 
        #endregion

        #region Recent members
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult RecentMembers(int count)
        {
            List<user> recent_members = (from u in DB.users
                                         orderby u.dt_create descending
                                         select u).Take(count).ToList();

            return PartialView(recent_members);
        } 
        #endregion

        #region Get System History
        [NonAction]
        private List<history> GetSystemHistory(DateTime start)
        {
            List<history> hist = (from h in DB.historys
                                  where h.dt_create >= start && h.connect == 0
                                  select h).ToList();
            return hist;
        }
        #endregion

        #region Get System History
        [NonAction]
        private List<history> GetSystemHistory(DateTime start, DateTime end)
        {
            List<history> hist = (from h in DB.historys
                                  where h.dt_create >= start && h.dt_create <= end && h.connect == 0
                                  select h).ToList();
            return hist;
        } 
        #endregion

        #region Most profitable systems
        [NonAction]
        private List<TopSystemsModel> MostProfitableSystems(List<history> hist, int count = 5)
        {
            List<TopSystemsModel> top_systems = (from h in hist
                                                 group h by h.system into system_history
                                                 join s in DB.vsystems on system_history.Key equals s.nmbr
                                                 where s.enabled == 1
                                                 select new TopSystemsModel()
                                                 {
                                                     nmbr = s.nmbr,
                                                     name = s.name,
                                                     pips = (from h1 in system_history
                                                             where h1.system == system_history.Key
                                                             select h1.pts).Sum(),
                                                     profit = (from h1 in system_history
                                                               where h1.system == system_history.Key
                                                               select h1.pl).Sum()
                                                 }).OrderByDescending(s => s.pips).Take(count).ToList();

            return top_systems;
        }
        #endregion

        #region Most popular system
        private List<PopSystemsModel> MostPopularSystem(DateTime start, DateTime end, int count = 5)
        {
            return (from rs in DB.rsystems
                    join s in DB.systems on rs.system equals s.nmbr
                    where rs.connect == 0
                    select new PopSystemsModel()
                    {
                        nmbr = s.nmbr,
                        name = s.name,
                        followers = rs.clients
                    }).OrderByDescending(vs => vs.followers).Take(count).ToList();
        } 
        #endregion

        #region Most profitable trades
        [NonAction]
        private List<TopTradesModel> MostProfitableTrades(DateTime start, DateTime end, int count = 5)
        {
            List<TopTradesModel> top_trades = (from h in DB.historys
                                               join s in DB.systems on h.system equals s.nmbr
                                               where s.enabled == 1 && h.connect == 0 && h.cmd < 2 && h.dt_create >= start && h.dt_create <= end
                                               orderby h.pts descending
                                               select new TopTradesModel()
                                               {
                                                   pts = h.pts,
                                                   pl = h.pl,
                                                   system = h.system,
                                                   system_name = s.name,
                                                   symbol = h.symbol,
                                                   open_price = h.open_price,
                                                   close_price = h.close_price,
                                                   digits = h.digits,
                                                   cmd = h.cmd,
                                                   copies = (from h1 in DB.historys
                                                             where h1.signal == h.uid
                                                             select h1).Count(),
                                                   dt_create = h.dt_create
                                               }).ToList();

            top_trades = top_trades.Distinct(new TopTradesComparer()).Take(count).ToList();

            return top_trades;
        }
        #endregion
    }

    public class LiveSystemComparer : IEqualityComparer<LiveSystemModel>
    {
        public bool Equals(LiveSystemModel x, LiveSystemModel y)
        {
            return (x.data.system == y.data.system);
        }

        public int GetHashCode(LiveSystemModel obj)
        {
            return obj.data.system.GetHashCode();
        }
    }

    public class SignalComparer : IEqualityComparer<signal>
    {
        public bool Equals(signal x, signal y)
        {
            return (x.system == y.system);
        }

        public int GetHashCode(signal obj)
        {
            return obj.system.GetHashCode();
        }
    }

    public class TopTradesComparer : IEqualityComparer<TopTradesModel>
    {
        public bool Equals(TopTradesModel x, TopTradesModel y)
        {
            return (x.system == y.system);
        }

        public int GetHashCode(TopTradesModel obj)
        {
            return obj.system.GetHashCode();
        }
    }
}