﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Pipular.Models;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Configuration;
using Pipular.Attributes.Validation;
using System.ComponentModel;

namespace Pipular.Controllers
{
    public class AccountController : Controller
    {
        // data base entity
        pipularEntities DB = new pipularEntities();
        BackgroundWorker bw = new BackgroundWorker();
        SERV3 serv3 = new SERV3();

        // list of trade experience
        public static List<SelectListItem> experienceList = new List<SelectListItem>() { 
            new SelectListItem() { Text = "No experience" , Value = "0"},
            new SelectListItem() { Text = "less then a year" , Value = "1"},
            new SelectListItem() { Text = "1 year" , Value = "2"},
            new SelectListItem() { Text = "2 years" , Value = "3"},
            new SelectListItem() { Text = "3 years" , Value = "4"},
            new SelectListItem() { Text = "4 years" , Value = "5"},
            new SelectListItem() { Text = "5 years" , Value = "6"},
            new SelectListItem() { Text = "6 years" , Value = "7"},
            new SelectListItem() { Text = "7 years" , Value = "8"},
            new SelectListItem() { Text = "8 years" , Value = "9"},
            new SelectListItem() { Text = "9 years" , Value = "10"},
            new SelectListItem() { Text = "10 years or more" , Value = "11"}
        };

        #region LOG ON
        [HttpGet]
        public ActionResult LogOn()
        {
            return PartialView();
        }
        #endregion

        #region LOG ON POST
        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                IQueryable<user> usrs;
                user usr = new user();
                string login_email = model.LoginOrEmail.Trim();

                if (IsEmail(login_email))
                {
                    usrs = from user in DB.users
                           where user.email == login_email
                           select user;
                }
                else
                {
                    usrs = from user in DB.users
                           where user.login == login_email
                           select user;
                }

                if (usrs != null & usrs.Count() > 0)
                {
                    usr = usrs.First();
                }
                else
                {
                    ModelState.AddModelError("", "This login or email not found");
                    return PartialView(model);
                }

                // if email not confirmed
                if (usr.enabled == 2)
                {
                    return RedirectToAction("Confirm_send", "Account", new { message = "E-mail not confirmed yet.", uid = usr.nmbr });
                }

                int attemps = int.Parse(ConfigurationManager.AppSettings["Logon_max_attemps"]);
                int ban = int.Parse(ConfigurationManager.AppSettings["Logon_BAN"]);

                bool IsBan = false;
                if (usr.dt_login > DateTime.Now) IsBan = true;

                if (usr.attempts >= attemps - 1 || IsBan)
                {
                    // check ban
                    if (!IsBan)
                    {
                        usr.dt_login = DateTime.Now.AddMinutes(ban);
                        usr.attempts = 0;
                        DB.SaveChanges();
                    }
                    TimeSpan ban_remain = (DateTime)usr.dt_login - DateTime.Now;
                    ModelState.AddModelError("", "You used " + attemps + " attempts. Try to log on after " + (ban_remain.Minutes + 1) + " minutes");
                    return PartialView(model);
                }

                // compare passwords
                StringComparer comparer = StringComparer.Ordinal;

                if (comparer.Compare(GetMD5Hash(model.Password), usr.password) != 0)
                {
                    usr.attempts++;
                    DB.SaveChanges();
                    ModelState.AddModelError("", "Wrong password. Remaining " + (attemps - usr.attempts) + " attempts");
                    return PartialView(model);
                }

                // authentification
                usr.attempts = 0;
                usr.dt_login = DateTime.Now;
                DB.SaveChanges();

                // Add coockie
                FormsAuthentication.SetAuthCookie(usr.nmbr.ToString(), model.RememberMe);

                Response.Cookies["UserData"]["login"] = usr.login;
                Response.Cookies["UserData"].Expires = DateTime.Now.AddHours(double.Parse(ConfigurationManager.AppSettings["Coockie_Expires"]));


                // redirect
                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return PartialView("LogOnResult");
                }

            }
            return PartialView(model);
        }
        #endregion

        #region LOG OFF
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Response.Cookies["UserData"].Expires = DateTime.Now.AddDays(-1);

            return RedirectToAction("Systems", "Home");
        }
        #endregion

        #region Registration
        public ActionResult Register()
        {
            ViewData["ExperienceList"] = experienceList;

            return View();
        }
        #endregion

        #region Registration POST
        [HttpPost]
        public ActionResult Register(AdditionRegisterModel model)
        {
            ViewData["ExperienceList"] = experienceList;

            int uid = 0;

            if (ModelState.IsValid)
            {
                DateTime dt_conf = DateTime.Now.AddMinutes(int.Parse(ConfigurationManager.AppSettings["Delay_Confirm_Email"])); // with time delay
                string dt_conf_hash = GetMD5Hash(dt_conf.ToString());

                IsExists is_exist = new IsExists();

                // is it email?
                if (!IsEmail(model.Email))
                {
                    ModelState.AddModelError("", "E-mail " + model.Email + " isn't correct.");
                    return View(model);
                }

                // is this login exists?
                if (is_exist.Login(model.Login))
                {
                    ModelState.AddModelError("", "Login " + model.Login + " is exist.");
                    return View(model);
                }

                // is this email exists? 
                if (is_exist.Email(model.Email))
                {
                    ModelState.AddModelError("", "User with the same e-mail already exist.");
                    return View(model);
                }

                try
                {
                    user new_user = new user()
                    {
                        login = model.Login,
                        password = GetMD5Hash(model.Password),
                        email = model.Email,
                        fio = model.FIO,
                        photo = model.Foto,
                        icq = model.ICQ,
                        skype = model.Skype,
                        phone = model.Phone,
                        addr = model.Addr,
                        experience = model.Experience,
                        trade_type = GetCheckBoxCode(model.Trade_type),
                        trade_style = GetCheckBoxCode(model.Trade_style),
                        trade_markets = GetCheckBoxCode(model.Trade_markets),
                        trade_sessions = GetCheckBoxCode(model.Trade_sessions),
                        desc = model.Desc,

                        enabled = 2,
                        dt_create = dt_conf,
                        level = 10,
                        conf_code = dt_conf_hash
                    };

                    DB.users.AddObject(new_user);
                    DB.SaveChanges();

                    uid = new_user.nmbr;

                    try
                    {
                        string domen = Request.ServerVariables["HTTP_HOST"];
                        string confirmation_link = "http://" + domen + "/Account/Confirm_email?uid=" + uid.ToString() + "&code=" + dt_conf_hash;
                        string message = "Hellow " + new_user.fio + "!<p><b>Your confirmation link: </b><a href=\"" + confirmation_link + "\" target=_blank >" + confirmation_link + "</a></p>";

                        SendMail(new_user.email, message, "Confirmation of new account");
                    }
                    catch (Exception error)
                    {
                        ModelState.AddModelError("", error.Message);
                        return View(model);
                    }
                }
                catch (Exception error)
                {
                    ModelState.AddModelError("", error.Message);
                    return View(model);
                }

                return RedirectToAction("Confirm_send", "Account", new { message = "You were send a confirmation letter. Check your mailbox.", uid = uid });
            }

            // Появление этого сообщения означает наличие ошибки; повторное отображение формы
            return View(model);
        }
        #endregion

        #region Login async validation
        [HttpPost]
        public JsonResult IsLoginExists(string login)
        {
            IsExists validate = new IsExists();

            bool IsExist = validate.Login(login);

            return this.Json(!IsExist);
        }
        #endregion

        #region E-mail async validation
        public JsonResult IsEmailExists(string email)
        {
            IsExists validate = new IsExists();

            bool IsExist = validate.Email(email);

            return this.Json(!IsExist);
        }
        #endregion

        #region Confirmation letter was send
        public ActionResult Confirm_send(string message, int uid)
        {
            DateTime dt = (from user in DB.users
                           where user.nmbr == uid
                           select user.dt_create).First();
            HtmlString notice = new HtmlString("<p>If the letter is not received within " + ConfigurationManager.AppSettings["Delay_Confirm_Email"] + " minutes, then you can send it again.</p>");
            ViewBag.Message = message + notice;
            ViewBag.Delay = Time_difference(dt);
            ViewBag.Remain = "Remain";
            ViewBag.Button = "Send again";

            return View();
        }
        #endregion

        // confirmaton link
        #region Confirmation of email
        public ActionResult Confirm_email(int uid, string code)
        {            
            if (uid == 0 || code == null)
            {
                ViewBag.Message = "Wrong parameters";
                return View();
            }

            IQueryable<user> IQ_user = from user in DB.users
                                       where user.nmbr == uid
                                       select user;
            if (IQ_user != null)
            {
                if (IQ_user.Count() > 0)
                {
                    user curr_user = IQ_user.First();
                    StringComparer comparer = StringComparer.Ordinal;
                    if (comparer.Compare(curr_user.conf_code, code) == 0)
                    {
                        curr_user.enabled = 1;
                        curr_user.dt_create = DateTime.Now;
                        ViewBag.Message = "Confirming was successful! Now you may log on.";
                        try 
                        { 
                            DB.SaveChanges();
                        }
                        catch { ViewBag.Message = "Error writing to database"; }

                        bw.DoWork += serv3.SendCMD;
                        bw.RunWorkerAsync(new string[] { "ADD_USER", "users", curr_user.nmbr.ToString() });
                    }
                    else ViewBag.Message = "Wrong confirming code";
                }
                else { ViewBag.Message = "User not found"; }
            }
            else { ViewBag.Message = "User with this uid not exists"; }

            return View();
        }
        #endregion

        #region Sending confirmation mail
        public ActionResult SendConfEmail(int uid)
        {
            user usr = (from user in DB.users
                        where user.nmbr == uid
                        select user).First();

            DateTime dt_conf = DateTime.Now.AddMinutes(int.Parse(ConfigurationManager.AppSettings["Delay_Confirm_Email"])); // with time delay
            string dt_conf_hash = GetMD5Hash(dt_conf.ToString());

            usr.conf_code = dt_conf_hash;
            usr.dt_create = dt_conf;
            DB.SaveChanges();

            string domen = Request.ServerVariables["HTTP_HOST"];
            string confirmation_link = "http://" + domen + "/Account/Confirm_email?uid=" + uid.ToString() + "&code=" + dt_conf_hash;
            string message = "Hellow " + usr.fio + "!<p><b>Your confirmation link: </b><a href=\"" + confirmation_link + "\" target=_blank >" + confirmation_link + "</a></p>";

            string note = "You were send a confirmation letter. Check your mailbox.";

            try { SendMail(usr.email, message, "Confirmation of new account"); }
            catch { note = "Error: can not send e-mail."; }

            return RedirectToAction("Confirm_send", "Account", new { message = note, uid = uid });
        }
        #endregion

        // Non action

        #region Is a e-mail?
        [NonAction]
        private bool IsEmail(string email)
        {
            Regex regex = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");
            return regex.IsMatch(email);
        }
        #endregion

        #region Send email
        [NonAction]
        public static int SendMail(string mail_to, string message, string subject)
        {
            string host = ConfigurationManager.AppSettings["Mail_HOST"];
            int port = int.Parse(ConfigurationManager.AppSettings["Mail_PORT"]);
            bool use_auth = bool.Parse(ConfigurationManager.AppSettings["Mail_UseAuthorization"]);
            bool use_ssl = bool.Parse(ConfigurationManager.AppSettings["Mail_UseSSL"]);
            string login = ConfigurationManager.AppSettings["Mail_LOGIN"];
            string pass = ConfigurationManager.AppSettings["Mail_PASSWORD"];
            MailAddress mail_from = new MailAddress(ConfigurationManager.AppSettings["Mail_FROM"]);


            MailMessage mail_message = new MailMessage();
            mail_message.Subject = subject;
            mail_message.Body = message;
            mail_message.BodyEncoding = Encoding.UTF8;
            mail_message.IsBodyHtml = true;
            mail_message.From = mail_from;
            mail_message.To.Add(new MailAddress(mail_to));

            SmtpClient smtp = new SmtpClient(host, port);
            smtp.EnableSsl = use_ssl;
            if (use_auth) smtp.Credentials = new NetworkCredential(login, pass);
            smtp.Send(mail_message);
            smtp.Dispose();

            return 0;
        }
        #endregion

        #region Get CheckBox code (int) for Trade_type, Trade_style, etc
        [NonAction]
        public static int GetCheckBoxCode(List<int> list)
        {
            int code = 0;

            if (list != null)
                if (list.Count > 0)
                    foreach (int c in list) code ^= c;

            return code;
        }
        #endregion

        #region Time difference about now
        [NonAction]
        private int Time_difference(DateTime dt) // in srconds
        {
            TimeSpan span = dt - DateTime.Now;

            int delay = (int)Math.Round(span.TotalSeconds, 0);

            if (delay < 0) delay = 0;

            return delay;
        }
        #endregion

        #region Get MD5 hash code
        [NonAction]
        public static string GetMD5Hash(string input)
        {
            MD5 md5Hasher = MD5.Create();

            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString().ToUpper();
        }
        #endregion
    }
}
