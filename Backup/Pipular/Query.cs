﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pipular.Models;
using System.Data.Objects;

namespace Pipular
{
    public static class Query
    {
        private static pipularEntities DB = new pipularEntities();
        
        #region Methods

        /// <summary>
        /// Get all history or by time
        /// </summary>
        /// <param name="system">System nmbr</param>
        /// <param name="connect">Connect nmbr</param>
        /// <param name="start">Time start</param>
        /// <param name="stop">Tie stop</param>
        /// <returns></returns>
        public static IQueryable<history> History(int system, int connect, DateTime? start, DateTime? stop)
        {
            return start.HasValue && stop.HasValue ? HistoryOfSystemByTime(DB, system, connect, start.Value, stop.Value) : HistoryOfSystem(DB, system, connect);
        }

        /// <summary>
        /// Get history by limit
        /// </summary>
        /// <param name="system">System nmbr</param>
        /// <param name="connect">Connect nmbr</param>
        /// <param name="limit">Highest time limit</param>
        /// <returns></returns>
        public static IQueryable<history> History(int system, int connect, DateTime limit)
        {
            return HistoryOfSystemByLimit(DB, system, connect, limit);
        }

        /// <summary>
        /// Get all trades or by time
        /// </summary>
        /// <param name="system">System nmbr</param>
        /// <param name="connect">Connect nmbr</param>
        /// <param name="start">Time start</param>
        /// <param name="stop">Time stop</param>
        /// <returns></returns>
        public static IQueryable<trade> Trades(int system, int connect, DateTime? start, DateTime? stop)
        {
            return start.HasValue && stop.HasValue ? TradesOfSystemByTime(DB, system, connect, start.Value, stop.Value) : TradesOfSystem(DB, system, connect);
        }

        /// <summary>
        /// Get trades by limit
        /// </summary>
        /// <param name="system">System nmbr</param>
        /// <param name="connect">Connect nmbr</param>
        /// <param name="limit">Highest time limit</param>
        /// <returns></returns>
        public static IQueryable<trade> Trades(int system, int connect, DateTime limit)
        {
            return TradesOfSystemByLimit(DB, system, connect, limit);
        }

        /// <summary>
        /// Get all signals or by time
        /// </summary>
        /// <param name="system">System nmbr</param>
        /// <param name="start">Start time</param>
        /// <param name="stop">Stop time</param>
        /// <returns></returns>
        public static IQueryable<signal> Signals(int system, DateTime? start, DateTime? stop)
        {
            return start.HasValue && stop.HasValue ? SignalsOfSystemByTime(DB, system, start.Value, stop.Value) : SignalsOfSystem(DB, system);
        }

        /// <summary>
        /// Get signals by limit
        /// </summary>
        /// <param name="system">System nmbr</param>
        /// <param name="start">Start time</param>
        /// <param name="stop">Stop time</param>
        /// <returns></returns>
        public static IQueryable<signal> Signals(int system, DateTime limit)
        {
            return SignalsOfSystemByLimit(DB, system, limit);
        }

        #endregion

        #region Compiled queries

        // all history
        private static readonly Func<pipularEntities, int, int, IQueryable<history>> HistoryOfSystem =
            CompiledQuery.Compile<pipularEntities, int, int, IQueryable<history>>(
            (p, system, connect) =>
                p.historys.Where(h =>
                    h.system == system && h.connect == connect)
                .OrderByDescending(h => h.dt_create)
            );

        // history by time
        private static readonly Func<pipularEntities, int, int, DateTime, DateTime, IQueryable<history>> HistoryOfSystemByTime =
            CompiledQuery.Compile<pipularEntities, int, int, DateTime, DateTime, IQueryable<history>>(
            (p, system, connect, start, stop) => 
                p.historys.Where(h => 
                    h.system == system && h.connect == connect && h.dt_create > start && h.dt_create <= stop)
                .OrderByDescending(h => h.dt_create)
            );

        // history by limit
        private static readonly Func<pipularEntities, int, int, DateTime, IQueryable<history>> HistoryOfSystemByLimit =
            CompiledQuery.Compile<pipularEntities, int, int, DateTime, IQueryable<history>>(
            (p, system, connect, limit) =>
                p.historys.Where(h =>
                    h.system == system && h.connect == connect && h.dt_create <= limit)
                .OrderByDescending(h => h.dt_create)
            );



        // all trades
        private static readonly Func<pipularEntities, int, int, IQueryable<trade>> TradesOfSystem =
            CompiledQuery.Compile<pipularEntities, int, int, IQueryable<trade>>(
            (p, system, connect) => 
                p.trades.Where(t => 
                    t.system == system && t.connect == connect)
                    .OrderByDescending(t => t.dt_create)
            );

        // trades by time
        private static readonly Func<pipularEntities, int, int, DateTime, DateTime, IQueryable<trade>> TradesOfSystemByTime =
            CompiledQuery.Compile<pipularEntities, int, int, DateTime, DateTime, IQueryable<trade>>(
            (p, system, connect, start, stop) => 
                p.trades.Where(t => 
                    t.system == system && t.connect == connect && t.dt_create > start && t.dt_create <= stop)
                    .OrderByDescending(t => t.dt_create)
            );

        // trades by limit
        private static readonly Func<pipularEntities, int, int, DateTime, IQueryable<trade>> TradesOfSystemByLimit =
            CompiledQuery.Compile<pipularEntities, int, int, DateTime, IQueryable<trade>>(
            (p, system, connect, limit) =>
                p.trades.Where(t =>
                    t.system == system && t.connect == connect && t.dt_create <= limit)
                    .OrderByDescending(t => t.dt_create)
            );


        // all signals
        private static readonly Func<pipularEntities, int, IQueryable<signal>> SignalsOfSystem =
            CompiledQuery.Compile<pipularEntities, int, IQueryable<signal>>(
            (p, system) =>
                p.signals.Where(s =>
                    s.system == system)
                    .OrderByDescending(s => s.dt_create)
            );


        // signals by time
        private static readonly Func<pipularEntities, int, DateTime, DateTime, IQueryable<signal>> SignalsOfSystemByTime =
            CompiledQuery.Compile<pipularEntities, int, DateTime, DateTime, IQueryable<signal>>(
            (p, system, start, stop) =>
                p.signals.Where(s => 
                    s.system == system && s.dt_create > start && s.dt_create <= stop)
                    .OrderByDescending(s => s.dt_create)
            );

        // signals by limit
        private static readonly Func<pipularEntities, int, DateTime, IQueryable<signal>> SignalsOfSystemByLimit =
            CompiledQuery.Compile<pipularEntities, int, DateTime, IQueryable<signal>>(
            (p, system, limit) =>
                p.signals.Where(s =>
                    s.system == system && s.dt_create <= limit)
                    .OrderByDescending(s => s.dt_create)
            );
        #endregion
    }
}