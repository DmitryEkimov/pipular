﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pipular.Accounting
{
    public class AccountingStatus
    {
        public DateTime last_start { get; set; }

        public TimeSpan left_to_start { get; set; }
    }
}