﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;
using Pipular.Models;

namespace Pipular.Accounting
{
    public class RecountResultCollection : Collection<RecountResult>
    {
        static string fileName = "AccountingTimeLog.xml";
        static string fileFolder = ConfigurationManager.AppSettings["ACCOUNTING_FOLDER"];
        static string filePath = AppDomain.CurrentDomain.BaseDirectory + fileFolder + "\\" + fileName;

        public void Save()
        {
            FileStream stream = new FileStream(filePath, FileMode.Create);
            
            if (stream == null)
                throw new ArgumentException("Parameter stream must be not null.", "stream");

            XmlSerializer serializer = new XmlSerializer(typeof(RecountResultCollection));

            serializer.Serialize(stream, this);
            stream.Close();
        }

        public static RecountResultCollection Load()
        {
            if (!File.Exists(filePath)) return new RecountResultCollection();
            
            FileStream stream = new FileStream(filePath, FileMode.Open);

            if (stream == null) return null;
                //throw new ArgumentException("Parameter stream must be not null.", "stream");

            XmlSerializer serializer = new XmlSerializer(typeof(RecountResultCollection));
            RecountResultCollection rrc = (RecountResultCollection)serializer.Deserialize(stream);
            stream.Close();

            return rrc;
        }

        public static void Delete(RecountResult recountResult)
        {
            pipularEntities DB = new pipularEntities();

            try
            {
                IQueryable<accounting> delete = from a in DB.accountings
                                                where a.dt_start == recountResult.start && a.dt_stop == recountResult.stop
                                                select a;

                foreach (var a in delete)
                {
                    DB.accountings.DeleteObject(a);
                }

                DB.SaveChanges();

                RecountResultCollection rrc = Load();

                RecountResult rr = rrc.FirstOrDefault(r => r.start == recountResult.start && r.stop == recountResult.stop);

                rrc.Remove(rr);
                rrc.Save();
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
