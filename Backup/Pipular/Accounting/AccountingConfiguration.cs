﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Pipular.Accounting
{
    public static class AccountingConfiguration
    {
        public static PeriodType Period 
        { 
            get 
            {
                byte result;
                return byte.TryParse(ConfigurationManager.AppSettings["ACCOUNTING_PERIOD"], out result) ? (PeriodType)result : PeriodType.days;
            }
            set
            {
                ConfigurationManager.AppSettings["ACCOUNTING_PERIOD"] = ((byte)value).ToString();
            }
        }

        public static int Size
        {
            get
            {
                int result;
                return int.TryParse(ConfigurationManager.AppSettings["ACCOUNTING_SIZE"], out result) ? result : 7;
            }
            set
            {
                ConfigurationManager.AppSettings["ACCOUNTING_SIZE"] = value.ToString();
            }
        }

        public static int BeginWith
        {
            get
            {
                int result;
                return int.TryParse(ConfigurationManager.AppSettings["ACCOUNTING_BEGIN_WITH"], out result) ? result : 6;
            }
            set
            {
                ConfigurationManager.AppSettings["ACCOUNTING_BEGIN_WITH"] = value.ToString();
            }
        }

        public static bool RecountNow
        {
            get
            {
                bool result;
                return bool.TryParse(ConfigurationManager.AppSettings["ACCOUNTING_RECOUNT_NOW"], out result) ? result : true;
            }
            set
            {
                ConfigurationManager.AppSettings["ACCOUNTING_RECOUNT_NOW"] = value.ToString();
            }
        }

        public static bool ReplaceLast
        {
            get
            {
                bool result;
                return bool.TryParse(ConfigurationManager.AppSettings["ACCOUNTING_REPLACE_LAST"], out result) ? result : true;
            }
            set
            {
                ConfigurationManager.AppSettings["ACCOUNTING_REPLACE_LAST"] = value.ToString();
            }
        }

        public static bool AutoStart
        {
            get
            {
                bool result;
                return bool.TryParse(ConfigurationManager.AppSettings["ACCOUNTING_AUTO_START"], out result) ? result : true;
            }
            set
            {
                ConfigurationManager.AppSettings["ACCOUNTING_AUTO_START"] = value.ToString();
            }
        }

        public enum PeriodType : byte
        {
            minutes = 1,
            hours = 2,
            days = 3,
            weeks = 4,
            months = 5
        }
    }
}