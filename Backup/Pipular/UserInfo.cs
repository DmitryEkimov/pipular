﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pipular
{
    public class UserInfo
    {
        public static string GetExperience(int experience)
        {
            switch (experience)
            {
                case 0:
                    return "no experience";
                case 1:
                    return "less then a year";
                case 11:
                    return "more 10 years";
                default:
                    return (experience - 1).ToString() + " years";
            }
        }

        public static string GetMarket(int market)
        {
            List<int> markets = ConvertCheckBox(market);
            string markets_str = " ";

            foreach (int m in markets)
            {
                switch (m)
                {
                    case 1:
                        markets_str += " Forex";
                        break;
                    case 2:
                        markets_str += " Futures";
                        break;
                    case 4:
                        markets_str += " Metals";
                        break;
                    case 8:
                        markets_str += " CFDs";
                        break;
                    case 16:
                        markets_str += " Stocks";
                        break;
                }
                markets_str += ",";
            }

            return markets_str.TrimEnd(',');
        }

        public static string GetStyle(int style)
        {
            List<int> styles = ConvertCheckBox(style);
            string style_str = " ";
            foreach (int s in styles)
            {
                switch (s)
                {
                    case 1:
                        style_str += " scalping";
                        break;
                    case 2:
                        style_str += " swing";
                        break;
                    case 4:
                        style_str += " news trading";
                        break;
                }
                style_str += ",";
            }
            return style_str.TrimEnd(',');
        }

        #region  Find checked checkboxes
        private static List<int> ConvertCheckBox(int value)
        {
            List<int> list = new List<int>();

            string bits = Convert.ToString(value, 2).TrimStart('0');

            int l = bits.Length - 1;

            for (int i = 0; i <= l; i++)
            {
                if (bits[i] == '1') list.Add((int)Math.Pow(2, l - i));
            }

            return list;
        }
        #endregion
    }
}