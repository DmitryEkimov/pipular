TYPE=TRIGGERS
triggers='CREATE DEFINER=`root`@`localhost` TRIGGER `pipular`.`balance_insert_after` AFTER INSERT ON `pipular`.`balances`\n  FOR EACH ROW UPDATE users SET users.balance = users.balance + NEW.summa WHERE users.nmbr = NEW.user' 'CREATE DEFINER=`root`@`localhost` TRIGGER `pipular`.`balance_update_after` AFTER UPDATE ON `pipular`.`balances`\n  FOR EACH ROW UPDATE users SET users.balance = users.balance - OLD.summa + NEW.summa WHERE users.nmbr = NEW.user' 'CREATE DEFINER=`root`@`localhost` TRIGGER `pipular`.`delete` BEFORE DELETE ON `pipular`.`balances`\n  FOR EACH ROW INSERT INTO balances_backup (nmbr_backup, user, system, type, status, active, summa, comment)\nVALUES (OLD.nmbr,OLD.user,OLD.system,OLD.type, OLD.status, OLD.active,OLD.summa,OLD.comment)'
sql_modes=1344274432 1344274432 1344274432
definers='root@localhost' 'root@localhost' 'root@localhost'
client_cs_names='utf8' 'utf8' 'utf8'
connection_cl_names='utf8_general_ci' 'utf8_general_ci' 'utf8_general_ci'
db_cl_names='utf8_general_ci' 'utf8_general_ci' 'utf8_general_ci'
